#include <Serialize/Io.hpp>
#include <Containers/Integer.hpp>

using namespace hsd::format_literals;

template <hsd::usize b, hsd::usize N>
static constexpr auto divide(hsd::integer<N> a)
{
    using WideInteger = hsd::integer<N * 2>;

    hsd::integer<N> mul = {};
    mul.set();
    mul /= hsd::integer<N>{b};
    mul += 1;

    hsd::pair<hsd::integer<N>, hsd::integer<N>> _res;

    WideInteger _a = a, _b;

    hsd::println("\n_a = {}\nmul = {}"_fmt, _a, mul);

    _a *= mul;

    hsd::println("_a * mul = {}"_fmt, _a);

    _a >>= N;

    _res.second = a - (_a * WideInteger{b});
    _res.first = hsd::integer<N>{_a};

    return _res;
}

int main()
{
    hsd::integer<128> _lhs = 0xFFFF'FFFF'FFFF'FFFFULL;
    constexpr hsd::usize _rhs = 0xFFFF'FFFF'FFFF'FFFFULL;

    hsd::u128 a = 0xFFFF'FFFF'FFFF'FFFFULL;

    hsd::println("{}"_fmt, _lhs * hsd::integer<64>{_rhs});
    hsd::println("{}"_fmt, a * a);

    _lhs <<= 1;

    hsd::u128 aaaa = -1;

    aaaa /= a;
    aaaa += 1;

    hsd::f128 aa = a;
    aa *= 2;

    hsd::println("\na = {}\nmul = {}"_fmt, aa, aaaa);

    aa *= aaaa;

    hsd::println("_a * mul = {}"_fmt, aa);

    a >>= 32;

    hsd::println("{hex}"_fmt, a * a);

    auto [quot, rem] = divide<_rhs>(_lhs);

    hsd::println("{}"_fmt, quot);
    hsd::println("{}"_fmt, rem);
}