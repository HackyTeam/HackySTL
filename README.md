<p align = 'left'>
  <img src='https://gitlab.com/HackyTeam/HackySTL/-/raw/dev/Docs/Logo.png'>
</p>

This is a project which discards any backwards compatibility
thus, it's written using the latest version of the C++ standard.
The idea came from another [project](https://github.com/LegatAbyssWalker/amazingCode) 

# Goal:
To have reimplemented a bare minimum of useful features from over a year ago

# Contributors:
- DeKrain (Implementations)
- qookie (Mostly debugging and some implementations)

# Platforms
  [Discord](https://discord.gg/dEghMASRKb)