
# Contributor Stylistic guides for HackySTL

## File names and folder structures

Use `PascalCase` for module files and testing files

## Identation style

Allman style, end of discussion.

## Naming style

For now, everything will be snake_case, possibly excluding generic parameters

## Documentation style

Just to be there, will be decided on that later