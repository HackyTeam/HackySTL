#include <Serialize/Io.hpp>
#include <Containers/Bitset.hpp>
#include <assert.h>


int main()
{
    using namespace hsd::format_literals;
 
    // constructors:
    constexpr hsd::bitset<4> b1;
    constexpr hsd::bitset<4> b2{0xA}; // == 0B1010
    hsd::bitset<4> b3{0b0011}; // can also be constexpr since C++23
    hsd::bitset<8> b4{0b0110}; // == 0B0000'0110
 
    // bitsets can be printed out to a stream:
    hsd::print("b1: {}"_fmt, b1);
    hsd::print("; b2: {}"_fmt, b2);
    hsd::print("; b3: {}"_fmt, b3);
    hsd::print("; b4: {}"_fmt, b4);
    hsd::print("\n"_fmt);
 
    // bitset supports bitwise operations:
    b3 |= 0b0100; assert(b3 == 0b0111);
    b3 &= 0b0011; assert(b3 == 0b0011);
    b3 ^= hsd::bitset<4>{0b1100}; 
    assert(b3 == 0b1111);
 
    // operations on the whole set:
    b3.reset(); 
    assert(b3 == 0);

    b3.set(); 
    assert(b3 == 0b1111);

    assert(b3.all() && b3.any() && !b3.none());

    b3.flip(); 
    assert(b3 == 0);
 
    // operations on individual bits:
    b3.set(1, true).unwrap(); 
    assert(b3 == 0b0010);

    b3.set(1, false).unwrap(); 
    assert(b3 == 0);

    b3.flip(2); 
    assert(b3 == 0b0100);

    b3.reset(2); 
    assert(b3 == 0);
 
    // subscript operator[] is supported:
    b3[2] = true; 
    assert(true == b3[2]);
 
    // other operations:
    assert(b3.count() == 1);
    assert(b3.size() == 4);

    hsd::bitset<12> b{0b111101110010};
    hsd::println("{} (initial value)"_fmt, b);
 
    for (; b.any(); b >>= 1)
    {
        while (!b.test(0).unwrap())
            b >>= 1;
        
        hsd::println("{}"_fmt, b);
    }

    hsd::println("{} (final value)"_fmt, b);
    //assert(b3.to_ullong() == 0b0100ULL);
    //assert(b3.to_string() == "0100");
}