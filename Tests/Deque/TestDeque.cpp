#include <Containers/Deque.hpp>
#include <Serialize/Io.hpp>

int main()
{
    using namespace hsd::format_literals;
    hsd::deque<hsd::i32> test = {{1, 2, 3, 4, 5}};
    hsd::deque<hsd::i32> test2 = {{1, 2, 3, 4, 5}};

    test.push_front(200);
    test.push_front(400);
    test.push_back(300);
    test.push_back(500);
    test.pop_front();
    test.pop_back();
    test.push_front(400);
    test.push_back(300);

    for (auto it = test.rbegin(); it != test.rend();)
    {
        if (*it == 1)
        {
            it = test.erase(it).unwrap();
        }
        else
        {
            hsd::print("{} "_fmt, *it);
            --it;
        }
    }

    hsd::print("\n"_fmt);
}