#include <Serialize/Io.hpp>
#include <Wrappers/Functional.hpp>
#include <Threading/Future.hpp>
 
int main()
{
    using namespace hsd::format_literals;

    hsd::packaged_task<hsd::f64()> packaged_task([](){ return 3.14159; });

    hsd::future<hsd::f64> future = packaged_task.get_future();

    auto lambda = [task = hsd::move(packaged_task)]() mutable { task(); };

    hsd::function<void()> function = hsd::move(lambda); // Ok

    function().unwrap();

    hsd::println("{}"_fmt, future.get().unwrap());
}