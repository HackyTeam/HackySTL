#include <Serialize/Io.hpp>
#include <Containers/Integer.hpp>
#include <Threading/Time.hpp>

int main()
{
    using namespace hsd::format_literals;

    hsd::integer<32> b1 = 123'132;
    hsd::integer<32> b2 = 234'543;

    hsd::println("{}"_fmt, b2 - b1);
    hsd::println("{}"_fmt, b2 + b1);
    hsd::println("{}"_fmt, b2 * b1);

    auto [quot, rem] = b2.divide_with_reminder(b1).unwrap();

    hsd::println("{}"_fmt, quot);
    hsd::println("{}"_fmt, rem);

    hsd::integer<16384> b3 = 0;

    b3 -= 1;

    hsd::io::cout().get_stream().reserve(8192);

    hsd::clock Clk;
    hsd::println("{}"_fmt, b3);

    hsd::println("\n\nTime elapsed: {} us\n\n"_fmt, Clk.restart().to_microseconds());

    hsd::println("{}"_fmt, hsd::limits<hsd::f128>::max);

    hsd::println("\n\nTime elapsed: {} us\n\n"_fmt, Clk.restart().to_microseconds());
}