#include <Serialize/Logging.hpp>
#include <Serialize/ToString.hpp>

template <hsd::usize N>
auto test_trace([[maybe_unused]] const char* msg, [[maybe_unused]] hsd::stack_trace st = hsd::stack_trace::add())
    -> hsd::option_err<hsd::stack_trace_error>
{
    auto res = test_trace<N - 1>(msg);
    res.unwrap();

    return {};
}

template <>
auto test_trace<0>([[maybe_unused]] const char* msg, [[maybe_unused]] hsd::stack_trace st)
    -> hsd::option_err<hsd::stack_trace_error>
{
    return hsd::stack_trace_error{};
}

static void test_profiler_to_str([[maybe_unused]] hsd::profiler pf = hsd::profiler::add())
{
    using FpType = hsd::f128;

    for (hsd::usize it = 0; it < 1000; ++it)
    {
        for (auto idx = hsd::limits<FpType>::denorm_min; idx != hsd::limits<FpType>::infinity; idx *= 10)
        {
            auto v = hsd::to_pseudo_string_hex(-idx);
            asm volatile("" : "+m"(idx) : : "memory");
            asm volatile("" : "+m"(v) : : "memory");
        }
    }
}

static void test_profiler_to_snpr([[maybe_unused]] hsd::profiler pf = hsd::profiler::add())
{
    using FpType = hsd::f128;

    for (hsd::usize it = 0; it < 1000; ++it)
    {
        for (auto idx = hsd::limits<FpType>::denorm_min; idx != hsd::limits<FpType>::infinity; idx *= 10)
        {
            char buf[100000];
            snprintf(buf, 99999, "%La", -idx);
            asm volatile("" : "+m"(buf) : : "memory");
            asm volatile("" : "+m"(idx) : : "memory");
        }
    }
}

int main()
{
    //test_trace<120>("hello").unwrap();  
        
    test_profiler_to_str();
    test_profiler_to_snpr();
    return 0;
}