#include <Base/Random.hpp>
#include <Serialize/Io.hpp>
#include <assert.h>

void test_assert()
{
    hsd::mt19937_32 gen32;
    hsd::mt19937_64 gen64;
 
    gen32.discard(10'000 - 1);
    gen64.discard(10'000 - 1);
 
    assert(gen32.generate() == 4'123'659'995);
    assert(gen64.generate() == 9'981'545'732'273'789'042ull);
}

int main()
{
    using namespace hsd::format_literals;

    hsd::mt19937_32 engine;

    for (hsd::u16 i = 0; i < 65000; i++)
    {
        hsd::println(
            "{}, {}"_fmt,
            engine.generate(1, 4).unwrap(),
            engine.generate(1., 4.).unwrap()
        );

        engine.discard(5);
    }

    test_assert();
}