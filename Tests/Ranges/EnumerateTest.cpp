#include <Containers/Vector.hpp>
#include <Containers/Span.hpp>
#include <Serialize/Io.hpp>

int main()
{
    using namespace hsd::format_literals;

    hsd::vector<hsd::i32> vec = {{1, 2, 3, 4, 5, 6, 7, 8, 9}};
    hsd::io::cout().get_stream().reserve(1024);

    for (auto [val, count] : vec.to_span().enumerate())
    {
        hsd::print("({}, {}) "_fmt, val, count);
        val = val + 1;
    }

    hsd::print("\n"_fmt);

    for (const auto& val : vec)
    {
        hsd::print("{} "_fmt, val);
    }

    hsd::print("\n"_fmt);
}