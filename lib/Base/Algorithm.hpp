#pragma once

#include "Utility.hpp"
#include "Concepts.hpp"

namespace hsd
{
    template <typename T>
    concept DigitVector = requires(T a) {
        {a.size()} -> IsSame<usize>;
        {static_cast<remove_reference_t<decltype(a[0])>>(a[0])} -> IsNumber;
    };

    template <typename T, typename Getter>
    concept DigitVectorGetter = requires(T a, Getter getter) {
        {a.size()} -> IsSame<usize>;
        {a[0]};
        {getter(a[0])} -> IsNumber;
    };

    template <typename T>
    concept ArrayLike = requires(T a) {
        {a.size()} -> IsSame<usize>;
        {a[0]};
    };

    template <typename T>
    concept ArrayLikeComparable = requires(T a) {
        {a.size()} -> IsSame<usize>;
        {a[0]};
        {a[0] > a[1]};
    };

    template <typename Vec>
    struct count_sorter
    {
        static constexpr void sort(Vec& arr, usize exp)
        {
            Vec _output = {};
            usize _index, _count[10] = {};

            if constexpr (requires {_output.push_back(arr[0]);})
            {
                typename Vec::value_type val = {};

                for (_index = 0; _index < arr.size(); _index++)
                    _output.push_back(val);
            }

            for (_index = 0; _index < arr.size(); _index++)
            {
                _count[(arr[_index] / exp) % 10]++;
            }

            for (_index = 1; _index < 10; _index++)
            {
                _count[_index] += _count[_index - 1];
            }

            for (_index = arr.size() - 1; _index > 0; _index--)
            {
                _output[_count[(arr[_index] / exp) % 10] - 1] = arr[_index];
                _count[(arr[_index] / exp) % 10]--;
            }

            _output[_count[(arr[0] / exp) % 10] - 1] = arr[0];
            _count[(arr[0] / exp) % 10]--;

            for (_index = 0; _index < arr.size(); _index++)
            {
                swap(arr[_index], _output[_index]);
            }
        }

        static constexpr void sort(Vec& arr, usize exp, auto&& get_digit)
        {
            Vec _output = {};
            usize _index, _count[10] = {};

            if constexpr (requires {_output.push_back(arr[0]);})
            {
                typename Vec::value_type val = {};

                for (_index = 0; _index < arr.size(); _index++)
                    _output.push_back(val);
            }

            for (_index = 0; _index < arr.size(); _index++)
            {
                _count[(get_digit(arr[_index]) / exp) % 10]++;
            }

            for (_index = 1; _index < 10; _index++)
            {
                _count[_index] += _count[_index - 1];
            }

            for (_index = arr.size() - 1; _index > 0; _index--)
            {
                _output[_count[(get_digit(arr[_index]) / exp) % 10] - 1] = get_digit(arr[_index]);
                _count[(get_digit(arr[_index]) / exp) % 10]--;
            }

            _output[_count[(get_digit(arr[0]) / exp) % 10] - 1] = get_digit(arr[0]);
            _count[(get_digit(arr[0]) / exp) % 10]--;

            for (_index = 0; _index < arr.size(); _index++)
            {
                swap(arr[_index], _output[_index]);
            }
        }
    };

    template <DigitVector Vec>
    static constexpr void radix_sort(Vec& arr, auto&& pred)
    {
        auto _max_val = [&]()
        {
            auto _mx = arr.begin();

            for (auto _iter = arr.begin(); _iter != arr.end(); _iter++)
            {
                _mx = (pred(*_iter, *_mx)) ? _iter : _mx;
            }

            return *_mx;
        }();
    
        for (usize exp = 1; _max_val / exp > 0; exp *= 10)
        {
            count_sorter<Vec>::sort(arr, exp);
        }
    }

    template <typename Vec, typename Getter>
    requires (DigitVectorGetter<Vec, Getter>)
    static constexpr void radix_sort(Vec& arr, auto&& pred, Getter&& get_digit)
    {
        auto _max_val = [&]()
        {
            auto _mx = arr.begin();

            for (auto _iter = arr.begin(); _iter != arr.end(); _iter++)
            {
                _mx = (pred(*_iter, *_mx)) ? _iter : _mx;
            }

            return *_mx;
        }();
    
        for (usize exp = 1; get_digit(_max_val) / exp > 0; exp *= 10)
        {
            count_sorter<Vec>::sort(arr, exp, get_digit);
        }
    }

    template <typename Vec>
    struct quick_sorter
    {
        static constexpr usize partition(Vec& vec, usize low, usize high)
        {
            bool dec_high = true;

            while (low < high)
            {
                if (vec[low] > vec[high])
                {
                    swap(vec[high], vec[low]);
                    dec_high = !dec_high;
                }

                (dec_high == true) ? --high : ++low;
            }

            return low;
        }

        static constexpr void sort(Vec& vec, usize low, usize high)
        {
            if (low >= high) return;

            usize middle = partition(vec, low, high);
            
            if (middle != 0) sort(vec, low, middle - 1);
            
            sort(vec, middle + 1, high);
        }

        static constexpr usize partition(Vec& vec, auto& pred, usize low, usize high)
        {
            bool dec_high = true;

            while (low < high)
            {
                if (pred(vec[low], vec[high]))
                {
                    swap(vec[high], vec[low]);
                    dec_high = !dec_high;
                }

                (dec_high == true) ? --high : ++low;
            }

            return low;
        }

        static constexpr void sort(Vec& vec, auto& pred, usize low, usize high)
        {
            if (low >= high) return;

            usize middle = partition(vec, pred, low, high);
            
            if (middle != 0) sort(vec, pred, low, middle - 1);
            
            sort(vec, pred, middle + 1, high);
        }
    };

    template <ArrayLikeComparable Vec>
    static constexpr void quick_sort(Vec& arr)
    {
        if constexpr (requires {arr.length();})
        {
            quick_sorter<Vec>::sort(arr, 0, arr.length() - 1);
        }
        else
        {
            quick_sorter<Vec>::sort(arr, 0, arr.size() - 1);
        }
    }

    template <ArrayLikeComparable Vec>
    static constexpr void quick_sort(Vec& arr, auto&& pred)
    {
        if constexpr (requires {arr.length();})
        {
            quick_sorter<Vec>::sort(arr, pred, 0, arr.length() - 1);
        }
        else
        {
            quick_sorter<Vec>::sort(arr, pred, 0, arr.size() - 1);
        }
    }
} // namespace hsd
