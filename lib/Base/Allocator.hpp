#pragma once

#include "Limits.hpp"
#include "Result.hpp"
#include "Types.hpp"
#include "Utility.hpp"

namespace hsd
{
    namespace allocator_detail
    {
        class allocator_error
        {
        private:
            const char* _err = nullptr;

        public:
            constexpr allocator_error(const char* error)
                : _err{error}
            {}

            const char* pretty_error() const
            {
                return _err;
            }
        };

        // thanks qookie
        struct block 
        {
            u64 in_use : 1;
            u64 size : 8 * sizeof(u64) - 1;
        };

        static inline auto* _get_next_block(block* ptr, usize sz)
        {
            return reinterpret_cast<block*>(
                reinterpret_cast<uchar*>(ptr) + sz + sizeof(block)
            );
        }

        template <typename T>
        static inline auto* _get_data_from_block(block* ptr)
        {
            return reinterpret_cast<T*>(
                reinterpret_cast<uchar*>(ptr) + sizeof(block)
            );
        }

        static inline void _make_next_block(
            block* ptr, usize block_size, 
            usize free_size, usize data_size)
        {
            block_size += block_size & (alignof(block) - 1);
            data_size += data_size & (alignof(block) - 1);

            auto* _next_block = _get_next_block(ptr, block_size);
            _next_block->in_use = 0;
            
            _next_block->size = {
                (free_size != 0) ? 
                free_size - data_size : 
                ptr->size - data_size - sizeof(block)
            };
            
            ptr->size = block_size;
        }

        template <typename T>
        static inline auto* _make_block(
            block* ptr, usize free_size, usize data_size)
        {
            auto _data_sz = (free_size != 0) ? 
                data_size - sizeof(block) : data_size;

            ptr->in_use = 1;
            auto _available_size = (free_size != 0) ? free_size : ptr->size;

            if (_available_size > data_size)
            {
                _make_next_block(ptr, _data_sz, free_size, data_size);
            }

            return _get_data_from_block<T>(ptr);
        }

        static inline auto _iterate_blocks_size(block*& prev, block*& curr)
        {
            usize _ret = 0;

            if (prev == nullptr)
            {
                prev = curr;
                _ret += curr->size;
            }
            else
            {
                _ret += curr->size + sizeof(block);
            }
            
            curr = _get_next_block(curr, curr->size);
            return _ret;
        }
    } // namespace allocator_detail

    struct mallocator
    {
        template <typename T>
        [[nodiscard]] static inline auto allocate(usize size)
            -> result<T*, allocator_detail::allocator_error>
        {
            T* _result = static_cast<T*>(malloc(sizeof(T) * size));

            if (_result == nullptr)
            {
                return allocator_detail::allocator_error{"No space left in RAM"};
            }
            else
            {
                return _result;
            }
        }

        static inline auto deallocate(void* ptr, usize)
            -> option_err<allocator_detail::allocator_error>
        {
            free(ptr);
            return {};
        }
    };    

    class allocator
    {
    private:
        struct core_alloc_unit
        {
            usize size;
            usize alignment;
        };

        template <typename T>
        static inline auto* _get_data(core_alloc_unit* ptr)
        {
            return reinterpret_cast<T*>(
                reinterpret_cast<uchar*>(ptr) + sizeof(core_alloc_unit)
            );
        }

        template <typename T>
        static constexpr auto _get_max_align()
        {
            usize _align = 1;

            while (_align < alignof(T))
            {
                _align *= 2;
            }

            return _align;
        }

    public:
        template <typename T>
        [[nodiscard]] static inline auto allocate(usize size)
            -> result<T*, allocator_detail::allocator_error>
        {
            using pointer_type = core_alloc_unit*;
            using value_type = core_alloc_unit;

            if (size > limits<usize>::max / sizeof(T))
            {
                return allocator_detail::allocator_error{"Bad length for allocation"};
            }
            
            constexpr auto _align = _get_max_align<T>();
            auto _alloc_size = sizeof(value_type) + size * sizeof(T);
            auto* _result = reinterpret_cast<pointer_type>(
                new_allocate(_alloc_size, _align)
            );

            if (_result == nullptr)
            {
                return allocator_detail::allocator_error{"No space left in RAM"};
            }

            _result->size = _alloc_size;
            _result->alignment = _align;
            return _get_data<T>(_result);
        }

        template <typename T>
        static inline auto deallocate(T* ptr, usize)
            -> option_err<allocator_detail::allocator_error>
        {
            using pointer_type = core_alloc_unit*;
            using value_type = core_alloc_unit;

            if (ptr == nullptr) return {};

            auto* _alloc_value = reinterpret_cast<pointer_type>(
                reinterpret_cast<uchar*>(ptr) - sizeof(value_type)
            );

            delete_deallocate(
                _alloc_value, _alloc_value->size, _alloc_value->alignment
            );

            return {};
        }
    };

    class buffered_allocator
    {
    private:
        uchar* _buf = nullptr;
        usize _size = 0;
        using block_type = allocator_detail::block;
        
    public:
        inline buffered_allocator(uchar* buf, usize size)
            : _buf{buf}, _size{size}
        {
            if (_buf == nullptr)
            {
                panic("Buffer is nullptr");
            }
            else if (_size <= sizeof(block_type))
            {
                panic("Buffer is too small to contain data");
            }
            
            auto* _block = reinterpret_cast<block_type*>(_buf);

            if (_block->in_use == 0 && _block->size == 0)
            {
                _block->size = _size - sizeof(block_type);
            }
        }

        inline buffered_allocator(const buffered_allocator& other)
            : _buf{other._buf}, _size{other._size}
        {}
        
        inline buffered_allocator(buffered_allocator&& other)
        {
            _buf = exchange(other._buf, nullptr);
            _size = exchange(other._size, 0u);
        }

        inline buffered_allocator& operator=(const buffered_allocator& other)
        {
            _buf = other._buf;
            _size = other._size;

            return *this;
        }

        inline buffered_allocator& operator=(buffered_allocator&& other)
        {
            _buf = exchange(other._buf, nullptr);
            _size = exchange(other._size, 0u);

            return *this;
        }

        template <typename T>
        [[nodiscard]] inline auto allocate(usize size)
            -> result<T*, allocator_detail::allocator_error>
        {
            size *= sizeof(T);
            usize _free_size = 0;
            auto* _curr_block = reinterpret_cast<block_type*>(_buf);
            block_type* _prev_block = nullptr;

            if (size > _size)
            {
                return allocator_detail::allocator_error {
                    "No space left in buffer"
                };
            }

            while (reinterpret_cast<uchar*>(_curr_block) < _buf + _size)
            {
                if (_curr_block->in_use == 0)
                {
                    if (_curr_block->size >= size + sizeof(block_type))
                    {
                        return {
                            allocator_detail::_make_block<T>(_curr_block, 0, size)
                        };
                    }
                    else if (_curr_block->size >= size)
                    {
                        _curr_block->in_use = 1;
                    }
                    else
                    {
                        _free_size += allocator_detail::_iterate_blocks_size(
                            _prev_block, _curr_block
                        );
                    }
                }
                else if (_prev_block != nullptr)
                {
                    if (_free_size >= (size + sizeof(block_type)))
                    {
                        return {
                            allocator_detail::_make_block<T>(
                                _prev_block, _free_size, 
                                size + sizeof(block_type)
                            )
                        };
                    }

                    _free_size = 0;
                    _prev_block = nullptr;

                    _curr_block = allocator_detail::_get_next_block(
                        _curr_block, _curr_block->size
                    );
                }
                else
                {
                    _curr_block = allocator_detail::_get_next_block(
                        _curr_block, _curr_block->size
                    );
                }
            }

            return allocator_detail::allocator_error{"Insufficient memory"};
        }

        template <typename T>
        inline auto deallocate(T* ptr, usize)
            -> option_err<allocator_detail::allocator_error>
        {
            if (ptr == nullptr)
            {
                return {};
            }

            if (
                reinterpret_cast<uchar*>(ptr) < _buf || 
                reinterpret_cast<uchar*>(ptr) >= _buf + _size)
            {
                return allocator_detail::allocator_error{"Pointer out of bounds"};
            }

            auto* _block_ptr = reinterpret_cast<block_type*>(
                reinterpret_cast<uchar*>(ptr) - sizeof(block_type)
            );
            
            _block_ptr->in_use = 0;
            return {};
        }
    };
}