#pragma once

#include "Types.hpp"

// MSVC is the dumbest thing in existence
#ifdef HSD_COMPILER_MSVC
#undef min
#undef max
#endif

namespace hsd
{
    template <typename>
    struct limits;

    namespace detail
    {
        template <typename T>
        constexpr bool is_signed_byte()
        {
            return static_cast<T>(-1) < 0;
        }

        template <typename T, usize B>
        constexpr usize digits_byte()
        {
            return B - is_signed_byte<T>();
        }

        template <typename T, usize B>
        constexpr T max_byte()
        {
            constexpr auto _lshf = static_cast<T>(1) << (digits_byte<T, B>() - 1);

            if constexpr (is_signed_byte<T>())
            {
                return ((_lshf - 1) << 1) + 1;
            }

            return ~static_cast<T>(0);
        }

        template <typename T, usize B>
        constexpr T min_byte()
        {
            if constexpr (is_signed_byte<T>())
            {
                return -max_byte<T, B>() - 1;
            }

            return 0;
        }

        template <typename T, usize B>
        constexpr usize digits10_byte()
        {
            return digits_byte<T, B>() * 643L / 2136;
        }

        template <typename T>
        constexpr bool is_signed()
        {
            return is_signed_byte<T>();
        }

        template <typename T>
        constexpr auto min()
        {
            return min_byte<T, sizeof(T) * 8>();
        }

        template <typename T>
        constexpr auto max()
        {
            return max_byte<T, sizeof(T) * 8>();
        }

        template <typename T>
        constexpr auto digits() 
        {
            return digits_byte<T, sizeof(T) * 8>();
        }

        template <typename T>
        constexpr auto digits10()
        {
            return digits10_byte<T, sizeof(T) * 8>();
        }

        template <typename T>
        constexpr T max_digits10(T value)
        {
            return 2 + value * 643L / 2136;
        }

        template <typename T>
        static constexpr auto _exp_bits()
        {
            hsd::usize _sz = 0;
            auto _max_exp = (limits<T>::max_exponent + 1) * 2;

            while ((_max_exp & 1) == 0)
            {
                _max_exp >>= 1;
                ++_sz;
            }

            return _sz;
        }
    } // namespace detail
    

    template <typename T>
    struct limits
    {
        static constexpr bool  is_specialized        = false;
        static constexpr T     infinity              = 0;
        static constexpr T     epsilon               = 0;
        static constexpr T     nan                   = 0;
        static constexpr T     min                   = detail::min<T>();
        static constexpr T     max                   = detail::max<T>();
        static constexpr T     denorm_min            = 0;
        static constexpr bool  is_signed             = detail::is_signed<T>();
        static constexpr i32   min_exponent          = 0;
        static constexpr i32   min_exponent10        = 0;
        static constexpr i32   min_exponent10_denorm = 0;
        static constexpr i32   max_exponent          = 0;
        static constexpr i32   max_exponent10        = 0;
        static constexpr usize exponent              = 0;
        static constexpr usize digits                = detail::digits<T>();
        static constexpr usize mantissa              = 0;
        static constexpr usize digits10              = detail::digits10<T>();
        static constexpr usize max_digits10          = 0;
    }; 

    template <>
    struct limits<f32>
    {
        static constexpr bool  is_specialized        = true;
        static constexpr f32   infinity              = __builtin_huge_valf();
        static constexpr f32   nan                   = __builtin_nanf("");
        static constexpr f32   epsilon               = __FLT_EPSILON__;
        static constexpr f32   min                   = __FLT_MIN__;
        static constexpr f32   max                   = __FLT_MAX__;
        static constexpr f32   denorm_min            = __FLT_DENORM_MIN__;
        static constexpr bool  is_signed             = true;
        static constexpr i32   min_exponent          = __FLT_MIN_EXP__ - 1;
        static constexpr i32   min_exponent10        = __FLT_MIN_10_EXP__ - 1;
        static constexpr i32   min_exponent_denorm   = -149;
        static constexpr i32   min_exponent10_denorm = -45;
        static constexpr i32   max_exponent          = __FLT_MAX_EXP__ - 1;
        static constexpr i32   max_exponent10        = __FLT_MAX_10_EXP__;
        static constexpr usize exponent              = detail::_exp_bits<f32>();
        static constexpr usize digits                = __FLT_MANT_DIG__;
        static constexpr usize mantissa              = __FLT_MANT_DIG__ - 1;
        static constexpr usize digits10              = __FLT_DIG__;
        static constexpr usize max_digits10          = __FLT_DECIMAL_DIG__;
    };

    template <>
    struct limits<f64>
    {
        static constexpr bool  is_specialized        = true;
        static constexpr f64   infinity              = __builtin_huge_val();
        static constexpr f64   nan                   = __builtin_nan("");
        static constexpr f64   epsilon               = __DBL_EPSILON__;
        static constexpr f64   min                   = __DBL_MIN__;
        static constexpr f64   max                   = __DBL_MAX__;
        static constexpr f64   denorm_min            = __DBL_DENORM_MIN__;
        static constexpr bool  is_signed             = true;
        static constexpr i32   min_exponent          = __DBL_MIN_EXP__ - 1;
        static constexpr i32   min_exponent10        = __DBL_MIN_10_EXP__ - 1;
        static constexpr i32   min_exponent_denorm   = -1074;
        static constexpr i32   min_exponent10_denorm = -324;
        static constexpr i32   max_exponent          = __DBL_MAX_EXP__ - 1;
        static constexpr i32   max_exponent10        = __DBL_MAX_10_EXP__;
        static constexpr usize exponent              = detail::_exp_bits<f64>();
        static constexpr usize digits                = __DBL_MANT_DIG__;
        static constexpr usize mantissa              = __DBL_MANT_DIG__ - 1;
        static constexpr usize digits10              = __DBL_DIG__;
        static constexpr usize max_digits10          = __DBL_DECIMAL_DIG__;
    };

    template <>
    struct limits<f128>
    {
        static constexpr bool  is_specialized        = true;
        static constexpr f128  infinity              = __builtin_huge_vall();
        static constexpr f128  nan                   = __builtin_nanl("");
        static constexpr f128  epsilon               = __LDBL_EPSILON__;
        static constexpr f128  min                   = __LDBL_MIN__;
        static constexpr f128  max                   = __LDBL_MAX__;
        static constexpr f128  denorm_min            = __LDBL_DENORM_MIN__;
        static constexpr bool  is_signed             = true;
        static constexpr i32   min_exponent          = __LDBL_MIN_EXP__ - 1;
        static constexpr i32   min_exponent10        = __LDBL_MIN_10_EXP__ - 1;
        static constexpr i32   min_exponent_denorm   = -16445;
        static constexpr i32   min_exponent10_denorm = -4951;
        static constexpr i32   max_exponent          = __LDBL_MAX_EXP__ - 1;
        static constexpr i32   max_exponent10        = __LDBL_MAX_10_EXP__;
        static constexpr usize exponent              = detail::_exp_bits<f128>();
        static constexpr usize digits                = __LDBL_MANT_DIG__;
        static constexpr usize mantissa              = __LDBL_MANT_DIG__ - 1;
        static constexpr usize digits10              = __LDBL_DIG__;
        static constexpr usize max_digits10          = __LDBL_DECIMAL_DIG__;
    };
} // namespace hsd