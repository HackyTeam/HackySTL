#pragma once

#include "../Wrappers/Pair.hpp"
#include "SourceLocation.hpp"
#include "Concepts.hpp"

#include <stdio.h>
#include <stdlib.h>

namespace hsd
{
    namespace result_detail
    {
        template <typename T>
        static consteval pair<const char*, i32> get_type()
        {
            // Use source_location::current().function_name() for extracting the type name
            
            const char* _type_name = source_location::current().function_name();
            const char* _ptr = _type_name;

            for (; *_type_name != '\0'; ++_type_name)
            {
                // Find the first =

                if (*_type_name == '=')
                {
                    _ptr = _type_name + 2;
                }
            }

            return {_ptr, static_cast<i32>(_type_name - _ptr - 1)};
        }
    } // namespace result_detail

    [[noreturn]] static inline void panic(
        const char* message, source_location loc = source_location::current())
    {
        fprintf(
            stderr, "Got an error in file:"
            "\n%s\nInvoked from: %s at line:"
            " %u\nWith the message: %s\n", 
            loc.file_name(), loc.function_name(), loc.line(), message
        );

        abort();
    }

    template <typename Ok, typename Err>
    [[noreturn]] static inline void panic_type_err(
        source_location loc = source_location::current())
    {
        constexpr auto _ok_type = result_detail::get_type<Ok>();
        constexpr auto _err_type = result_detail::get_type<Err>();

        fprintf(
            stderr, "Got an error in file:"
            "\n%s\nInvoked from: %s at line:"
            " %u\nWith the message: Expected "
            "%.*s, got %.*s\n", loc.file_name(), loc.function_name(), 
            loc.line(), _ok_type.second, _ok_type.first,
            _err_type.second, _err_type.first
        );

        abort();
    }

    [[noreturn]] static inline void unimplemented(
        source_location loc = source_location::current())
    {
        panic("This funcion is unimplemented", move(loc));
    }

    struct ok_value {};
    struct err_value {};
    
    class runtime_error
    {
    protected:
        const char* _err = nullptr;
        
    public:
        constexpr runtime_error(const char* error)
            : _err{error}
        {}

        constexpr const char* pretty_error() const
        {
            return _err;
        }
    };

    template <typename Ok, typename Err> requires (!IsSame<Ok, Err>)
    class [[nodiscard("Result type should not be discarded")]] result
    {
    private:
        union
        {
            Ok _ok_data;
            Err _err_data;
        };

        bool _initialized = false;
        
    public:
        constexpr result(const Ok& value)
        requires (CopyConstructible<Ok>)
            : _ok_data{value}, _initialized{true}
        {}

        constexpr result(Ok&& value)
        requires (MoveConstructible<Ok>)
            : _ok_data{move(value)}, _initialized{true}
        {}

        constexpr result(const Err& value)
        requires (CopyConstructible<Err>)
            : _err_data{value}, _initialized{false}
        {}

        constexpr result(Err&& value)
        requires (MoveConstructible<Err>)
            : _err_data{move(value)}, _initialized{false}
        {}

        constexpr result(const result&) = delete;
        constexpr result& operator=(const result&) = delete;

        constexpr result(result&& other)
        {
            swap(_initialized, other._initialized);

            if(_initialized)
            {
                std::construct_at(&_ok_data, move(other._ok_data));
            }
            else
            {
                std::construct_at(&_err_data, move(other._err_data));
            }
        }

        constexpr result& operator=(result&& rhs)
        {
            result _tmp{move(rhs)};
            swap(_initialized, rhs._initialized);

            if (rhs._initialized)
            {
                std::construct_at(&rhs._ok_data, move(_ok_data));
            }
            else
            {
                std::construct_at(&rhs._err_data, move(_err_data));
            }

            swap(_initialized, _tmp._initialized);

            if (_initialized)
            {
                std::construct_at(&_ok_data, move(_tmp._ok_data));
            }
            else
            {
                std::construct_at(&_err_data, move(_tmp._err_data));
            }

            return *this;
        }

        constexpr ~result()
        {
            if (_initialized)
            {
                _ok_data.~Ok();
            }
            else
            {
                _err_data.~Err();
            }
        }

        constexpr bool is_ok() const
        {
            return _initialized;
        }

        explicit constexpr operator bool() const
        {
            return _initialized;
        }

        constexpr Ok unwrap(
            source_location loc = source_location::current())
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                if constexpr (requires (Err _err) {{_err.pretty_error()} -> IsSame<const char*>;})
                {
                    panic(_err_data.pretty_error(), move(loc));
                }
                else
                {
                    panic_type_err<Ok, Err>(move(loc));
                }
            }
        }

        constexpr Ok expect(
            const char* message = "Object was not initialized",
            source_location loc = source_location::current())
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                panic(message, move(loc));
            }
        }

        template <typename... Args>
        constexpr Ok unwrap_or(Args&&... args)
        requires (Constructible<Ok, Args...>)
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                return Ok{forward<Args>(args)...};
            }
        }
        
        constexpr Ok unwrap_or_default()
        requires (DefaultConstructible<Ok>)
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                return Ok{};
            }
        }

        template <typename Func>
        requires (InvocableRet<Ok, Func>)
        constexpr Ok unwrap_or_else(Func&& func)
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                return func();
            }
        }

        constexpr Err unwrap_err(
            source_location loc = source_location::current())
        {
            if (!_initialized)
            {
                return release(_err_data);
            }
            else
            {
                panic_type_err<Err, Ok>(move(loc));
            }
        }

        constexpr Err expect_err(
            const char* message = "Object was initialized",
            source_location loc = source_location::current())
        {
            if (!_initialized)
            {
                return release(_err_data);
            }
            else
            {
                panic(message, move(loc));
            }
        }
    };

    template <typename Ok>
    class [[nodiscard("Option type should not be discarded")]] option
    {
    private:
        union
        {
            Ok _ok_data;
        };

        bool _initialized = false;
        
    public:
        constexpr option(const Ok& value)
        requires (CopyConstructible<Ok>)
            : _ok_data{value}, _initialized{true}
        {}

        constexpr option(Ok&& value)
        requires (MoveConstructible<Ok>)
            : _ok_data{move(value)}, _initialized{true}
        {}

        constexpr option(err_value = {})
            : _initialized{false}
        {}

        constexpr option(option&& other)
            : _initialized{other._initialized}
        {
            if(_initialized)
            {
                std::construct_at(&_ok_data, move(other._ok_data));
            }
        }

        constexpr option(const option&) = delete;
        constexpr option& operator=(const option&) = delete;

        constexpr option& operator=(option&& rhs)
        {
            option _tmp = move(rhs);
            swap(_initialized, rhs._initialized);

            if (rhs._initialized)
            {
                std::construct_at(&rhs._ok_data, move(_ok_data));
            }

            swap(_initialized, _tmp._initialized);

            if (_initialized)
            {
                std::construct_at(&_ok_data, move(_tmp._ok_data));
            }

            return *this;
        }

        constexpr ~option()
        {
            if (_initialized)
            {
                _ok_data.~Ok();
            }
        }

        constexpr bool is_ok() const
        {
            return _initialized;
        }

        explicit constexpr operator bool() const
        {
            return _initialized;
        }

        constexpr Ok unwrap(
            source_location loc = source_location::current())
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                panic_type_err<Ok, void>(move(loc));
            }
        }

        constexpr Ok expect(
            const char* message = "Object was not initialized",
            source_location loc = source_location::current())
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                panic(message, move(loc));
            }
        }

        template <typename... Args>
        constexpr Ok unwrap_or(Args&&... args)
        requires (Constructible<Ok, Args...>)
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                return Ok{forward<Args>(args)...};
            }
        }
        
        constexpr Ok unwrap_or_default()
        requires (DefaultConstructible<Ok>)
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                return Ok{};
            }
        }

        template <typename Func>
        requires (InvocableRet<Ok, Func>)
        constexpr Ok unwrap_or_else(Func&& func)
        {
            if (_initialized)
            {
                return release(_ok_data);
            }
            else
            {
                return func();
            }
        }

        constexpr void unwrap_err(
            source_location loc = source_location::current())
        {
            if (_initialized)
            {
                panic_type_err<void, Ok>(move(loc));
            }
        }

        constexpr void expect_err(
            const char* message = "Object was initialized",
            source_location loc = source_location::current())
        {
            if (_initialized)
            {
                panic(message, move(loc));
            }
        }
    };

    template <typename Err>
    class [[nodiscard("Option type should not be discarded")]] option_err
    {
    private:
        union
        {
            Err _err_data;
        };

        bool _initialized = false;
        
    public:
        constexpr option_err(const Err& value)
        requires (CopyConstructible<Err>)
            : _err_data{value}, _initialized{false}
        {}

        constexpr option_err(Err&& value)
        requires (MoveConstructible<Err>)
            : _err_data{move(value)}, _initialized{false}
        {}

        constexpr option_err(ok_value = {})
            : _initialized{true}
        {}

        constexpr option_err(option_err&& other)
            : _initialized{other._initialized}
        {
            if(!_initialized)
            {
                std::construct_at(&_err_data, move(other._err_data));
            }
        }

        constexpr option_err(const option_err&) = delete;
        constexpr option_err& operator=(const option_err&) = delete;

        constexpr option_err& operator=(option_err&& rhs)
        {
            option_err _tmp = move(rhs);
            swap(_initialized, rhs._initialized);

            if (!rhs._initialized)
            {
                std::construct_at(&rhs._err_data, move(_err_data));
            }

            swap(_initialized, _tmp._initialized);

            if (!_initialized)
            {
                std::construct_at(&_err_data, move(_tmp._err_data));
            }

            return *this;
        }

        constexpr ~option_err()
        {
            if (!_initialized)
            {
                _err_data.~Err();
            }
        }

        constexpr bool is_ok() const
        {
            return _initialized;
        }

        explicit constexpr operator bool() const
        {
            return _initialized;
        }

        constexpr void unwrap(
            source_location loc = source_location::current())
        {
            if (!_initialized)
            {
                if constexpr (requires (Err _err) {{_err.pretty_error()} -> IsSame<const char*>;})
                {
                    panic(_err_data.pretty_error(), move(loc));
                }
                else
                {
                    panic_type_err<void, Err>(move(loc));
                }
            }
        }

        constexpr void expect(
            const char* message = "Object was not initialized",
            source_location loc = source_location::current())
        {
            if (!_initialized)
            {
                panic(message, move(loc));
            }
        }

        constexpr Err unwrap_err(
            source_location loc = source_location::current())
        {
            if (_initialized)
            {
                panic_type_err<Err, void>(move(loc));
            }
            else
            {
                return release(_err_data);
            }
        }

        constexpr Err expect_err(
            const char* message = "Object was initialized",
            source_location loc = source_location::current())
        {
            if (_initialized)
            {
                panic(message, move(loc));
            }
            else
            {
                return release(_err_data);
            }
        }
    };
} // namespace hsd
