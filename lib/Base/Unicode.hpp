#pragma once

#include "Result.hpp"

namespace hsd::unicode
{
    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 1 && sizeof(CharT2) == 1)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            ++src; ++_len; --src_len;
        }

        return _len;
    }

    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 1 && sizeof(CharT2) == 2)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;
        char32 _expr = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xFC00) == 0xD800 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xFC00) == 0xDC00
            ) {
                _expr = static_cast<char32>(
                    (
                        ((static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x3FF) << 10) | 
                        (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3FF)
                    ) + 0x10000
                );
            }
            else
            {
                _expr = static_cast<char32>(src[0]);
            }

            if (_expr < 0x10000)
            {
                if (_expr < 0x80)
                {
                    // ASCII
                    ++_len; src += 1; src_len -= 1;
                }
                else if (_expr < 0x800)
                {
                    // 11-bit
                    _len += 2; src += 1; src_len -= 1;
                }
                else
                {
                    // 16-bit
                    _len += 3; src += 1; src_len -= 1;
                }
            }
            else
            {
                // 21-bit
                _len += 4; src += 2; src_len -= 2;
            }
        }

        return _len;
    }
    
    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 1 && sizeof(CharT2) == 4)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (*src < 0x10000)
            {
                if (*src < 0x80)
                {
                    // ASCII
                    _len += 1; src += 1; src_len -= 1;
                }
                else if (*src < 0x800)
                {
                    // 11-bit
                    _len += 2; src += 1; src_len -= 1;
                }
                else
                {
                    // 16-bit
                    _len += 3; src += 1; src_len -= 1;
                }
            }
            else
            {
                // 21-bit
                _len += 4; src += 1; src_len -= 1;
            }
        }

        return _len;
    }

    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 2 && sizeof(CharT2) == 1)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xF8) == 0xF0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[3]) & 0xC0) == 0x80
            ) {
                // UTF-8: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
                _len += 2; src += 4; src_len -= 4;
            }
            else if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xF0) == 0xE0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0xC0) == 0x80
            ) {
                // UTF-8: 1110xxxx 10xxxxxx 10xxxxxx
                const auto expr = static_cast<char32>(
                    (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x0F) << 12 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3F) << 6  | 
                    (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0x3F)
                );

                if (expr < 0x10000)
                {
                    _len += 1; src += 3; src_len -= 3;
                }
                else
                {
                    _len += 2; src += 3; src_len -= 3;
                }
            }
            else if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xE0) == 0xC0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80
            ) {
                // UTF-8: 110xxxxx 10xxxxxx
                _len += 1; src += 2; src_len -= 3;
            }
            else
            {
                // UTF-8: 0xxxxxxx
                _len += 1; src += 1; src_len -= 1;
            }
        }
        
        return _len;
    }

    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 2 && sizeof(CharT2) == 2)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            ++_len; ++src; --src_len;
        }

        return _len;
    }

    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 2 && sizeof(CharT2) == 4)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (static_cast<add_unsigned_t<CharT2>>(*src) < 0x10000)
            {
                _len += 1; src += 1; src_len -= 1;
            }
            else
            {
                _len += 2; src += 1; src_len -= 1;
            }
        }

        return _len;
    }

    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 4 && sizeof(CharT2) == 1)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xF8) == 0xF0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[3]) & 0xC0) == 0x80
            ) {
                // UTF-8: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
                src += 4; src_len -= 4;
            }
            else if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xF0) == 0xE0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0xC0) == 0x80
            ) {
                // UTF-8: 1110xxxx 10xxxxxx 10xxxxxx
                src += 3; src_len -= 3;
            }
            else if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xE0) == 0xC0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80
            ) {
                // UTF-8: 110xxxxx 10xxxxxx
                src += 2; src_len -= 2;
            }
            else
            {
                // ASCII or invalid UTF-8
                src += 1; src_len -= 1;
            }

            _len++;
        }

        return _len;
    }

    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 4 && sizeof(CharT2) == 2)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xFC00) == 0xD800 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xFC00) == 0xDC00
            )
            {
                // UTF-16: 110110?? 10xxxxxx 10xxxxxx
                src += 2; src_len -= 2;
            }
            else
            {
                // ASCII or invalid UTF-16
                src += 1; src_len -= 1;
            }

            _len++;
        }

        return _len;
    }

    template <typename CharT1, typename CharT2> 
    requires (sizeof(CharT1) == 4 && sizeof(CharT2) == 4)
    static constexpr usize length(const CharT2* src, usize src_len = -1)
    {
        usize _len = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            ++_len; ++src; --src_len;
        }
        
        return _len;
    }

    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 1 && sizeof(CharT2) == 1)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        while (*src != static_cast<CharT2>('\0') && src_len != 0 && size > 1)
        {
            *dest++ = static_cast<CharT1>(*src++);
            --size; --src_len;
        }

        if ((*src != static_cast<CharT2>('\0') && src_len != 0) || size == 0)
        {
            return runtime_error{"Destination too small for conversion"};
        }

        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }

    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 1 && sizeof(CharT2) == 2)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        char32 _expr = 0;

        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xFC00) == 0xD800 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xFC00) == 0xDC00
            ) {
                _expr = static_cast<char32>(
                    (
                        ((static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x3FF) << 10) | 
                        (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3FF)
                    ) + 0x10000
                );
            }
            else
            {
                _expr = static_cast<char32>(src[0]);
            }

            if (_expr < 0x10000)
            {
                if (_expr < 0x80 && size > 1)
                {
                    // ASCII
                    dest[0] = static_cast<CharT1>(_expr);

                    dest += 1; src += 1; size -= 1; src_len -= 1;
                }
                else if (_expr < 0x800 && size > 2)
                {
                    // 11-bit
                    dest[0] = static_cast<CharT1>(0xC0 | ((_expr >> 6) & 0x1F));
                    dest[1] = static_cast<CharT1>(0x80 | (_expr & 0x3F));

                    dest += 2; src += 1; size -= 2; src_len -= 1;
                }
                else if (size > 3)
                {
                    // 16-bit
                    dest[0] = static_cast<CharT1>(0xE0 | ((_expr >> 12) & 0x0F));
                    dest[1] = static_cast<CharT1>(0x80 | ((_expr >> 6) & 0x3F));
                    dest[2] = static_cast<CharT1>(0x80 | (_expr & 0x3F));

                    dest += 3; src += 1; size -= 3; src_len -= 1;
                }
                else
                {
                    return runtime_error{"Destination too small for conversion"};
                }
            }
            else if (size > 4)
            {
                // 21-bit
                dest[0] = static_cast<CharT1>(0xF0 | (_expr >> 18));
                dest[1] = static_cast<CharT1>(0x80 | ((_expr >> 12) & 0x3F));
                dest[2] = static_cast<CharT1>(0x80 | ((_expr >> 6) & 0x3F));
                dest[3] = static_cast<CharT1>(0x80 | (_expr & 0x3F));

                dest += 4; src += 2; size -= 4; src_len -= 2;
            }
            else
            {
                return runtime_error{"Destination too small for conversion"};
            }
        }

        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }
    
    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 1 && sizeof(CharT2) == 4)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (*src < 0x10000)
            {
                if (*src < 0x80 && size > 1)
                {
                    // ASCII
                    dest[0] = static_cast<CharT1>(*src);

                    dest += 1; src += 1; src_len -= 1;
                }
                else if (*src < 0x800 && size > 2)
                {
                    // 11-bit
                    dest[0] = static_cast<CharT1>(0xC0 | ((*src >> 6) & 0x1F));
                    dest[1] = static_cast<CharT1>(0x80 | (*src & 0x3F));

                    dest += 2; src += 1; src_len -= 1;
                }
                else if (size > 3)
                {
                    // 16-bit
                    dest[0] = static_cast<CharT1>(0xE0 | ((*src >> 12) & 0x0F));
                    dest[1] = static_cast<CharT1>(0x80 | ((*src >> 6) & 0x3F));
                    dest[2] = static_cast<CharT1>(0x80 | (*src & 0x3F));

                    dest += 3; src += 1; src_len -= 1;
                }
                else
                {
                    return runtime_error{"Destination too small for conversion"};
                }
            }
            else if (size > 4)
            {
                // 21-bit
                dest[0] = static_cast<CharT1>(0xF0 | (*src >> 18));
                dest[1] = static_cast<CharT1>(0x80 | ((*src >> 12) & 0x3F));
                dest[2] = static_cast<CharT1>(0x80 | ((*src >> 6) & 0x3F));
                dest[3] = static_cast<CharT1>(0x80 | (*src & 0x3F));

                dest += 4; src += 1; src_len -= 1;
            }
            else
            {
                return runtime_error{"Destination too small for conversion"};
            }
        }

        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }

    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 2 && sizeof(CharT2) == 1)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xF8) == 0xF0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[3]) & 0xC0) == 0x80 &&
                size > 2
            ) {
                // UTF-8: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
                const auto expr = static_cast<char32>(
                    (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x07) << 18 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3F) << 12 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0x3F) << 6  | 
                    (static_cast<add_unsigned_t<CharT2>>(src[3]) & 0x3F)
                );

                dest[0] = static_cast<CharT1>(0xD800 | ((expr >> 10) - 0x40));
                dest[1] = static_cast<CharT1>(0xDC00 | (expr & 0x3FF));

                dest += 2; src += 4; size -= 2; src_len -= 4;
            }
            else if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xF0) == 0xE0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0xC0) == 0x80 
            ) {
                // UTF-8: 1110xxxx 10xxxxxx 10xxxxxx
                const auto expr = static_cast<char32>(
                    (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x0F) << 12 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3F) << 6  | 
                    (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0x3F)
                );

                if (expr < 0x10000 && size > 1)
                {
                    dest[0] = static_cast<CharT1>(expr);

                    dest += 1; src += 3; size -= 1; src_len -= 3;
                }
                else if (size > 2)
                {
                    dest[0] = static_cast<CharT1>(0xD800 | (expr >> 10));
                    dest[1] = static_cast<CharT1>(0xDC00 | (expr & 0x3FF));

                    dest += 2; src += 3; size -= 2; src_len -= 3;
                }
                else
                {
                    return runtime_error{"Destination too small for conversion"};
                }
            }
            else if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xE0) == 0xC0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 &&
                size > 1
            ) {
                // UTF-8: 110xxxxx 10xxxxxx
                dest[0] = static_cast<CharT1>(
                    (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x1F) << 6 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3F)
                );

                dest += 1; src += 2; size -= 1; src_len -= 2;
            }
            else if (size > 1)
            {
                // UTF-8: 0xxxxxxx
                dest[0] = static_cast<CharT1>(src[0]);

                dest += 1; src += 1; size -= 1; src_len -= 1;
            }
            else
            {
                return runtime_error{"Destination too small for conversion"};
            }
        }

        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }

    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 2 && sizeof(CharT2) == 2)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        while (*src != static_cast<CharT1>('\0') && src_len != 0 && size > 1)
        {
            *dest++ = static_cast<CharT1>(*src++);
            --size; --src_len;
        }

        if ((*src != static_cast<CharT2>('\0') && src_len != 0) || size == 0)
        {
            return runtime_error{"Destination too small for conversion"};
        }

        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }

    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 2 && sizeof(CharT2) == 4)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        while (*src != static_cast<CharT2>('\0') && src_len != 0)
        {
            if (static_cast<add_unsigned_t<CharT2>>(*src) < 0x10000 && size > 1)
            {
                dest[0] = static_cast<CharT1>(*src);

                dest += 1; src += 1; size -= 1; src_len -= 1;
            }
            else if (size > 2)
            {
                dest[0] = static_cast<CharT1>(
                    0xD800 | (
                        (static_cast<add_unsigned_t<CharT2>>(*src) >> 10) - 0x40
                    )
                );
                dest[1] = static_cast<CharT1>( 
                    0xDC00 | (
                        static_cast<add_unsigned_t<CharT2>>(*src) & 0x3FF
                    )
                );

                dest += 2; src += 1; size -= 2; src_len -= 1;
            }
            else
            {
                return runtime_error{"Destination too small for conversion"};
            }
        }

        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }

    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 4 && sizeof(CharT2) == 1)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        while (*src != static_cast<CharT2>('\0') && src_len != 0 && size > 1)
        {
            if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xF8) == 0xF0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[3]) & 0xC0) == 0x80
            ) {
                // UTF-8: 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
                *dest = static_cast<CharT1>(
                    (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x07) << 18 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3F) << 12 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0x3F) << 6  | 
                    (static_cast<add_unsigned_t<CharT2>>(src[3]) & 0x3F)
                );

                src += 4; src_len -= 4;
            }
            else if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xF0) == 0xE0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80 && 
                (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0xC0) == 0x80
            ) {
                // UTF-8: 1110xxxx 10xxxxxx 10xxxxxx
                *dest = static_cast<CharT1>(
                    (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x0F) << 12 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3F) << 6  | 
                    (static_cast<add_unsigned_t<CharT2>>(src[2]) & 0x3F)
                );
                src += 3; src_len -= 3;
            }
            else if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xE0) == 0xC0 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xC0) == 0x80
            ) {
                // UTF-8: 110xxxxx 10xxxxxx
                *dest = static_cast<CharT1>(
                    (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x1F) << 6 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x3F)
                );

                src += 2; src_len -= 2;
            }
            else
            {
                // ASCII or invalid UTF-8
                *dest = static_cast<CharT1>(src[0]);
                src += 1; src_len -= 1;
            }

            dest++; --size;
        }

        if ((*src != static_cast<CharT2>('\0') && src_len != 0) || size == 0)
        {
            return runtime_error{"Destination too small for conversion"};
        }

        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }

    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 4 && sizeof(CharT2) == 2)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        while (*src != static_cast<CharT2>('\0') && size > 1)
        {
            if (
                (static_cast<add_unsigned_t<CharT2>>(src[0]) & 0xFC00) == 0xD800 && 
                (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0xFC00) == 0xDC00
            )
            {
                // UTF-16: 110110?? 10xxxxxx 10xxxxxx
                *dest = static_cast<CharT1>(
                    ((static_cast<add_unsigned_t<CharT2>>(src[0]) & 0x03FF) << 10 | 
                    (static_cast<add_unsigned_t<CharT2>>(src[1]) & 0x03FF)) + 0x10000
                );

                src += 2; src_len -= 2;
            }
            else
            {
                // ASCII or invalid UTF-16
                *dest = static_cast<CharT1>(src[0]);
                src += 1; src_len -= 1;
            }

            dest++; --size;
        }

        if ((*src != static_cast<CharT2>('\0') && src_len != 0) || size == 0)
        {
            return runtime_error{"Destination too small for conversion"};
        }

        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }

    template <typename CharT1, typename CharT2>
    requires (sizeof(CharT1) == 4 && sizeof(CharT2) == 4)
    static constexpr result<usize, runtime_error> convert_to(
        CharT1* dest, usize size, const CharT2* src, usize src_len = -1)
    {
        while (*src != static_cast<CharT2>('\0') && src_len != 0 && size > 1)
        {
            *dest++ = static_cast<CharT1>(*src++);
            --size; --src_len;
        }

        if ((*src != static_cast<CharT2>('\0') && src_len != 0) || size == 0)
        {
            return runtime_error{"Destination too small for conversion"};
        }
        
        *dest = static_cast<CharT1>('\0');
        return size - 1;
    }
} // namespace hsd::unicode
