#pragma once

#include "TypeTraits.hpp"

// Required to unlock std::construct_at which
// allows us to in-place construct at compile time 
#if __has_include(<bits/stl_construct.h>)
#include <bits/stl_construct.h>
#elif __has_include(<xutility>)
#include <xutility>
#endif

#include <new>

#ifdef HSD_COMPILER_MSVC
#undef min
#undef max
#endif

namespace hsd
{
    template <typename T>
    static constexpr remove_reference_t<T>&& move(T&& val)
    {
        return static_cast<remove_reference_t<T>&&>(val);
    }

    template <typename T>
    static constexpr remove_reference_t<T> release(T&& val)
    {
        return static_cast<remove_reference_t<T>&&>(val);
    }

    template <typename T>
    static constexpr T&& forward(remove_reference_t<T>& val)
    {
        return static_cast<T&&>(val);
    }

    template <typename T>
    static constexpr T&& forward(remove_reference_t<T>&& val)
    {
        return static_cast<T&&>(val);
    }

    template <typename T, typename U = T>
    static constexpr T exchange(T& target, U&& new_val) noexcept
    {
        T tmp = move(target);
        target = forward<U>(new_val);
        return tmp;
    }

    template <typename InIt, typename OutIt>
    static constexpr OutIt move(InIt first, InIt last, OutIt dest)
    {
        while (first != last) 
            *dest++ = move(*first++);

        return dest;
    }

    [[nodiscard]] static constexpr bool is_constant_evaluated()
    {
        return __builtin_is_constant_evaluated();
    }

    template <typename To>
    [[nodiscard]] static constexpr auto bit_cast(auto from)
    {
        return __builtin_bit_cast(To, from);
    }

    template <typename T, typename... Args>
    static consteval void literal_construct(T& dest, Args&&... args)
    {
        dest = T{forward<Args>(args)...};
    }

    template <typename T>
    static constexpr void as_const(T&&) = delete;
    
    template <typename T>
    static constexpr const T& as_const(T& val)
    {
        return val;
    }
    
    template <typename T>
    static constexpr const T* as_const(T* val)
    {
        return val;
    }
    
    template <typename T>
    static constexpr const T& as_const(const T& val)
    {
        return val;
    }
    
    template <typename T>
    static constexpr const T* as_const(const T* val)
    {
        return val;
    }

    template <typename Type>
    static constexpr const Type* addressof(const Type& value)
    {
        return __builtin_addressof(value);
    }

    template <typename Type>
    static constexpr Type* addressof(Type& value)
    {
        return __builtin_addressof(value);
    }

    template <typename T1>
    static constexpr void swap(T1& first, T1& second) noexcept
    {
        auto _tmp = move(first);
        first = move(second);
        second = move(_tmp);
    }

    static constexpr void swap(auto& first, auto& second) noexcept
    {
        auto _tmp = move(first);
        first = move(second);
        second = move(_tmp);
    }
    
    template <typename InIt, typename OutIt>
    static constexpr OutIt copy(InIt first, InIt last, OutIt dest)
    {
        while (first != last)
        { 
            *dest++ = *first++;
        }

        return dest;
    }

    template <typename InIt, typename OutIt>
    static constexpr OutIt copy_n(InIt first, usize n, OutIt dest)
    {
        using value_type = remove_reference_t<decltype(*dest)>;

        for (usize _index = 0; _index != n; ++_index)
        {
            *dest++ = static_cast<value_type>(*first++);
        }
    
        return dest;
    }

    template <typename InIt, typename Pred>
    static constexpr auto find(InIt first, InIt last, Pred&& pred)
    {
        for (; first != last; ++first)
        {
            if (pred(*first)) return first;
        }

        return last;
    }

    template <typename InIt, typename ValueType>
    static constexpr void set(InIt first, InIt last, ValueType value)
    {
        for (; first != last; *first++ = value)
            ;
    }

    template < typename InIt, typename OutIt, typename Pred >
    static constexpr OutIt copy_if(InIt first, InIt last, OutIt dest, Pred pred)
    {
        while (first != last) 
        {
            if (pred(*first))
            {
                *dest++ = *first;
            }
                
            ++first;
        }
        return dest;
    }

    template <typename Elem, usize Count>
    static constexpr Elem* begin(Elem (&arr)[Count])
    {
        return static_cast<Elem*>(arr);
    }

    template <typename Elem, usize Count>
    static constexpr Elem* end(Elem (&arr)[Count])
    {
        return static_cast<Elem*>(arr) + Count;
    }

    template <typename Elem, usize Count>
    static constexpr const Elem* begin(const Elem (&arr)[Count])
    {
        return static_cast<const Elem*>(arr);
    }


    template <typename Elem, usize Count>
    static constexpr const Elem* end(const Elem (&arr)[Count])
    {
        return static_cast<const Elem*>(arr) + Count;
    }

    template<typename T, typename... Args>
    static constexpr auto construct_at(T* ptr, Args&&... args)
    {
        return std::construct_at(ptr, forward<Args>(args)...);
    }

    static inline void* new_allocate(usize size, [[maybe_unused]] usize alignment)
    {
        #ifdef __cpp_aligned_new
        return ::operator new(
            size, static_cast<std::align_val_t>(alignment), std::nothrow
        );
        #else
        return ::operator new(_alloc_size, std::nothrow);
        #endif
    }

    static inline void delete_deallocate(
        void* ptr, [[maybe_unused]] usize size, usize alignment)
    {
        #ifdef __cpp_sized_deallocation
        ::operator delete(ptr, size, static_cast<std::align_val_t>(alignment));
        #else
        ::operator delete(ptr, static_cast<std::align_val_t>(alignment));
        #endif
    }

    static constexpr void for_each(const auto& arr, auto&& func)
    {
        for (const auto& _iter : arr)
        {
            func(_iter);
        }
    }

    static constexpr void for_each(auto& arr, auto&& func)
    {
        for (auto& _iter : arr)
        {
            func(_iter);
        }
    }
} // namespace hsd
