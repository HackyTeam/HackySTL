#pragma once

#include "StackArray.hpp"
#include "../Serialize/_PrinterImpl.hpp"

namespace hsd
{
    template <usize N>
    class bitset
    {
    protected:
        using internal_type = u32;

        static constexpr internal_type _div_val = 5;
        static constexpr internal_type _mod_val = 31;
        static constexpr internal_type _size_val = 32;
        static constexpr internal_type _one = 1;
        
        static constexpr internal_type _set_val = -1;
        static constexpr internal_type _len_mod = (N & _mod_val);
        static constexpr internal_type _arr_sz = (N >> _div_val) + (_len_mod != 0);

        stack_array<internal_type, _arr_sz> _arr{};
        
        static constexpr internal_type _msb_val = 
            (_len_mod != 0) ? ((_one << _len_mod) - _one) : _set_val;

        template <usize M>
        friend class bitset;

    private:
        struct bad_access
        {
            constexpr const char* pretty_error() const
            {
                return "Tried to access an element out of bounds";
            }
        };

        template <typename Val>
        class bit_ref
        {
        private:
            Val* _data = nullptr;
            usize _arr_size = 0;
            usize _idx = 0;

            friend class bitset<N>;

            constexpr bit_ref(Val* data, usize array_size, usize index)
                : _data{data}, _arr_size{array_size}, _idx{index}
            {}

        public:
            constexpr bit_ref() = default;

            constexpr bit_ref(const bit_ref&) = default;
            constexpr bit_ref(bit_ref&&) = default;

            constexpr bit_ref& operator=(const bit_ref&) = default;
            constexpr bit_ref& operator=(bit_ref&&) = default;

            constexpr bit_ref& operator=(bool rhs)
            requires (!hsd::IsConst<Val>)
            {
                if (rhs == true)
                {
                    _data[_arr_size - 1 - (_idx >> _div_val)] |= _one << (_idx & _mod_val);
                }
                else
                {
                    _data[_arr_size - 1 - (_idx >> _div_val)] &= ~(_one << (_idx & _mod_val));
                }

                return *this;
            }

            constexpr operator bool() const
            {
                return ((_data[_arr_size - 1 - (_idx >> _div_val)] >> (_idx & _mod_val)) & _one);
            }

            constexpr bool operator~() const
            {
                return !static_cast<bool>(*this);
            }

            constexpr bit_ref& flip()
            requires (!hsd::IsConst<Val>)
            {
                return (*this = ~(*this));
            }
        };

    public:
        constexpr bitset() = default;

        constexpr bitset(const bitset&) = default;
        constexpr bitset(bitset&&) = default;

        constexpr bitset& operator=(const bitset&) = default;
        constexpr bitset& operator=(bitset&&) = default;
        
        constexpr bitset(u64 other)
        {
            _arr[_arr.size() - 1] = other & 0xFFFF'FFFF;
            
            if  (_arr.size() >= 2)
            {
                _arr[_arr.size() - 2] = (other >> 32) & 0xFFFF'FFFF;
            } 

            _arr[0] &= _msb_val;
        }
        constexpr bit_ref<internal_type> operator[](usize index)
        {
            return {_arr.data(), _arr.size(), index};
        }

        constexpr bit_ref<const internal_type> operator[](usize index) const
        {
            return {_arr.data(), _arr.size(), index};
        }

        constexpr usize size() const
        {
            return N;
        }

        constexpr auto test(usize index) const
            -> result<bool, bad_access>
        {
            if (index >= N)
            {
                return bad_access{};
            }

            return static_cast<bool>(bit_ref<const internal_type>{_arr.data(), _arr.size(), index});
        }

        constexpr bool any() const
        {
            for (internal_type _val : _arr)
            {
                if (_val != 0)
                    return true;
            }

            return false;
        }

        constexpr bool none() const
        {
            return !any();
        }

        constexpr bool all() const
        {
            usize _idx = 1;
            
            if (_arr[0] != _msb_val)
                return false;

            for (; _idx != _arr.size(); ++_idx)
            {
                if (_arr[_idx] != _set_val)
                    return false;
            }

            return true;
        }

        constexpr usize count() const
        {
            usize _cnt = 0;

            for (auto _val : _arr)
            {
                _cnt += __builtin_popcount(_val);
            }
            
            return _cnt;
        }

        constexpr bitset& reset()
        {
            _arr.fill(0);
            return *this;
        }

        constexpr bitset& reset(usize index)
        {
            (*this)[index] = 0;
            return *this;
        }

        constexpr bitset& set()
        {
            usize _idx = 1;
            _arr[0] = _msb_val;

            for (; _idx != _arr.size(); ++_idx)
            {
                _arr[_idx] = _set_val;
            }

            return *this;
        }

        constexpr bitset& flip()
        {
            usize _idx = 1;
            _arr[0] = (~_arr[0]) & _msb_val;

            for (; _idx != _arr.size(); ++_idx)
            {
                _arr[_idx] = ~_arr[_idx];
            }

            return *this;
        }

        constexpr bitset& flip(usize index)
        {
            (*this)[index].flip();
            return *this;
        }

        constexpr auto set(usize index, bool value = true)
            -> result<reference<bitset>, bad_access>
        {
            if (index >= N)
            {
                return bad_access{};
            }

            (*this)[index] = value;
            return {*this};
        }

        constexpr const auto& get_underlying_array() const
        {
            return _arr;
        }

        constexpr auto get_underlying_array_size() const
        {
            return _arr.size();
        }

        constexpr bool operator==(const bitset& rhs) const
        {
            for (usize _idx = 0; _idx < _arr.size(); ++_idx)
            {
                if (_arr[_idx] != rhs._arr[_idx])
                    return false;
            }

            return true;
        }

        constexpr bool operator!=(const bitset& rhs) const
        {
            return !(*this == rhs);
        }

        constexpr bitset& operator&=(const bitset& rhs)
        {
            for (usize _idx = 0; _idx < _arr.size(); ++_idx)
            {
                _arr[_idx] &= rhs._arr[_idx];
            }

            return *this;
        }

        constexpr bitset& operator|=(const bitset& rhs)
        {
            for (usize _idx = 0; _idx < _arr.size(); ++_idx)
            {
                _arr[_idx] |= rhs._arr[_idx];
            }

            return *this;
        }

        constexpr bitset& operator^=(const bitset& rhs)
        {
            for (usize _idx = 0; _idx < _arr.size(); ++_idx)
            {
                _arr[_idx] ^= rhs._arr[_idx];
            }

            return *this;
        }

        constexpr bitset operator&(const bitset& rhs) const 
        {
            return bitset<N>{*this} &= rhs;
        }

        constexpr bitset operator|(const bitset& rhs) const
        {
            return bitset<N>{*this} |= rhs;
        }

        constexpr bitset operator^(const bitset& rhs) const
        {
            return bitset<N>{*this} ^= rhs;
        }

        constexpr bitset& operator<<=(usize pos)
        {
            if (pos >= N)
            {
                return reset();
            }
            else if (pos == 0)
            {
                return *this;
            }

            // assume 9, _to_idx = 1; _rest_idx = 1

            //                0        1        2
            // bitset<20> 0000bbbb bbbbbbbb bbbbbbbb

            usize _to_pos = pos >> _div_val;
            usize _rest_pos = pos & _mod_val;
            
            if (_to_pos != 0)
            {
                for (usize _idx = 0; _idx < _arr.size() - _to_pos; ++_idx)
                {
                    _arr[_idx] = _arr[_idx + _to_pos];
                }

                for (usize _idx = _arr.size() - _to_pos; _idx < _arr.size(); ++_idx)
                {
                    _arr[_idx] = 0;
                }
            }

            if (_rest_pos != 0)
            {
                internal_type _prev_carry = 0;
                internal_type _carry = 0;

                for (usize _idx = _arr.size() - _to_pos; _idx > 0; --_idx)
                {
                    const internal_type _shf_val = 
                        ((_one << _rest_pos) - _one) << (_size_val - _rest_pos);

                    _carry = (_shf_val & _arr[_idx - 1]) >> (_size_val - _rest_pos);
                    _arr[_idx - 1] <<= _rest_pos;
                    _arr[_idx - 1] |= _prev_carry;
                    _prev_carry = _carry;
                }
            }

            _arr[0] &= _msb_val;
            return *this;
        }

        constexpr bitset& operator>>=(usize pos)
        {
            if (pos >= N)
            {
                return reset();
            }
            else if (pos == 0)
            {
                return *this;
            }

            // assume 9, _to_idx = 1; _rest_idx = 1 

            //                0        1        2
            // bitset<20> 0000bbbb bbbbbbbb bbbbbbbb

            usize _to_pos = pos >> _div_val;
            usize _rest_pos = pos & _mod_val;
            
            if (_to_pos != 0)
            {
                for (usize _idx = _arr.size(); _idx > _to_pos; --_idx)
                {
                    _arr[_idx - 1] = _arr[_idx - _to_pos - 1];
                }

                _to_pos = (_arr.size() > _to_pos) ? _to_pos : _arr.size();

                for (usize _idx = 0; _idx < _to_pos; ++_idx)
                {
                    _arr[_idx] = 0;
                }
            }

            if (_rest_pos != 0)
            {
                internal_type _prev_carry = 0;
                internal_type _carry = 0;

                for (usize _idx = _to_pos; _idx < _arr.size(); ++_idx)
                {
                    const internal_type _shf_val = (_one << _rest_pos) - _one;

                    _carry = (_shf_val & _arr[_idx]) << (_size_val - _rest_pos);
                    _arr[_idx] >>= _rest_pos;
                    _arr[_idx] |= _prev_carry;
                    _prev_carry = _carry;
                }
            }

            return *this;
        }

        constexpr bitset operator<<(usize pos) const
        {
            return bitset<N>{*this} <<= pos;
        }

        constexpr bitset operator>>(usize pos) const
        {
            return bitset<N>{*this} >>= pos;
        }

        constexpr bitset operator~() const
        {
            return bitset<N>{*this}.flip();
        }

        template <usize M>
        constexpr operator bitset<M>() const
        {
            bitset<M> _new_val;

            usize _min_sz = (_arr.size() < _new_val._arr.size()) ? _arr.size() : _new_val._arr.size();

            for (usize _idx = 1; _idx < _min_sz + 1; ++_idx)
            {
                _new_val._arr[_new_val._arr.size() - _idx] = _arr[_arr.size() - _idx];
            }

            _new_val._arr[0] &= _new_val._msb_val;
            return _new_val;
        }
    };

    template <format_output fmt, typename CharT, usize N>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, const bitset<N>& val)
    {
        using FmtType = decltype(fmt);

        auto _arr = [&val]
        {
            if constexpr (fmt.tag == FmtType::none)
            {
                to_string_detail::to_string_value_for<CharT, N> _buf;

                for (usize _idx = N; _idx > 0; --_idx)
                {
                    _buf.push(static_cast<CharT>((val[_idx - 1]) ? '1' : '0'));
                }

                return _buf;
            }
            else if constexpr ((fmt.tag & FmtType::prec) == FmtType::prec)
            {
                return option_err<runtime_error>{"Precision not allowed for integral type"};
            }
            else if constexpr ((fmt.tag & FmtType::hex) == FmtType::hex)
            {
                const auto& _copy = val.get_underlying_array();
                using CopyType = remove_cvref_t<decltype(_copy)>;

                to_string_detail::to_string_value_for<CharT, CopyType::size() * 2 + 2> _buf;

                constexpr const CharT _hex_table[16] = {
                    '0', '1', '2', '3', '4', '5', '6', '7', 
                    '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
                };

                _buf.push('0');
                _buf.push('x');

                for (usize _idx = 0; _idx < _copy.size(); ++_idx)
                {
                    _buf.push(_hex_table[(_copy[_idx] >> 28)]);
                    _buf.push(_hex_table[(_copy[_idx] >> 24) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 20) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 16) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 12) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 8) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 4) & 15]);
                    _buf.push(_hex_table[_copy[_idx] & 15]);
                }

                return _buf;
            }
            else
            {
                return option_err<runtime_error>{"Unknown format tag for integral type"};
            }
        }();

        if constexpr (IsSame<decltype(_arr), option_err<runtime_error>>)
        {
            return _arr.unwrap_err();
        }
        else
        {
            usize _write_sz = (_arr.size() + fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

            if (_write_sz > sz)
            {
                return runtime_error{"Buffer too small for printing"};
            }
    
            str = _print_to_impl(str, fmt.format);
            str = _print_to_impl(str, fmt.style);
            str = _print_to_impl(str, _arr) + 1;
            str = _print_to_impl(str, fmt.reset);

            return _write_sz;
        }
    }
} // namespace hsd