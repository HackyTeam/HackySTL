#pragma once

#include "../Base/Allocator.hpp"
#include "../Wrappers/Reference.hpp"

namespace hsd 
{
    template <typename T, typename Allocator = allocator>
    class deque
    {
    private:
        using alloc_type = Allocator;
        alloc_type _alloc;
        T* _data = nullptr;
        usize _capacity = 0;
        usize _size = 0;

        class iterator
        {
        private:
            T* _data = nullptr;
            usize _current = 0;
            usize _capacity = 0;
            friend class deque<T, Allocator>;

            constexpr iterator _get_previous() const
            {
                return {_data, _current - 1, _capacity};
            }

            constexpr T* _get_address()
            {
                return (_data + ((_current + _capacity) % _capacity));
            }

            constexpr const T* _get_address() const
            {
                return (_data + ((_current + _capacity) % _capacity));
            }

            constexpr iterator(T* data, usize current, usize capacity)
                : _data{data}, _current{current}, _capacity{capacity}
            {}

            usize operator-(const iterator rhs) const
            {
                return _current - rhs._current;
            }

        public:
            constexpr iterator() = default;

            constexpr friend bool operator==(const iterator& lhs, const iterator& rhs)
            {
                return (
                    lhs._data == lhs._data && 
                    lhs._current == rhs._current
                );
            }

            constexpr friend bool operator!=(const iterator& lhs, const iterator& rhs)
            {
                return lhs._current != rhs._current;
            }

            constexpr auto& operator++()
            {
                _current++;
                return *this;
            }

            constexpr auto& operator--()
            {
                _current--;
                return *this;
            }

            constexpr iterator operator++(i32)
            {
                iterator tmp = *this;
                operator++();
                return tmp;
            }

            constexpr iterator operator--(i32)
            {
                iterator tmp = *this;
                operator--();
                return tmp;
            }

            constexpr auto& operator*()
            {
                return *_get_address();
            }

            constexpr const auto& operator*() const
            {
                return *_get_address();
            }

            constexpr auto* operator->()
            {
                return _get_address();
            }

            constexpr const auto* operator->() const
            {
                return _get_address();
            }
        };

        struct bad_access
        {
            constexpr const char* pretty_error() const
            {
                return "Tried to access an element out of bounds";
            }
        };

        iterator _front = {};
        iterator _back = {};

    public:
        using value_type = T;
        using const_iterator = const iterator;

        constexpr ~deque()
        {
            for (auto _idx = begin(); _idx != end(); ++_idx)
                _idx->~T();

            _alloc.deallocate(_data, _capacity).unwrap();
        }

        constexpr deque()
        requires (DefaultConstructible<alloc_type>) = default;

        constexpr deque(usize size)
        requires (DefaultConstructible<alloc_type>)
            : _alloc{}
        {
            resize(size);
        }

        constexpr deque(const alloc_type& alloc)
            : _alloc{alloc}
        {}

        constexpr deque(usize size, const alloc_type& alloc)
            : _alloc{alloc}
        {
            resize(size);
        }

        constexpr deque(const deque& other)
            : _alloc{other._alloc}, 
            _capacity{other._capacity}, 
            _size{other._size}
        {
            _data = _alloc
                .template allocate<T>(other._capacity)
                .unwrap();

            for (usize _index = 0; _index < _size; ++_index)
                construct_at(&_data[_index], other[_index]);

            _front = {_data, 0u, _capacity};
            _back = {_data, _size, _capacity};
        }

        constexpr deque(deque&& other)
            : _alloc{move(other._alloc)},
            _data{exchange(other._data, nullptr)},
            _capacity{exchange(other._capacity, 0u)},
            _size{exchange(other._size, 0u)},
            _front{exchange(other._front, iterator{})},
            _back{exchange(other._back, iterator{})}
        {}

        template <usize N>
        constexpr deque(const T (&arr)[N])
        requires (DefaultConstructible<alloc_type>)
            : _capacity{N}, _size{N}
        {
            _data = _alloc
                .template allocate<T>(N)
                .unwrap();

            for (usize _index = 0; _index < _size; ++_index)
                _alloc.construct_at(&_data[_index], arr[_index]);

            _front = {_data, 0u, _capacity};
            _back = {_data, _size, _capacity};
        }

        template <usize N>
        constexpr deque(T (&&arr)[N])
        requires (DefaultConstructible<alloc_type>)
            : _capacity{N}, _size{N}
        {
            _data = _alloc
                .template allocate<T>(N)
                .unwrap();

            for (usize _index = 0; _index < _size; ++_index)
                construct_at(&_data[_index], move(arr[_index]));

            _front = {_data, 0u, _capacity};
            _back = {_data, _size, _capacity};
        }

        template <usize N>
        constexpr deque& operator=(const T (&arr)[N])
        requires (DefaultConstructible<alloc_type>)
        {
            clear();

            if (_capacity < N)
            {
                reserve(N);
            }
            
            for (usize _index = 0; _index < N; ++_index)
            {
                construct_at(&_data[_index], arr[_index]);
            }
                
            _size = N;
            _front = {_data, 0u, _capacity};
            _back = {_data, _size, _capacity};

            return *this;
        }

        template <usize N>
        constexpr deque& operator=(T (&&arr)[N])
        requires (DefaultConstructible<alloc_type>)
        {
            clear();

            if (_capacity < N)
            {
                reserve(N);
            }
            
            for (usize _index = 0; _index < N; ++_index)
            {
                construct_at(&_data[_index], move(arr[_index]));
            }
                
            _size = N;
            _front = {_data, 0u, _capacity};
            _back = {_data, _size, _capacity};

            return *this;
        }

        constexpr deque& operator=(const deque& rhs)
        {
            clear();
            _alloc = rhs._alloc;
            reserve(rhs._size);
            
            for (usize _index = 0; _index < rhs._size; ++_index)
            {
                construct_at(&_data[_index], rhs[_index]);
            }
                
            _size = rhs._size;
            _front = {_data, 0u, _capacity};
            _back = {_data, _size, _capacity};

            return *this;
        }

        constexpr deque& operator=(deque&& rhs)
        {
            _alloc = exchange(rhs._alloc, _alloc);
            _data = exchange(rhs._data, _data);
            _capacity = exchange(rhs._capacity, _capacity);
            _size = exchange(rhs._size, _size);
            _front = exchange(rhs._front, _front);
            _back = exchange(rhs._back, _back);

            return *this;
        }

        constexpr void resize(usize new_size)
        {
            if (new_size > _capacity)
            {
                // To handle _capacity = 0 case
                usize _new_capacity = _capacity ? _capacity : 1;
                
                while (_new_capacity < new_size)
                    _new_capacity += (_new_capacity + 1) / 2;

                T* _new_buf = _alloc
                    .template allocate<T>(_new_capacity)
                    .unwrap();
                
                usize _index = 0;

                for (; _index < _size; ++_index)
                {
                    auto& _value = at_unchecked(_index);
                    construct_at(&_new_buf[_index], move(_value));
                    _value.~T();
                }
                for (; _index < new_size; ++_index)
                {
                    construct_at(&_new_buf[_index]);
                }

                _alloc.deallocate(_data, _capacity).unwrap();
                _data = _new_buf;
                _capacity = _new_capacity;
                _size = new_size;
                _front = {_data, 0u, _capacity};
                _back = {_data, _size, _capacity};
            }
            else if (new_size > _size)
            {
                for (usize _index = _size; _index < new_size; ++_index)
                {
                    ++_back;
                    construct_at(addressof(at_unchecked(_index)));
                }
                
                _size = new_size;
            }
            else if (new_size < _size)
            {
                for (usize _index = _size; _index > new_size; --_index)
                {
                    --_back;
                    at_unchecked(_index - 1).~T();
                }
                
                _size = new_size;
            }
        }

        constexpr void reserve(usize new_cap)
        {
            if (new_cap > _capacity)
            {
                // To handle _capacity = 0 case
                usize _new_capacity = _capacity ? _capacity : 1;

                while (_new_capacity < new_cap)
                    _new_capacity += (_new_capacity + 1) / 2;

                T* _new_buf = _alloc
                    .template allocate<T>(_new_capacity)
                    .unwrap();

                for (usize _index = 0; _index < _size; ++_index)
                {
                    auto& _value = at_unchecked(_index);
                    construct_at(&_new_buf[_index], move(_value));
                    _value.~T();
                }

                _alloc
                .deallocate(_data, _capacity)
                .unwrap();
                
                _data = _new_buf;
                _capacity = _new_capacity;
                _front = {_data, 0u, _capacity};
                _back = {_data, _size, _capacity};
            }
        }

        constexpr auto erase_for(const_iterator from, const_iterator to)
            -> result<iterator, bad_access>
        {
            if (from._data < _front._data || to._data > _back._data || (to - from) > _size)
                return bad_access{};

            _size -= static_cast<usize>(to - from);

            if (from == _back)
            {
                for (usize _idx = 0; _idx < static_cast<usize>(to - from); ++_idx)
                {
                    pop_back();
                }

                return end();
            }

            iterator _from_it = from;
            iterator _to_it = to;

            for (; _to_it != _back; ++_to_it, ++_from_it)
            {
                *_from_it = move(*_to_it);
            }

            _back = _from_it;
            return from;
        }

        constexpr auto erase(const_iterator pos)
        {
            iterator _pos = pos;
            return erase_for(pos, ++_pos);
        }

        constexpr void clear()
        {
            for (usize _index = _size; _index > 0; --_index)
                at_unchecked(_index - 1).~T();
                
            _front._current = 0;
            _back._current = 0;
            _size = 0;
        }

        template <typename... Args>
        constexpr void emplace_back(Args&&... args)
        {
            reserve(_size + 1);
            construct_at(_back._get_address(), forward<Args>(args)...);
            ++_size;
            ++_back;
        }

        template <typename... Args>
        constexpr void emplace_front(Args&&... args)
        {
            reserve(_size + 1);
            construct_at((--_front)._get_address(), forward<Args>(args)...);
            ++_size;
        }

        constexpr void push_back(const T& val)
        {
            emplace_back(val);
        }

        constexpr void push_back(T&& val)
        {
            emplace_back(move(val));
        }

        constexpr void push_front(const T& val)
        {
            emplace_front(val);
        }

        constexpr void push_front(T&& val)
        {
            emplace_front(move(val));
        }

        constexpr void pop_back() noexcept
        {
            if(_size > 0)
            {
                at_unchecked(_size - 1).~T();
                --_back;
                --_size;
            }
        }

        constexpr void pop_front() noexcept
        {
            if(_size > 0)
            {
                at_unchecked(0).~T();
                ++_front;
                --_size;
            }
        }

        constexpr usize size() const
        {
            return _size;
        }

        constexpr usize capacity() const
        {
            return _capacity;
        }

        constexpr Allocator get_allocator() const
        {
            return _alloc;
        }

        constexpr auto& operator[](usize index)
        {
            return at_unchecked(index);
        }

        constexpr auto& operator[](usize index) const
        {
            return at_unchecked(index);
        }

        constexpr auto& front()
        {
            return *begin();
        }

        constexpr auto& front() const
        {
            return *begin();
        }

        constexpr auto& back()
        {
            return *(end()._get_previous());
        }

        constexpr auto& back() const
        {
            return *(end()._get_previous());
        }

        constexpr auto at(usize index)
            -> result<reference<T>, bad_access>
        {
            if (index >= _size) return bad_access{};

            return {at_unchecked(index)};
        }

        constexpr auto at(usize index) const
            -> result<reference<const T>, bad_access>
        {
            if (index >= _size) return bad_access{};

            return {at_unchecked(index)};
        }

        constexpr auto& at_unchecked(usize index)
        {
            usize _idx = (_front._current + _front._capacity + index) % _capacity;

            return _data[_idx];
        }

        constexpr const auto& at_unchecked(usize index) const
        {
            usize _idx = (_front._current + _front._capacity + index) % _capacity;

            return _data[_idx];
        }

        constexpr iterator begin()
        {
            return _front;
        }

        constexpr const_iterator begin() const
        {
            return _front;
        }

        constexpr const_iterator cbegin() const
        {
            return _front;
        }

        constexpr iterator rbegin()
        {
            return end()._get_previous();
        }

        constexpr const_iterator rbegin() const
        {
            return end()._get_previous();
        }

        constexpr iterator end()
        {
            return _back;
        }

        constexpr const_iterator end() const
        {
            return _back;
        }

        constexpr const_iterator cend() const
        {
            return _back;
        }

        constexpr iterator rend()
        {
            return begin()._get_previous();
        }

        constexpr const_iterator rend() const
        {
            return begin()._get_previous();
        }
    };
} // namespace hsd