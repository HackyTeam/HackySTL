#include "Bitset.hpp"

namespace hsd
{
    template <usize N>
    class integer : public bitset<N>
    {
    private:
        struct division_error
        {
            constexpr const char* pretty_error() const
            {
                return "Tried to divide by 0";
            }
        };

        struct division_result
        {
            integer quotient;
            integer reminder;
        };

        using base_type = bitset<N>;

        template <usize M>
        friend class integer;

        constexpr auto _split_number(hsd::usize mid) const
        {
            using RetType = hsd::pair<integer<N>, integer<N>>;
            integer<N> _lhs, _rhs;

            for (hsd::usize _idx = 0; _idx < mid; ++_idx)
            {
                _lhs <<= 32;
                _lhs |= this->_arr[_idx];
            }

            for (hsd::usize _idx = mid; _idx < this->_arr.size(); ++_idx)
            {
                _rhs <<= 32;
                _rhs |= this->_arr[_idx];
            }

            return RetType{_lhs, _rhs};
        }

        static constexpr auto _karatusba_mult(const integer<N>& lhs, const integer<N>& rhs)
        {
            using IntegerType = integer<N>;

            constexpr IntegerType _max_num = 0xFFFF'FFFF;

            if (lhs <= _max_num)
            {
                return rhs * lhs.get_last_byte();
            }
            else if (rhs <= _max_num)
            {
                return lhs * rhs.get_last_byte();
            }

            const auto _rhs_size = rhs.num_size();
            const auto _lhs_size = lhs.num_size();

            const auto _max = (_rhs_size > _lhs_size) ? _rhs_size : _lhs_size;
            auto _mid = _max / 2;

            const auto [_high_lhs, _low_lhs] = lhs._split_number(_mid + (lhs.get_underlying_array_size() - _lhs_size));
            const auto [_high_rhs, _low_rhs] = rhs._split_number(_mid + (rhs.get_underlying_array_size() - _rhs_size));

            _mid = _max - _mid;

            const auto _z0 = _karatusba_mult(_low_lhs, _low_rhs);
            const auto _z1 = _karatusba_mult(_low_lhs + _high_lhs, _low_rhs + _high_rhs);
            const auto _z2 = _karatusba_mult(_high_lhs, _high_rhs);

            return IntegerType{_z2 << (32 * (_mid * 2))} + IntegerType{(_z1 - _z2 - _z0) << (32 * _mid)} + _z0;
        }

    public:
        constexpr integer() = default;

        constexpr integer(const bitset<N>& other)
            : bitset<N>{other}
        {}

        constexpr integer(bitset<N>&& other)
            : bitset<N>{forward<bitset<N>>(other)}
        {}

        constexpr integer(u64 _value)
            : bitset<N>{_value}
        {}

        constexpr const bitset<N>& to_bitset() const
        {
            return *this;
        }

        constexpr auto get_last_byte() const
        {
            return this->_arr[this->_arr.size() - 1];
        }

        constexpr usize num_size() const
        {
            for (usize _idx = 0; _idx < this->_arr.size(); ++_idx)
            {
                if (this->_arr[_idx] != 0)
                {
                    return this->_arr.size() - _idx;
                }
            }

            return 0;
        }

        constexpr integer& operator=(const bitset<N>& other)
        {
            static_cast<bitset<N>&>(*this) = other;
            return *this;
        }

        constexpr integer& operator=(bitset<N>&& other)
        {
            static_cast<bitset<N>&>(*this) = forward<bitset<N>>(other);
            return *this;
        }

        constexpr integer& operator+=(const integer& rhs)
        {
            u64 _val = 0;

            for (usize _idx = this->_arr.size(); _idx > 0; --_idx)
            {
                _val = (_val >> this->_size_val);
                _val += this->_arr[_idx - 1];
                _val += rhs._arr[_idx - 1];
                this->_arr[_idx - 1] = static_cast<u32>(_val);
            }

            this->_arr[0] &= this->_msb_val;
            return *this;
        } 

        constexpr integer operator+(const integer& rhs) const
        {
            return integer{*this} += rhs;
        } 

        constexpr integer& operator-=(const integer& rhs)
        {
            if (*this < rhs)
            {
                integer _new_val = rhs - *this - 1;
                this->set();

                return *this -= _new_val;
            }

            bool _overflow = false;

            for (usize _idx = this->_arr.size(); _idx > 0; --_idx)
            {
                if (this->_arr[_idx - 1] < (rhs._arr[_idx - 1]) && _overflow == false)
                {
                    _overflow = true;
                    this->_arr[_idx - 1] -= rhs._arr[_idx - 1];
                }
                else
                {
                    bool _will_overflow = this->_arr[_idx - 1] < (rhs._arr[_idx - 1] + _overflow);
                    this->_arr[_idx - 1] -= rhs._arr[_idx - 1] + _overflow;
                    _overflow = _will_overflow;
                }
            }

            this->_arr[0] &= this->_msb_val;
            return *this;
        } 

        constexpr integer operator-(const integer& rhs) const
        {
            return integer{*this} -= rhs;
        }

        constexpr integer& operator*=(u32 rhs)
        {
            u64 _val = 0, _carry = 0;

            for (usize _idx = this->_arr.size(); _idx > 0; --_idx)
            {
                _val = this->_arr[_idx - 1];
                _val *= rhs;
                _val += _carry;

                _carry = (_val >> this->_size_val);
                this->_arr[_idx - 1] = static_cast<u32>(_val);
            }

            this->_arr[0] &= this->_msb_val;
            return *this;
        }

        constexpr integer operator*(u32 rhs) const
        {
            return integer{*this} *= rhs;
        }

        constexpr integer& operator*=(const integer& rhs)
        {
            *this = _karatusba_mult(*this, rhs);
            return *this;
        } 

        constexpr integer operator*(const integer& rhs) const
        {
            return _karatusba_mult(*this, rhs);
        } 

        constexpr i32 operator<=>(const integer& rhs) const
        {
            for (usize _idx = 0; _idx < rhs._arr.size(); ++_idx)
            {
                if (this->_arr[_idx] < rhs._arr[_idx])
                {
                    return -1;
                }
                else if (this->_arr[_idx] > rhs._arr[_idx])
                {
                    return 1;
                }
            }

            return 0;
        }

        constexpr bool operator==(const integer& rhs) const
        {
            return operator<=>(rhs) == 0;
        }

        constexpr auto divide_with_reminder(const integer& rhs) const
            -> result<division_result, division_error>
        {
            if (rhs.none() == true)
            {
                return division_error{};
            }

            division_result _result{};

            for (usize _idx = N; _idx > 0; --_idx)
            {
                _result.reminder <<= 1;
                _result.quotient <<= 1;
                _result.reminder.set(0, this->test(_idx - 1).unwrap()).unwrap();

                if (_result.reminder >= rhs)
                {
                    _result.reminder -= rhs;
                    _result.quotient.set(0).unwrap();
                }
            }

            return {move(_result)};
        }

        constexpr integer operator/(const integer& rhs) const
        {
            return divide_with_reminder(rhs).unwrap().quotient;
        }

        constexpr integer& operator/=(const integer& rhs)
        {
            *this = move(*this / rhs);
            return *this;
        }

        constexpr integer operator%(const integer& rhs) const
        {
            return divide_with_reminder(rhs).unwrap().reminder;
        }

        constexpr integer& operator%=(const integer& rhs)
        {
            *this = move(*this % rhs);
            return *this;
        }

        template <usize M>
        constexpr operator integer<M>() const
        {
            integer<M> _new_val;

            static_cast<bitset<M>&>(_new_val) = static_cast<const bitset<N>&>(*this);

            return _new_val;
        }

        constexpr integer& operator++()
        {
            *this += 1;
            return *this;
        }

        constexpr integer operator++(i32)
        {
            integer _copy = *this;
            *this += 1;
            return _copy;
        }

        constexpr integer& operator--()
        {
            *this -= 1;
            return *this;
        }

        constexpr integer operator--(i32)
        {
            integer _copy = *this;
            *this -= 1;
            return _copy;
        }
    };

    template <format_output fmt, typename CharT, usize N>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, const integer<N>& val)
    {
        using FmtType = decltype(fmt);

        auto _arr = [&val]
        {
            if constexpr (fmt.tag == FmtType::none)
            {
                auto _copy = val;
                constexpr auto _arr_sz = _copy.get_underlying_array_size();

                constexpr usize _buf_len = static_cast<usize>(1 + N * math::log10(2.));
                to_string_detail::to_string_value_rev<CharT, _buf_len> _buf;

                if (val.none() == true)
                {
                    _buf.push('0');
                    return _buf;
                }

                if constexpr (N > (sizeof(u64) * 8))
                {
                    constexpr integer<N> _one_billion = 10'000'000'000'000'000'000ULL;
                    constexpr integer<N> _cmp_num = 0xFFFF'FFFF'FFFF'FFFFULL;

                    while (_copy > _cmp_num)
                    {
                        const auto [_new_val, _reminder] = _copy.divide_with_reminder(_one_billion).unwrap();

                        constexpr auto _arr_sz = _reminder.get_underlying_array_size();
                        const auto& _internal_arr = _reminder.get_underlying_array();

                        u64 _chunk = _internal_arr[_arr_sz - 2];
                        
                        _chunk <<= 32;
                        _chunk |= _internal_arr[_arr_sz - 1];
                        _copy = move(_new_val);

                        for (usize _idx = 0; _idx < 19; ++_idx)
                        {
                            _buf.push(static_cast<CharT>('0') + (_chunk % 10));
                            _chunk /= 10;
                        }
                    }
                }

                u64 _chunk = 0;

                if constexpr (_arr_sz >= 2)
                {
                    const auto& _internal_arr = _copy.get_underlying_array();
                    _chunk = _internal_arr[_arr_sz - 2];
                
                    _chunk <<= 32;
                    _chunk |= _internal_arr[_arr_sz - 1];
                }
                else
                {
                    _chunk = _copy.get_last_byte();   
                }

                while (_chunk != 0)
                {
                    _buf.push(static_cast<CharT>('0') + (_chunk % 10));
                    _chunk /= 10;
                }

                return _buf;
            }
            else if constexpr ((fmt.tag & FmtType::prec) == FmtType::prec)
            {
                return option_err<runtime_error>{"Precision not allowed for integral type"};
            }
            else if constexpr ((fmt.tag & FmtType::hex) == FmtType::hex)
            {
                const auto& _copy = val.get_underlying_array();
                using CopyType = remove_cvref_t<decltype(_copy)>;

                to_string_detail::to_string_value_for<CharT, CopyType::size() * 8 + 2> _buf;

                constexpr const CharT _hex_table[16] = {
                    '0', '1', '2', '3', '4', '5', '6', '7', 
                    '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
                };

                _buf.push('0');
                _buf.push('x');

                for (usize _idx = 0; _idx < _copy.size(); ++_idx)
                {
                    _buf.push(_hex_table[(_copy[_idx] >> 28)]);
                    _buf.push(_hex_table[(_copy[_idx] >> 24) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 20) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 16) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 12) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 8) & 15]);
                    _buf.push(_hex_table[(_copy[_idx] >> 4) & 15]);
                    _buf.push(_hex_table[_copy[_idx] & 15]);
                }

                return _buf;
            }
            else
            {
                return option_err<runtime_error>{"Unknown format tag for integral type"};
            }
        }();

        if constexpr (IsSame<decltype(_arr), option_err<runtime_error>>)
        {
            return _arr.unwrap_err();
        }
        else
        {
            usize _write_sz = (_arr.size() + fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

            if (_write_sz > sz)
            {
                return runtime_error{"Buffer too small for printing"};
            }
    
            str = _print_to_impl(str, fmt.format);
            str = _print_to_impl(str, fmt.style);
            str = _print_to_impl(str, _arr) + 1;
            str = _print_to_impl(str, fmt.reset);

            return _write_sz;
        }
    }
}