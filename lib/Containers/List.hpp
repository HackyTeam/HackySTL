#pragma once

#include "../Base/Allocator.hpp"

namespace hsd
{
    template < typename T, typename Allocator > class list;

    namespace list_detail
    {
        struct bad_iterator
        {
            const char* operator()() const
            {
                return "Null pointer access denied";
            }
        };

        template <typename T>
        struct list_impl
        {
            T _value;
            list_impl* _next = nullptr;
            list_impl* _back = nullptr;

            template <typename... Args>
            constexpr list_impl(Args&&... args)
                : _value{forward<Args>(args)...}
            {}
        };
    
        template < typename T, typename Allocator >
        class iterator
        {
        private:
            using impl_type = list_impl<T>;
            impl_type* _iterator = nullptr;

            friend class list<T, Allocator>;
        
        public:
            constexpr iterator() {}
            constexpr iterator(hsd::null_type) {}

            constexpr iterator(const iterator& other)
            {
                _iterator = other._iterator;
            }
        
            constexpr iterator& operator=(const iterator& rhs)
            {
                _iterator = rhs._iterator;
                return *this;
            }

            constexpr friend bool operator==(const iterator& lhs, const iterator& rhs)
            {
                return lhs._iterator == rhs._iterator;
            }

            constexpr friend bool operator!=(const iterator& lhs, const iterator& rhs)
            {
                return lhs._iterator != rhs._iterator;
            }

            constexpr auto& operator++()
            {
                _iterator = _iterator->_next;
                return *this;
            }

            constexpr auto& operator--()
            {
                _iterator = _iterator->_back;
                return *this;
            }

            constexpr iterator operator++(i32)
            {
                iterator tmp = *this;
                operator++();
                return tmp;
            }

            constexpr iterator operator--(i32)
            {
                iterator tmp = *this;
                operator--();
                return tmp;
            }

            constexpr auto& operator*()
            {
                return _iterator->_value;
            }

            constexpr const auto& operator*() const
            {
                return _iterator->_value;
            }

            constexpr auto* operator->()
            {
                return addressof(_iterator->_value);
            }

            constexpr const auto* operator->() const
            {
                return addressof(_iterator->_value);
            }
        };
    }

    template < typename T, typename Allocator = allocator >
    class list
    {
    public:
        using iterator = list_detail::iterator<T, Allocator>;
        using const_iterator = const iterator;
        using alloc_type = Allocator;

    private:
        iterator _head;
        iterator _tail;
        alloc_type _alloc;
        usize _size = 0;

    public:
        constexpr list()
        requires (DefaultConstructible<alloc_type>) = default;

        constexpr list(const list& other)
            : _alloc{move(other._alloc)}
        {
            for(const auto& _element : other)
                push_back(_element);
        }

        constexpr list(list&& other)
            : _head{exchange(other._head, nullptr)},
            _tail{exchange(other._tail, nullptr)},
            _alloc{move(other._alloc)}, _size{other._size}
        {}

        template <usize N>
        constexpr list(const T (&arr)[N])
        requires (DefaultConstructible<alloc_type>)
        {
            for (usize _index = 0; _index < N; _index++)
                push_back(arr[_index]);
        }

        template <usize N>
        constexpr list(T (&&arr)[N])
        requires (DefaultConstructible<alloc_type>)
        {
            for (usize _index = 0; _index < N; _index++)
                push_back(move(arr[_index]));
        }

        constexpr list& operator=(const list& rhs)
        {
            clear();
            _alloc = rhs._alloc;

            for (const auto& _element : rhs)
                push_back(_element);
            
            return *this;
        }

        constexpr list& operator=(list&& rhs)
        {
            swap(_alloc, rhs._alloc);
            swap(_head, rhs._head);
            swap(_tail, rhs._tail);
            
            return *this;
        }

        template <usize N>
        constexpr list& operator=(const T (&arr)[N])
        {
            clear();
            usize _index = 0;

            for (auto _it = begin(); _it != end() && _index < N; _it++, _index++)
                *_it = arr[_index];

            for (; _index < N; _index++)
                push_back(arr[_index]);

            return *this;
        }

        template <usize N>
        constexpr list& operator=(T (&&arr)[N])
        {
            clear();
            usize _index = 0;

            for (auto _it = begin(); _it != end() && _index < N; _it++, _index++)
                *_it = move(arr[_index]);

            for (; _index < N; _index++)
                push_back(move(arr[_index]));

            return *this;
        }

        constexpr auto erase(const_iterator pos)
            -> result<iterator, runtime_error>
        {
            if (pos._iterator == nullptr)
            {
                // this in the only situation when
                // .erase() will "throw" because
                // there is no fast way to check
                // if it belongs to this list or not
                return runtime_error{"Accessed an null element"};
            }
            
            if (pos == _tail)
            {
                pop_back();
                return end();
            }
            
            if (pos == _head)
            {
                pop_front();
                return begin();
            }

            iterator _next_iter, _back_iter;
            _next_iter._iterator = pos._iterator->_next;
            _back_iter._iterator = pos._iterator->_back;
            _next_iter._iterator->_back = _back_iter._iterator;
            _back_iter._iterator->_next = _next_iter._iterator;

            _size--;
            
            (*pos).~T();

            _alloc
            .deallocate(pos._iterator, 1)
            .unwrap();
            
            return _next_iter;
        }

        constexpr void push_back(const T& value)
        {
            emplace_back(value);
        }

        constexpr void push_back(T&& value)
        {
            emplace_back(move(value));
        }

        template <typename... Args>
        constexpr void emplace_back(Args&&... args)
        {
            using impl_type = typename iterator::impl_type;

            if (empty())
            {
                _head._iterator = _alloc
                .template allocate<impl_type>(1)
                .unwrap();

                construct_at(
                    _head._iterator, forward<Args>(args)...
                );
                
                _tail = _head;
            }
            else
            {
                iterator _new_tail;
                
                _new_tail._iterator = _alloc
                .template allocate<impl_type>(1)
                .unwrap();

                construct_at(
                    _new_tail._iterator, forward<Args>(args)...
                );
                
                _new_tail._iterator->_back = _tail._iterator;
                _tail._iterator->_next = _new_tail._iterator;
                _tail = _new_tail;
            }

            _size++;
        }

        constexpr void pop_back()
        {
            auto _old_tail = _tail--;
            (*_old_tail).~T();

            _alloc
            .deallocate(_old_tail._iterator, 1)
            .unwrap();

            _size--;
                
            if (_tail != end()) _tail._iterator->_next = nullptr;
            
            if (_size == 0)
            {
                _head._iterator = nullptr;
            }
        }

        constexpr void push_front(const T& value)
        {
            emplace_front(value);
        }

        constexpr void push_front(T&& value)
        {
            emplace_front(move(value));
        }

        template <typename... Args>
        constexpr void emplace_front(Args&&... args)
        {
            using impl_type = typename iterator::impl_type;

            if (empty())
            {
                _head._iterator = _alloc
                .template allocate<impl_type>(1)
                .unwrap();

                construct_at(
                    _head._iterator, forward<Args>(args)...
                );
                
                _tail = _head;
            }
            else
            {
                iterator _new_head;
                
                _new_head._iterator = _alloc
                .template allocate<impl_type>(1)
                .unwrap();

                construct_at(
                    _new_head._iterator, forward<Args>(args)...
                );
                
                _new_head._iterator->_next = _head._iterator;
                _head._iterator->_back = _new_head._iterator;
                _head = _new_head;
            }

            _size++;
        }

        constexpr void pop_front()
        {
            auto _old_head = _head++;
            (*_old_head).~T();

            _alloc
            .deallocate(_old_head._iterator, 1)
            .unwrap();
                
            _size--;

            if (_head != end()) _head._iterator->_back = nullptr;

            if (_size == 0)
            {
                _tail._iterator = nullptr;
            }
        }

        constexpr void clear()
        {
            for (; !empty(); pop_front())
                ;
                
            pop_front();
        }

        constexpr usize size() const
        {
            return _size;
        }

        constexpr bool empty()
        {
            return size() == 0;
        }

        constexpr T& front()
        {
            return *_head;
        }

        constexpr T& back()
        {
            return *_tail;
        }

        constexpr iterator begin()
        {
            return _head;
        }

        constexpr const_iterator begin() const
        {
            return cbegin();
        }

        constexpr iterator end()
        {
            return {nullptr};
        }

        constexpr const_iterator end() const
        {
            return cend();
        }

        constexpr const_iterator cbegin() const
        {
            return _head;
        }

        constexpr const_iterator cend() const
        {
            return {nullptr};
        }

        constexpr iterator rbegin()
        {
            return _tail;
        }

        constexpr const_iterator rbegin() const
        {
            return crbegin();
        }

        constexpr iterator rend()
        {
            return {nullptr};
        }

        constexpr const_iterator rend() const
        {
            return crend();
        }

        constexpr const_iterator crbegin() const
        {
            return _tail;
        }

        constexpr const_iterator crend() const
        {
            return {nullptr};
        }
    };
}