#pragma once

#include "../Base/Random.hpp"
#include "Span.hpp"
#include "../Wrappers/Reference.hpp"

namespace hsd
{
    namespace ranges
    {
        namespace views
        {
            namespace views_detail
            {
                template <ReverseIterable T>
                class reverse_iterator
                {
                private:
                    T _iter;
                
                public:
                    constexpr reverse_iterator(T iter)
                        : _iter{iter}
                    {}

                    constexpr auto& operator++()
                    {
                        --_iter;
                        return *this;
                    }

                    constexpr reverse_iterator operator++(i32)
                    {
                        reverse_iterator tmp = *this;
                        operator++();
                        return tmp;
                    }

                    constexpr auto& operator--()
                    {
                        ++_iter;
                        return *this;
                    }

                    constexpr reverse_iterator operator--(i32)
                    {
                        reverse_iterator tmp = *this;
                        operator--();
                        return tmp;
                    }

                    constexpr bool operator==(const reverse_iterator& rhs) const
                    {
                        return _iter == rhs._iter;
                    }

                    constexpr bool operator!=(const reverse_iterator& rhs) const
                    {
                        return _iter != rhs._iter;
                    }

                    constexpr auto& operator*()
                    {
                        return *_iter;
                    }

                    constexpr const auto& operator*() const
                    {
                        return *_iter;
                    }
                };

                template <ForwardIterable T>
                class random_iterator
                {
                private:
                    T _iter;
                    T _origin;
                    usize _index;
                    static inline mt19937_64 engine{};
                
                public:
                    constexpr random_iterator(T iter, usize index)
                        : _iter{iter}, _origin{iter}, _index{index}
                    {}

                    constexpr auto& operator++()
                    {
                        _iter = _origin;
                        
                        usize _to = engine.generate<usize>(0u, _index + 1).unwrap();
                        
                        for (usize _from = 0; _from < _to; _from++, _iter++)
                            ;

                        _index++;
                        return *this;
                    }

                    constexpr random_iterator operator++(i32)
                    {
                        auto tmp = *this;
                        operator++();
                        return tmp;
                    }

                    constexpr friend bool operator==(
                        const random_iterator& lhs, const random_iterator& rhs)
                    {
                        return lhs._index == rhs._index;
                    }

                    constexpr friend bool operator!=(
                        const random_iterator& lhs, const random_iterator& rhs)
                    {
                        return lhs._index != rhs._index; 
                    }

                    constexpr auto& operator*()
                    {
                        return *_iter;
                    }

                    constexpr const auto& operator*() const
                    {
                        return *_iter;
                    }
                };

                template <ForwardIterable T, typename F>
                requires (InvocableRet<bool, F, decltype(*declval<T>())>)
                class filter_iterator
                {
                private:
                    using iter_type = T;
                    using func_type = reference<F>;

                    func_type _func;
                    iter_type _current;
                    iter_type _end;
                
                public:
                    constexpr filter_iterator(
                        func_type func, iter_type current, iter_type end)
                        : _func{func}, _current{current}, _end{end}
                    {}

                    constexpr auto& operator++()
                    {
                        auto _copy = _current;
                        
                        _current = find(
                            (_copy != _end) ? ++_copy : _copy, _end, _func.get()
                        );
                        
                        return *this;
                    }

                    constexpr filter_iterator operator++(i32)
                    {
                        filter_iterator tmp = *this;
                        operator++();
                        return tmp;
                    }

                    constexpr bool operator==(const filter_iterator& rhs) const
                    {
                        return _current == rhs._current;
                    }

                    constexpr bool operator!=(const filter_iterator& rhs) const
                    {
                        return _current != rhs._current;
                    }

                    constexpr auto& operator*()
                    {
                        return *_current;
                    }

                    constexpr const auto& operator*() const
                    {
                        return *_current;
                    }
                };

                template <ForwardIterable T, typename F>
                class filter_wrapper
                {
                private:
                    using func_type = F;
                    using iter_type = filter_iterator<T, F>;

                    func_type _func;
                    iter_type _begin;
                    iter_type _end;

                public:
                    constexpr filter_wrapper(
                        func_type func, const T& begin, const T& end)
                        : _func{func}, _begin{_func, begin, end}, _end{_func, end, end}
                    {}

                    constexpr iter_type begin()
                    {
                        return _begin;
                    }

                    constexpr const iter_type begin() const
                    {
                        return _begin;
                    }

                    constexpr iter_type end()
                    {
                        return _end;
                    }

                    constexpr const iter_type end() const
                    {
                        return _end;
                    }
                };

                template <ForwardIterable T, typename F>
                requires (Invocable<F, decltype(*declval<T>())>)
                class transform_iterator
                {
                private:
                    using iter_type = T;
                    using func_type = reference<F>;

                    func_type _func;
                    iter_type _iter;
                
                public:
                    constexpr transform_iterator(func_type func, iter_type iter)
                        : _func{func}, _iter{iter}
                    {}

                    constexpr auto& operator++()
                    {
                        ++_iter;
                        return *this;
                    }

                    constexpr transform_iterator operator++(i32)
                    {
                        transform_iterator tmp = *this;
                        operator++();
                        return tmp;
                    }

                    constexpr bool operator==(const transform_iterator& rhs) const
                    {
                        return _iter == rhs._iter;
                    }

                    constexpr bool operator!=(const transform_iterator& rhs) const
                    {
                        return _iter != rhs._iter;
                    }

                    constexpr decltype(auto) operator*()
                    {
                        return _func.get()(*_iter);
                    }

                    constexpr decltype(auto) operator*() const
                    {
                        return _func.get()(*_iter);
                    }
                };

                template <ForwardIterable T, typename F>
                class transform_wrapper
                {
                private:
                    using func_type = F;
                    using iter_type = transform_iterator<T, F>;

                    func_type _func;
                    iter_type _begin;
                    iter_type _end;

                public:
                    constexpr transform_wrapper(
                        func_type func, const T& begin, const T& end)
                        : _func{func}, _begin{_func, begin}, _end{_func, end}
                    {}

                    constexpr iter_type begin()
                    {
                        return _begin;
                    }

                    constexpr const iter_type begin() const
                    {
                        return _begin;
                    }

                    constexpr iter_type end()
                    {
                        return _end;
                    }

                    constexpr const iter_type end() const
                    {
                        return _end;
                    }
                };

                template <ForwardIterable T>
                class iter_wrapper
                {
                private:
                    using iter_type = T;

                    iter_type _begin;
                    iter_type _end;
                    usize _size = 0;

                public:
                    constexpr iter_wrapper(
                        const iter_type& begin, const iter_type& end, usize size)
                        : _begin{begin}, _end{end}, _size{size}
                    {}

                    constexpr usize size() const
                    {
                        return _size;
                    }

                    constexpr iter_type begin()
                    {
                        return _begin;
                    }

                    constexpr const iter_type begin() const
                    {
                        return _begin;
                    }

                    constexpr iter_type end()
                    {
                        return _end;
                    }

                    constexpr const iter_type end() const
                    {
                        return _end;
                    }

                    constexpr iter_type rbegin()
                    {
                        auto _val = _end;
                        return --_val;
                    }

                    constexpr const iter_type rbegin() const
                    {
                        auto _val = _end;
                        return --_val;
                    }

                    constexpr iter_type rend()
                    {
                        auto _val = _begin;
                        return --_val;
                    }

                    constexpr const iter_type rend() const
                    {
                        auto _val = _begin;
                        return --_val;
                    }
                };
                
                template <ReverseIterable T>
                class reverse
                {
                private:
                    using iter_type = views_detail::reverse_iterator<T>;
                    usize _view_size = 0;
                    iter_type _begin;
                    iter_type _end;
                
                    constexpr reverse(const iter_type& begin, 
                        const iter_type& end, usize view_size)
                        : _view_size{view_size}, _begin{begin}, _end{end}
                    {}

                public:
                    using value_type = T;

                    template <IsReverseContainer U>
                    constexpr reverse(U&& container)
                        : _view_size{container.size()}, 
                        _begin{container.rbegin()}, 
                        _end{container.rend()}
                    {}

                    constexpr auto drop(usize quantity) const
                        -> result<reverse, runtime_error> 
                    {
                        if (quantity > _view_size)
                        {
                            return runtime_error{"Dropping out of bounds"};
                        }
                        else
                        {
                            usize _index = 0;
                            iter_type _result_iter = _begin;
                            
                            for (; _index < quantity; _index++, _result_iter++)
                                ;

                            return reverse{_result_iter, _end, _view_size - _index};
                        }
                    }

                    constexpr auto take(usize quantity) const
                        -> result<reverse, runtime_error> 
                    {
                        if(quantity > _view_size)
                        {
                            return runtime_error{"Dropping out of bounds"};
                        }
                        else
                        {
                            usize _index = 0;
                            iter_type _result_iter = _begin;
                            for(; _index < quantity; _index++, _result_iter++);
                            return reverse{_begin, _result_iter, _view_size - _index};
                        }
                    }

                    constexpr usize size() const
                    {
                        return _view_size;
                    }

                    constexpr auto begin()
                    {
                        return _begin;
                    }

                    constexpr auto begin() const
                    {
                        return _begin;
                    }

                    constexpr auto end()
                    {
                        return _end;
                    }

                    constexpr auto end() const
                    {
                        return _end;
                    }
                };

                template <ForwardIterable T>
                class random
                {
                private:
                    using iter_type = views_detail::random_iterator<T>;
                    usize _view_size = 0;
                    iter_type _begin;
                    iter_type _end;
                
                    constexpr random(const iter_type& begin, const iter_type& end, usize view_size)
                        : _view_size{view_size}, _begin{begin}, _end{end}
                    {}

                public:
                    using value_type = T;

                    template <IsForwardContainer U>
                    constexpr random(U&& container)
                        : _view_size{container.size()}, _begin{container.begin(), 0}, 
                        _end{container.end(), container.size()}
                    {}

                    constexpr auto drop(usize quantity) const
                        -> result<random, runtime_error>
                    {
                        if (quantity > _view_size)
                        {
                            return runtime_error{"Dropping out of bounds"};
                        }
                        else
                        {
                            usize _index = 0;
                            iter_type _result_iter = _begin;
                            
                            for (; _index < quantity; _index++, _result_iter++)
                                ;

                            return random{_result_iter, _end, _view_size - _index};
                        }
                    }

                    constexpr auto take(usize quantity) const
                        -> result<random, runtime_error> 
                    {
                        if (quantity > _view_size)
                        {
                            return runtime_error{"Dropping out of bounds"};
                        }
                        else
                        {
                            usize _index = 0;
                            iter_type _result_iter = _begin;
                            
                            for(; _index < quantity; _index++, _result_iter++)
                                ;

                            return random{_begin, _result_iter, _view_size - _index};
                        }
                    }

                    constexpr usize size() const
                    {
                        return _view_size;
                    }

                    constexpr auto begin()
                    {
                        return _begin;
                    }

                    constexpr auto begin() const
                    {
                        return _begin;
                    }

                    constexpr auto end()
                    {
                        return _end;
                    }

                    constexpr auto end() const
                    {
                        return _end;
                    }
                };

                template <IsReverseContainer U>
                reverse(U&& container) -> reverse<decltype(container.rend())>;
                template <IsForwardContainer U>
                random(U&& container) -> random<decltype(container.end())>;

                struct _reverse
                {
                    template <IsReverseContainer U>
                    constexpr friend auto operator|(U&& rhs, const _reverse&)
                    {
                        using iter_type = remove_cvref_t<decltype(rhs.begin())>;

                        return reverse<iter_type>{rhs};
                    }
                };

                struct _random
                {
                    template <IsForwardContainer U>
                    constexpr friend auto operator|(U&& rhs, const _random&)
                    {
                        using iter_type = remove_cvref_t<decltype(rhs.begin())>;

                        return random<iter_type>{rhs};
                    }
                };
            } // namespace views_detail
            
            template <typename T>
            concept IsView = (
                IsSame<T, span<typename T::value_type>> ||
                IsSame<T, views_detail::reverse<typename T::value_type>> ||
                IsSame<T, views_detail::random<typename T::value_type>>
            );

            static constexpr views_detail::_reverse reverse = {};
            static constexpr views_detail::_random random = {};

            class drop
            {
            private:
                usize _count;

            public:
                constexpr drop(usize count)
                    : _count{count}
                {}

                template <IsForwardContainer U>
                constexpr friend auto operator|(U&& lhs, const drop& rhs)
                {
                    auto _iter = lhs.begin();
                    usize _cnt = rhs._count;
                    usize _new_size = 0;

                    if constexpr (requires {lhs.length();})
                    {
                        _new_size = lhs.length() - _cnt;
                    }
                    else
                    {
                        _new_size = lhs.size() - _cnt;
                    }

                    while (_cnt != 0)
                    {
                        if (_iter == lhs.end())
                        {
                            panic("Too few elements");
                        }

                        ++_iter; --_cnt;
                    }
                    
                    return views_detail::iter_wrapper{_iter, lhs.end(), _new_size};
                }
            };

            template <typename FuncType>
            class drop_while
            {
            private:
                FuncType _func;

            public:
                constexpr drop_while(FuncType func)
                    : _func(hsd::forward<FuncType>(func))
                {}

                template <IsForwardContainer U>
                constexpr friend auto operator|(U&& lhs, const drop_while& rhs)
                requires (InvocableRet<bool, FuncType, decltype(*lhs.begin())>)
                {
                    auto _iter = lhs.begin();
                    usize _new_size = 0;

                    if constexpr (requires{rhs._func(*lhs.begin()).unwrap();})
                    {
                        for (; _iter != lhs.end() && 
                            rhs._func(*_iter).unwrap(); _iter++)
                            ;
                    }
                    else
                    {
                        for (; _iter != lhs.end() && rhs._func(*_iter); _iter++)
                            ;
                    }

                    for (auto _iter_copy = _iter; _iter_copy != lhs.end(); ++_iter_copy)
                    {
                        ++_new_size;
                    }

                    return views_detail::iter_wrapper{_iter, lhs.end(), _new_size};
                }
            };

            class take
            {
            private:
                usize _count;

            public:
                constexpr take(usize count)
                    : _count{count}
                {}

                template <IsForwardContainer U>
                constexpr friend auto operator|(U&& lhs, const take& rhs)
                {
                    auto _iter = lhs.begin();
                    usize _cnt = rhs._count;

                    while (_cnt != 0)
                    {
                        if (_iter == lhs.end())
                        {
                            panic("Too few elements");
                        }

                        ++_iter; --_cnt;
                    }
                    
                    return views_detail::iter_wrapper{lhs.begin(), _iter, rhs._count};
                }
            };

            template <typename FuncType>
            class take_while
            {
            private:
                FuncType _func;

            public:
                constexpr take_while(FuncType func)
                    : _func(hsd::forward<FuncType>(func))
                {}

                template <IsForwardContainer U>
                constexpr friend auto operator|(U&& lhs, const take_while& rhs)
                requires (InvocableRet<bool, FuncType, decltype(*lhs.begin())>)
                {
                    auto _iter = lhs.begin();
                    usize _new_size = 0;

                    if constexpr (requires{rhs._func(*lhs.begin()).unwrap();})
                    {
                        for (; _iter != lhs.end() && rhs._func(*_iter).unwrap(); _iter++)
                        {
                            ++_new_size;
                        }
                    }
                    else
                    {
                        for (; _iter != lhs.end() && rhs._func(*_iter); _iter++)
                        {
                            ++_new_size;
                        }
                    }

                    return views_detail::iter_wrapper{lhs.begin(), _iter, _new_size};
                }
            };
        } // namespace views
        
        template <typename FuncType>
        class filter
        {
        private:
            FuncType _func;

        public:
            constexpr filter(FuncType func)
                : _func(hsd::forward<FuncType>(func))
            {}

            template <IsForwardContainer U>
            constexpr friend auto operator|(U&& lhs, const filter& rhs)
            requires (InvocableRet<bool, FuncType, decltype(*lhs.begin())>)
            {
                auto _filter_func = [rhs](auto&& val)
                {
                    if constexpr (requires{rhs._func(val).unwrap();})
                    {
                        return rhs._func(val).unwrap();
                    }
                    else
                    {
                        return rhs._func(val);
                    }
                };

                return views::views_detail::filter_wrapper{
                    _filter_func, lhs.begin(), lhs.end()
                };
            }
        };

        template <typename FuncType>
        class transform
        {
        private:
            FuncType _func;

        public:
            constexpr transform(FuncType func)
                : _func(hsd::forward<FuncType>(func))
            {}

            template <IsForwardContainer U> 
            constexpr friend auto operator|(U&& lhs, const transform& rhs)
            requires (InvocableRet<
                remove_cvref_t<decltype(*lhs.begin())>, FuncType, decltype(*lhs.begin())>)
            {
                auto _transform_func = [rhs](auto&& val)
                {
                    if constexpr (requires{rhs._func(val).unwrap();})
                    {
                        return rhs._func(val).unwrap();
                    }
                    else
                    {
                        return rhs._func(val);
                    }
                };

                return views::views_detail::transform_wrapper{
                    _transform_func, lhs.begin(), lhs.end()
                };
            }
        };
    } // namespace ranges

    namespace views = ranges::views;    
} // namespace hsd
