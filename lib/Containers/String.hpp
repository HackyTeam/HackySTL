#pragma once

#include "../Base/CString.hpp"
#include "StringView.hpp"
#include "../Base/Algorithm.hpp"
#include "../Serialize/ToString.hpp"
#include "../Base/Unicode.hpp"

namespace hsd
{
    template < typename CharT, typename Allocator = allocator >
    class basic_string
    {
    private:
        using _str_utils = basic_cstring<CharT>;
        static constexpr const CharT _s_empty = 0;
        using alloc_type = Allocator;
        
        alloc_type _alloc;
        CharT* _data = nullptr;
        usize _size = 0;
        usize _capacity = 0;

        constexpr void _reset()
        {
            _alloc.deallocate(_data, _capacity + 1).unwrap();
            _data = nullptr;
        }

        template <typename CharT2, typename Allocator2>
        friend class basic_string;

    public:
        using iterator = CharT*;
        using const_iterator = const CharT*;
        using value_type = CharT;
        static constexpr usize npos = static_cast<usize>(-1);

        constexpr basic_string()
        requires (DefaultConstructible<alloc_type>) = default;

        constexpr basic_string(const alloc_type& alloc)
            : _alloc{alloc}
        {}

        constexpr basic_string(usize size)
        requires (DefaultConstructible<alloc_type>)
        {
            _data = _alloc
                .template allocate<CharT>(size + 1)
                .unwrap();
            
            _capacity = size;
        }

        constexpr basic_string(usize size, const alloc_type& alloc)
            : _alloc{alloc}
        {
            _data = _alloc
                .template allocate<CharT>(size + 1)
                .unwrap();

            _capacity = size;
        }

        constexpr basic_string(const CharT* cstr)
        requires (DefaultConstructible<alloc_type>)
            : _alloc{}
        {
            _size = (cstr != nullptr) ? _str_utils::length(cstr) : 0;
            _capacity = _size;
            
            _data = _alloc
                .template allocate<CharT>(_size + 1)
                .unwrap();
            
            if (cstr != nullptr)
            {
                _str_utils::copy(_data, cstr, _size);
            }

            _data[_size] = static_cast<CharT>(0);
        }

        constexpr basic_string(
            const CharT* cstr, const alloc_type& alloc)
            : _alloc{alloc}
        {
            _size = (cstr != nullptr) ? _str_utils::length(cstr) : 0;
            _capacity = _size;
            _data = _alloc.allocate(_size + 1).unwrap();
            
            if (cstr != nullptr)
            {
                _str_utils::copy(_data, cstr, _size);
            }

            _data[_size] = static_cast<CharT>(0);
        }

        constexpr basic_string(const CharT* cstr, usize size)
        requires (DefaultConstructible<alloc_type>)
            : _alloc{}
        {
            _size = size;
            _capacity = _size;
            _data = _alloc
                .template allocate<CharT>(_size + 1)
                .unwrap();
            
            if (cstr != nullptr)
            {
                _str_utils::copy(_data, cstr, _size);
            }

            _data[_size] = static_cast<CharT>(0);
        }

        constexpr basic_string(
            const CharT* cstr, usize size, const alloc_type& alloc)
            : _alloc{alloc}
        {
            _size = size;
            _capacity = _size;
            _data = _alloc.allocate(_size + 1).unwrap();
            
            if (cstr != nullptr)
            {
                _str_utils::copy(_data, cstr, _size);
            }

            _data[_size] = static_cast<CharT>(0);
        }

        constexpr basic_string(basic_string_view<CharT> view)
        requires (DefaultConstructible<alloc_type>)
            : basic_string{view.data(), view.size()}
        {}

        constexpr basic_string(
            basic_string_view<CharT> view, const alloc_type& alloc)
            : basic_string{view.data(), view.size(), alloc}
        {}

        constexpr basic_string(const basic_string& other)
            : _alloc{other._alloc}
        {
            _size = other._size;
            _capacity = other._capacity;
            _data = _alloc
                .template allocate<CharT>(_capacity + 1)
                .unwrap();
            
            if (other._data != nullptr)
            {
                _str_utils::copy(_data, other._data, _size);
            }

            _data[_size] = static_cast<CharT>(0);
        }

        template <typename CharT2>
        constexpr basic_string(const basic_string<CharT2, Allocator>& other)
            : _alloc{other._alloc}
        {
            _size = _capacity = unicode::length<CharT, CharT2>(other._data);
                
            _data = _alloc
                .template allocate<CharT>(_capacity + 1)
                .unwrap();
            
            unicode::convert_to(_data, _size + 1, other._data).unwrap();

            _data[_size] = static_cast<CharT>(0);
        }

        constexpr basic_string(basic_string&& other)
            : _alloc{move(other._alloc)}
        {
            swap(_size, other._size);
            swap(_capacity, other._capacity);
            swap(_data, other._data);
        }

        constexpr ~basic_string()
        {
            _reset();
        }

        constexpr basic_string& operator=(const CharT* rhs)
        {
            auto _new_size = _str_utils::length(rhs);
            reserve(_new_size);
            _size = _new_size;
            _str_utils::copy(_data, rhs, _size);
            _data[_size] = static_cast<CharT>(0);
            return *this;
        }

        constexpr basic_string& operator=(const basic_string_view<CharT>& rhs)
        {
            auto _new_size = rhs.size();
            reserve(_new_size);
            _size = _new_size;
            copy_n(rhs.data(), _size, _data);
            _data[_size] = static_cast<CharT>(0);
            return *this;
        }

        constexpr basic_string& operator=(const basic_string& rhs)
        {
            _reset();
            _alloc = rhs._alloc;
            _size = rhs._size;
            _capacity = _size;
            
            _data = _alloc
                .template allocate<CharT>(_size + 1)
                .unwrap();
            
            copy_n(rhs.c_str(), _size, _data);
            _data[_size] = static_cast<CharT>(0);
            return *this;
        }

        template <typename CharT2>
        constexpr basic_string& operator=(const basic_string<CharT2, Allocator>& rhs)
        {
            _reset();
            _alloc = rhs._alloc;
            _size = _capacity = unicode::length<CharT, CharT2>(rhs._data);
            _data = _alloc.allocate(_size + 1).unwrap();
            
            unicode::convert_to(_data, _size + 1, rhs._data).unwrap();
            _data[_size] = static_cast<CharT>(0);
            return *this;
        }

        constexpr basic_string& operator=(basic_string&& rhs)
        {
            swap(_alloc, rhs._alloc);
            swap(_size, rhs._size);
            swap(_capacity, rhs._capacity);
            swap(_data, rhs._data);
            return *this;
        }

        constexpr basic_string operator+(const basic_string& rhs) const
        {
            if (rhs.data() == nullptr || _data == nullptr)
            {
                panic("Cannot concatenate null strings");
            }
            else
            {
                basic_string _buf(_size + rhs._size);
                _buf._size = _size + rhs._size;
                _str_utils::copy(_buf._data, _data, _size);
                _str_utils::add(_buf._data, rhs._data, _size);
                _buf._data[_buf._size] = static_cast<CharT>(0);
                return _buf;
            }
        }

        constexpr basic_string operator+(const basic_string_view<CharT>& rhs) const
        {
            if (rhs.data() == nullptr || _data == nullptr)
            {
                panic("Cannot concatenate null strings");
            }
            else
            {
                basic_string _buf(_size + rhs.size());
                _buf._size = _size + rhs.size();
                _str_utils::copy(_buf._data, _data, _size);
                _str_utils::add(_buf._data, rhs.data(), _size);
                _buf._data[_buf._size] = static_cast<CharT>(0);
                return _buf;
            }
        }

        constexpr basic_string operator+(const CharT* rhs) const
        {
            if (rhs == nullptr || _data == nullptr)
            {
                panic("Cannot concatenate null strings");
            }
            else
            {
                usize _rhs_len = _str_utils::length(rhs);
                basic_string _buf(_size + _rhs_len);
                _buf._size = _size + _rhs_len;
                _str_utils::copy(_buf._data, _data, _size);
                _str_utils::add(_buf._data, rhs, _size);
                _buf._data[_buf._size] = static_cast<CharT>(0);
                return _buf;
            }
        }

        constexpr friend basic_string operator+(
            const basic_string_view<CharT>& lhs, const basic_string& rhs)
        {
            if (lhs.data() == nullptr || rhs._data == nullptr)
            {
                panic("Cannot concatenate null strings");
            }
            else
            {
                basic_string _buf(rhs._size + lhs.size());
                _str_utils::copy(_buf._data, lhs.data(), lhs.size());
                _str_utils::add(_buf._data, rhs._data, lhs.size());
                _buf._data[_buf._size] = static_cast<CharT>(0);
                return _buf;
            }
        }

        constexpr friend basic_string operator+(const CharT* lhs, const basic_string& rhs)
        {
            if (lhs == nullptr || rhs._data == nullptr)
            {
                panic("Cannot concatenate null strings");
            }
            else
            {
                usize _lhs_len = _str_utils::length(lhs);
                basic_string _buf(rhs._size + _lhs_len);
                _buf._size = rhs._size + _lhs_len;
                _str_utils::copy(_buf._data, lhs, _lhs_len);
                _str_utils::add(_buf._data, rhs._data, _lhs_len);
                _buf._data[_buf._size] = static_cast<CharT>(0);
                return _buf;
            }
        }

        constexpr basic_string& operator+=(const basic_string& rhs)
        {
            if (rhs.data() == nullptr || _data == nullptr)
            {
                panic("Cannot concatenate null strings");
            }
            else if (_capacity <= _size + rhs._size)
            {
                basic_string _buf(_size + rhs._size);
                _buf._size = _size + rhs._size;
                _str_utils::copy(_buf._data, _data, _size);
                _str_utils::add(_buf._data, rhs._data, _size);
                _buf._data[_buf._size] = static_cast<CharT>(0);
                operator=(move(_buf));
                return *this;
            }
            else
            {
                _str_utils::add(_data, rhs._data, _size);
                _size += rhs._size;
                _data[_size] = static_cast<CharT>(0);
                return *this;
            }
        }

        constexpr basic_string& operator+=(const basic_string_view<CharT>& rhs)
        {
            if (rhs.data() == nullptr || _data == nullptr)
            {
                panic("Error: nullptr argument.");
            }
            else if (_capacity <= _size + rhs.size())
            {
                basic_string _buf(_size + rhs.size());
                _buf._size = _size + rhs.size();
                _str_utils::copy(_buf._data, _data, _size);
                _str_utils::add(_buf._data, rhs.data(), _size);
                _buf._data[_buf._size] = static_cast<CharT>(0);
                operator=(move(_buf));
                return *this;
            }
            else
            {
                _str_utils::add(_data, rhs.data(), _size);
                _size += rhs.size();
                _data[_size] = static_cast<CharT>(0);
                return *this;
            }
        }

        constexpr basic_string& operator+=(const CharT* rhs)
        {
            if (rhs == nullptr || _data == nullptr)
            {
                panic("Error: nullptr argument.");
            }
            else 
            {
                usize _rhs_len = _str_utils::length(rhs);

                if (_capacity <= _size + _rhs_len)
                {
                    basic_string _buf(_size + _rhs_len);
                    _buf._size = _size + _rhs_len;
                    _str_utils::copy(_buf._data, _data, _size);
                    _str_utils::add(_buf._data, rhs, _size);
                    _buf._data[_buf._size] = static_cast<CharT>(0);
                    operator=(move(_buf));
                    return *this;
                }
                else
                {
                    _str_utils::add(_data, rhs, _size);
                    _data[_size] = static_cast<CharT>(0);
                    return *this;
                }
            }
        }

        constexpr CharT& operator[](usize index)
        {
            return _data[index];
        }

        constexpr const CharT& operator[](usize index) const
        {
            return _data[index];
        }

        constexpr i32 operator<=>(const basic_string& rhs) const
        {
            return _str_utils::compare(_data, rhs._data);
        }

        constexpr bool operator==(const basic_string& rhs) const
        {
            return _size == rhs._size && operator<=>(rhs) == 0;
        }

        constexpr i32 operator<=>(const basic_string_view<CharT>& rhs) const
        {
            return _str_utils::compare(_data, rhs.data(), (length() > rhs.size()) ? length() : rhs.size());
        }

        constexpr bool operator==(const basic_string_view<CharT>& rhs) const
        {
            return _size == rhs.size() && operator<=>(rhs) == 0;
        }

        constexpr auto at(usize index)
            -> result<reference<CharT>, bad_access>
        {
            if(index >= _size)
                return bad_access{};

            return {_data[index]};
        }

        constexpr auto at(usize index) const
            -> result<reference<const CharT>, bad_access>
        {
            if(index >= _size)
                return bad_access{};

            return {_data[index]};
        }

        constexpr usize find(const basic_string& str, usize pos = 0) const
        {
            if (pos >= _size)
            {
                return npos;
            }
            else
            {
                const CharT* _find_addr = _str_utils::find(
                    &_data[pos], str._data
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize find(const CharT* str, usize pos = 0) const
        {
            if (pos >= _size)
            {
                return npos;
            }
            else
            {
                const CharT* _find_addr = _str_utils::find(
                    &_data[pos], str
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize find(CharT letter, usize pos = 0) const
        {
            if (pos >= _size)
            {
                return npos;
            }
            else
            {
                const CharT* _find_addr = _str_utils::find(
                    &_data[pos], letter
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize rfind(const basic_string& str, usize pos = npos) const
        {
            if (pos >= _size && pos != npos)
            {
                return npos;
            }
            else if (pos == npos)
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    _data, str._data, _size
                );

                if(_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
            else
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str._data, _size - pos
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize rfind(const CharT* str, usize pos = npos) const
        {
            if (pos >= _size && pos != npos)
            {
                return npos;
            }
            else if (pos == npos)
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    _data, str, _size
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
            else
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str, _size - pos
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize rfind(CharT str, usize pos = npos) const
        {
            if (pos >= _size && pos != npos)
            {
                return npos;
            }
            else if (pos == npos)
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    _data, str, _size
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
            else
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str, _size - pos
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr bool starts_with(CharT letter) const
        {
            if (_data != nullptr)
                return _data[0] == letter;

            return false;
        }

        constexpr bool starts_with(const CharT* str) const
        {
            if (_data != nullptr)
                return find(str) == 0;

            return false;
        }

        constexpr bool starts_with(const basic_string& str) const
        {
            if (_data != nullptr)
                return find(str) == 0;

            return false;
        }

        constexpr bool contains(CharT letter) const
        {
            if (_data != nullptr)
                return find(letter) != npos;

            return false;
        }

        constexpr bool contains(const CharT* str) const
        {
            if (_data != nullptr)
                return find(str) != npos;

            return false;
        }

        constexpr bool contains(const basic_string& str) const
        {
            if (_data != nullptr)
                return find(str) != npos;

            return false;
        }

        constexpr bool ends_with(CharT letter) const
        {
            if (_data != nullptr)
                return _data[_size - 1] == letter;

            return false;
        }

        constexpr bool ends_with(const CharT* str) const
        {
            usize _len = cstring::length(str);

            if (_data != nullptr)
                return rfind(str) == (_size - _len);

            return false;
        }

        constexpr bool ends_with(const basic_string& str) const
        {
            if (_data != nullptr)
                return rfind(str) == (_size - str._size);

            return false;
        }

        constexpr auto sub_string(usize from, usize count)
            -> result<basic_string, bad_access>
        {
            if (from > _size || (from + count) > _size)
                return bad_access{};

            return basic_string{_data + from, count};
        }

        constexpr auto sub_string(usize from)
            -> result<basic_string, bad_access>
        {
            return sub_string(from, _size - from);
        }

        constexpr auto erase(const_iterator pos)
            -> result<iterator, bad_access>
        {
            return erase_for(pos, pos + 1);
        }

        constexpr auto erase_for(const_iterator from, const_iterator to)
            -> result<iterator, bad_access>
        {
            if (from < begin() || from > end() || to < begin() || to > end() || from > to)
                return bad_access{};

            usize _current_pos = static_cast<usize>(from - begin());
            usize _last_pos = static_cast<usize>(to - begin());

            for (usize _index = 0; _index < _capacity - _last_pos + 1; _index++)
            {
                this->_data[_current_pos + _index] = 
                    move(this->_data[_last_pos + _index]);
            }

            _size -= static_cast<usize>(to - from) + 1;
            return begin() + _last_pos;
        }
    
        constexpr void reserve(usize new_cap)
        {
            if (new_cap > _capacity)
            {
                // To handle _capacity = 0 case
                usize _new_capacity = _capacity ? _capacity : 1;

                while (_new_capacity < new_cap)
                    _new_capacity += (_new_capacity + 1) / 2;

                // Allocate space for NULL byte
                auto* _new_buf = _alloc
                    .template allocate<CharT>(_new_capacity + 1)
                    .unwrap();

                _new_buf[_new_capacity] = 0;
                
                for (usize _index = 0; _index < _size; ++_index)
                {
                    auto& _value = _data[_index];
                    _new_buf[_index] = move(_value);
                }

                _alloc.deallocate(_data, _capacity + 1).unwrap();
                _data = _new_buf;
                _capacity = _new_capacity;
            }
        }

        constexpr void resize(usize new_length)
        {
            if (new_length >= _capacity)
            {
                // To handle _capacity = 0 case
                usize _new_capacity = _capacity ? _capacity : 1;
                
                while (_new_capacity < new_length)
                    _new_capacity += (_new_capacity + 1) / 2;

                if (_new_capacity == new_length)
                    ++_new_capacity;

                CharT* _new_buf = _alloc
                    .template allocate<CharT>(_new_capacity)
                    .unwrap();
                
                usize _index = 0;

                for (; _index < _size; ++_index)
                {
                    auto& _value = _data[_index];
                    construct_at(&_new_buf[_index], move(_value));
                    _value.~CharT();
                }
                for (; _index <= new_length; ++_index)
                {
                    construct_at(&_new_buf[_index]);
                }

                _alloc.deallocate(_data, _capacity).unwrap();
                _data = _new_buf;
                _capacity = _new_capacity - 1;
                _size = new_length;
            }
            else if (new_length > _size)
            {
                for (usize _index = _size; _index <= new_length; ++_index)
                {
                    construct_at(&_data[_index]);
                }
                
                _size = new_length;
            }
            else if (new_length < _size)
            {
                for (usize _index = _size; _index > new_length; --_index)
                    _data[_index - 1].~CharT();
                
                _size = new_length;
                _data[_size] = 0;
            }
        }

        template <typename... Args>
        constexpr void emplace_back(Args&&... args)
        {
            reserve(_size + 2);
            _data[_size] = CharT{forward<Args>(args)...};
            _data[++_size] = '\0';
        }

        constexpr void push_back(const CharT& val)
        {
            emplace_back(val);
        }

        constexpr void push_back(CharT&& val)
        {
            emplace_back(move(val));
        }

        constexpr void clear()
        {
            if (_capacity != 0)
            {
                _data[0] = '\0';
                _size = 0;
            }
        }

        constexpr void pop_back()
        {
            if (_size > 0)
            {
                _data[_size--] = '\0';
            }
        }

        constexpr CharT& front()
        {
            return _data[0];
        }

        constexpr CharT& back()
        {
            return _data[_size - 1];
        }

        constexpr usize size() const
        {
            return _size + 1;
        }

        constexpr usize length() const
        {
            return _size;
        }

        constexpr usize capacity() const
        {
            return _capacity;
        }

        constexpr iterator data()
        {
            return _data;
        }

        constexpr const_iterator data() const
        {
            return _data;
        }

        constexpr const_iterator c_str() const
        {
            return _data != nullptr ? _data : &_s_empty;
        }

        constexpr iterator begin()
        {
            return data();
        }

        constexpr const_iterator begin() const
        {
            return data();
        }

        constexpr iterator end()
        {
            return begin() + length();
        }

        constexpr const_iterator end() const
        {
            return begin() + length();
        }

        constexpr const_iterator cbegin() const
        {
            return begin();
        }

        constexpr const_iterator cend() const
        {
            return end();
        }

        explicit constexpr operator basic_string_view<CharT>() const
        {
            return basic_string_view<CharT>{_data, _size + 1};
        }
    };

    template <typename CharT>
    static constexpr option_err<runtime_error> _parse_from(
        const basic_string_view<CharT>& from, basic_string<CharT>& to)
    {
        to = from;
        return {};
    }

    template <typename CharT, CharacterType T>
    requires (!IsSame<CharT, T>)
    static constexpr option_err<runtime_error> _parse_from(
        const basic_string_view<CharT>& from, basic_string<T>& to)
    {
        auto _len = unicode::length<T, CharT>(from.data(), from.size());
        to.resize(_len);
        
        auto _res = unicode::convert_to(to.data(), _len, from.data(), from.size());
        
        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }
        
        return {};
    }

    template <format_output fmt, typename CharT, CharacterType T>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, const basic_string<T>& val)
    {
        using FmtType = decltype(fmt);

        if constexpr (fmt.tag != FmtType::none)
        {
            return runtime_error{"Unknown format tag for character type"};
        }

        usize _write_sz = (fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

        if (_write_sz > sz)
        {
            return runtime_error{"Buffer too small for printing"};
        }

        str = _print_to_impl(str, fmt.format);
        str = _print_to_impl(str, fmt.style);

        auto _res = unicode::convert_to<CharT, T>(str, sz, val.c_str(), val.size() - 1);

        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }

        usize _pos = sz - _res.unwrap() - 1;
        str = _print_to_impl(str + _pos, fmt.reset);

        return _pos + _write_sz;
    }

    template <typename CharT, usize N>
    class static_basic_string
    {
    private:
        using _str_utils = basic_cstring<CharT>;
        CharT _data[N]{};
        usize _size = 0;
        usize _capacity = N;

    public:
        using iterator = CharT*;
        using const_iterator = const CharT*;
        using value_type = CharT;
        static constexpr usize npos = static_cast<usize>(-1);

        constexpr static_basic_string() = default;

        constexpr static_basic_string(usize size)
        {
            if (size < N)
            {
                _size = size;
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string(%zu)"
                    ": size is too big\n", size
                );

                abort();
            }
        }

        constexpr static_basic_string(const CharT* cstr)
        {
            _size = (cstr != nullptr) ? _str_utils::length(cstr) : 0;

            if (_size < N)
            {
                if (cstr != nullptr)
                {
                    _str_utils::copy(_data, cstr, _size);
                }

                _data[_size] = static_cast<CharT>(0);
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string(%s)"
                    ": cstr is too big to be contained\n", cstr
                );

                abort();
            }
        }

        constexpr static_basic_string(const CharT* cstr, usize size)
        {
            if (size < N)
            {
                _size = size;
                
                if (cstr != nullptr)
                {
                    _str_utils::copy(_data, cstr, _size);
                }

                _data[_size] = static_cast<CharT>(0);
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string(%s, %zu)"
                    ": size is too big\n", cstr, size
                );

                abort();
            } 
        }

        constexpr static_basic_string(basic_string_view<CharT> view)
            : static_basic_string(view.data(), view.size())
        {}

        constexpr static_basic_string(const static_basic_string& other)
        {
            _size = other._size;
            _str_utils::copy(_data, other._data, _size);
        }

        template <usize N2>
        constexpr static_basic_string(const static_basic_string<CharT, N2>& other)
        {
            if (other._size < N)
            {
                _size = other._size;
                _str_utils::copy(_data, other._data, _size);
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string(%s)"
                    ": other is too big to be contained\n", other._data
                );

                abort();
            }
        }

        template <typename CharT2, usize N2>
        constexpr static_basic_string(const static_basic_string<CharT2, N2>& other)
        {
            auto _len = unicode::length(other._data);

            if (_len < N2)
            {
                _size = _len;
                
                unicode::convert_to(_data, _size + 1, other._data).unwrap();
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string(%s, %zu)"
                    ": cstr is too big to be contained\n", other._data, other._size
                );

                abort();
            }
        }

        constexpr static_basic_string(static_basic_string&& other)
        {
            swap(_size, other._size);
            move(other._data, other._data + _size, _data);
        }

        template <usize N2>
        constexpr static_basic_string(static_basic_string<CharT, N2>&& other)
        {
            if (other._size < N)
            {
                swap(_size, other._size);
                move(other._data, other._data + _size, _data);
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string(%s, %zu)"
                    ": other is too big to be contained\n", other._data
                );
            }
        }

        constexpr static_basic_string& operator=(const CharT* rhs)
        {
            _size = _str_utils::length(rhs);

            if (_size < N)
            {
                _str_utils::copy(_data, rhs, _size);
            }
            else
            {
                fprintf(
                    stderr, "Error at operator=(%s): rhs"
                    " is too big to be contained\n", rhs
                );

                abort();
            }

            return *this;
        }

        constexpr static_basic_string& operator=(const basic_string_view<CharT>& rhs)
        {
            if (rhs.size() < N)
            {
                _size = rhs.size();
                copy_n(rhs.data(), _size, _data);
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string::operator"
                    "=(%s): rhs is too big to be contained\n", rhs
                );

                abort();
            }

            return *this;
        }

        template <usize N2> requires (N2 <= N)
        constexpr static_basic_string& operator=(
            const static_basic_string<CharT, N2>& rhs)
        {
            _size = rhs._size;
            copy_n(rhs.c_str(), _size, _data);
            return *this;
        }

        template <typename CharT2, usize N2>
        constexpr static_basic_string& operator=(
            const static_basic_string<CharT2, N2>& rhs)
        {
            auto _len = unicode::length(rhs);

            if (_len < N2)
            {
                _size = _len;

                unicode::convert_to(_data, _size + 1, rhs, _size).unwrap();
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string::operator"
                    "=(%s): rhs is too big to be contained\n", rhs
                );

                abort();
            }

            return *this;
        }

        template <usize N2> requires (N2 <= N)
        constexpr static_basic_string& operator=(static_basic_string<CharT, N2>&& rhs)
        {
            swap(_size, rhs._size);
            swap(_capacity, rhs._capacity);
            move(rhs._data, rhs._data + _size, _data);
            return *this;
        }

        constexpr CharT& operator[](usize index)
        {
            return _data[index];
        }

        constexpr const CharT& operator[](usize index) const
        {
            return _data[index];
        }

        template <usize N2>
        constexpr i32 operator<=>(const static_basic_string<CharT, N2>& rhs) const
        {
            return _str_utils::compare(_data, rhs._data);
        }

        template <usize N2>
        constexpr bool operator==(const static_basic_string<CharT, N2>& rhs) const
        {
            return _size == rhs._size && operator<=>(rhs) == 0;
        }

        constexpr auto at(usize index)
            -> result<reference<CharT>, bad_access>
        {
            if(index >= _size)
                return bad_access{};

            return {_data[index]};
        }

        constexpr auto at(usize index) const
            -> result<reference<const CharT>, bad_access>
        {
            if(index >= _size)
                return bad_access{};

            return {_data[index]};
        }

        template <usize N2>
        constexpr usize find(
            const static_basic_string<CharT, N2>& str, usize pos = 0) const
        {
            if (pos >= _size)
            {
                return npos;
            }
            else
            {
                const CharT* _find_addr = _str_utils::find(
                    &_data[pos], str._data
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize find(const CharT* str, usize pos = 0) const
        {
            if (pos >= _size)
            {
                return npos;
            }
            else
            {
                const CharT* _find_addr = _str_utils::find(
                    &_data[pos], str
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize find(CharT letter, usize pos = 0) const
        {
            if (pos >= _size)
            {
                return npos;
            }
            else
            {
                const CharT* _find_addr = _str_utils::find(
                    &_data[pos], letter
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        template <usize N2>
        constexpr usize rfind(
            const static_basic_string<CharT, N2>& str, usize pos = npos) const
        {
            if (pos >= _size && pos != npos)
            {
                return npos;
            }
            else if (pos == npos)
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str._data, _size
                );

                if(_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
            else
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str._data, _size - pos
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize rfind(const CharT* str, usize pos = npos) const
        {
            if (pos >= _size && pos != npos)
            {
                return npos;
            }
            else if (pos == npos)
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str, _size
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
            else
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str, _size - pos
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr usize rfind(CharT str, usize pos = npos) const
        {
            if (pos >= _size && pos != npos)
            {
                return npos;
            }
            else if (pos == npos)
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str, _size
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
            else
            {
                const CharT* _find_addr = _str_utils::find_rev(
                    &_data[pos], str, _size - pos
                );

                if (_find_addr == nullptr)
                {
                    return npos;
                }
                else
                {
                    return static_cast<usize>(_find_addr - _data);
                }
            }
        }

        constexpr bool starts_with(CharT letter) const
        {
            if (_size != 0)
                return _data[0] == letter;

            return false;
        }

        constexpr bool starts_with(const CharT* str) const
        {
            if (_size != 0)
                return find(str) == 0;

            return false;
        }

        template <usize N2>
        constexpr bool starts_with(
            const static_basic_string<CharT, N2>& str) const
        {
            if (_size != 0)
                return find(str) == 0;

            return false;
        }

        constexpr bool contains(CharT letter) const
        {
            if (_size != 0)
                return find(letter) != npos;

            return false;
        }

        constexpr bool contains(const CharT* str) const
        {
            if (_size != 0)
                return find(str) != npos;

            return false;
        }

        template <usize N2>
        constexpr bool contains(
            const static_basic_string<CharT, N2>& str) const
        {
            if (_size != 0)
                return find(str) != npos;

            return false;
        }

        constexpr bool ends_with(CharT letter) const
        {
            if (_size != 0)
                return _data[_size - 1] == letter;

            return false;
        }

        constexpr bool ends_with(const CharT* str) const
        {
            usize _len = cstring::length(str);

            if (_size != 0)
                return rfind(str) == (_size - _len);

            return false;
        }

        template <usize N2>
        constexpr bool ends_with(
            const static_basic_string<CharT, N2>& str) const
        {
            if (_size != 0)
                return rfind(str) == (_size - str._size);

            return false;
        }

        constexpr auto erase(const_iterator pos)
            -> result<iterator, bad_access>
        {
            return erase_for(pos, pos + 1);
        }

        constexpr auto erase_for(const_iterator from, const_iterator to)
            -> result<iterator, bad_access>
        {
            if (from < begin() || from > end() || to < begin() || to > end() || from > to)
                return bad_access{};

            usize _current_pos = static_cast<usize>(from - begin());
            usize _last_pos = static_cast<usize>(to - begin());

            for (usize _index = 0; _index < _capacity - _last_pos + 1; _index++)
            {
                this->_data[_current_pos + _index] = 
                    move(this->_data[_last_pos + _index]);
            }

            _size -= static_cast<usize>(to - from) + 1;
            return begin() + _last_pos;
        }

        template <typename... Args>
        constexpr void emplace_back(Args&&... args)
        {
            if (_size < _capacity)
            {
                _data[_size] = CharT{forward<Args>(args)...};
                _data[++_size] = '\0';
            }
            else
            {
                fprintf(
                    stderr, "Error at static_basic_string"
                    ":: emplace_back: String is full\n"
                );

                abort();
            }
        }

        constexpr void push_back(const CharT& val)
        {
            emplace_back(val);
        }

        constexpr void push_back(CharT&& val)
        {
            emplace_back(move(val));
        }

        constexpr void clear()
        {
            if (_capacity != 0)
            {
                _data[0] = '\0';
                _size = 0;
            }
        }

        constexpr void resize(usize size)
        {
            if (size > _capacity)
            {
                fprintf(
                    stderr, "Error at static_basic_string::resize"
                    "(%zu): size is bigger than capacity\n", size
                );

                abort();
            }
            else if (size < _capacity)
            {
                _size = size;
            }
        }

        constexpr void pop_back()
        {
            if (_size != 1)
            {
                _data[--_size] = '\0';
            }
            else if (_data[0] != '\0')
            {
                _data[0] = '\0';
            }
        }

        constexpr CharT& front()
        {
            return _data[0];
        }

        constexpr CharT& back()
        {
            return _data[_size - 1];
        }

        constexpr usize size() const
        {
            return (_size != 0) ? _size + 1 : 0;
        }

        constexpr usize length() const
        {
            return _size;
        }

        constexpr usize capacity() const
        {
            return _capacity;
        }

        constexpr iterator data()
        {
            return _data;
        }

        constexpr const_iterator data() const
        {
            return _data;
        }

        constexpr const_iterator c_str() const
        {
            return _data;
        }

        constexpr iterator begin()
        {
            return data();
        }

        constexpr const_iterator begin() const
        {
            return data();
        }

        constexpr iterator end()
        {
            return begin() + length();
        }

        constexpr const_iterator end() const
        {
            return begin() + length();
        }

        constexpr const_iterator cbegin() const
        {
            return begin();
        }

        constexpr const_iterator cend() const
        {
            return end();
        }

        explicit constexpr operator basic_string_view<CharT>() const
        {
            return basic_string_view<CharT>(_data, _size);
        }
    };

    template <format_output fmt, typename CharT, CharacterType T, usize N>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, const static_basic_string<T, N>& val)
    {
        using FmtType = decltype(fmt);

        if constexpr (fmt.tag != FmtType::none)
        {
            return runtime_error{"Unknown format tag for character type"};
        }

        usize _write_sz = (fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

        if (_write_sz > sz)
        {
            return runtime_error{"Buffer too small for printing"};
        }

        str = _print_to_impl(str, fmt.format);
        str = _print_to_impl(str, fmt.style);

        auto _res = unicode::convert_to<CharT, T>(str, sz, val.data(), val.size() - 1);

        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }

        usize _pos = sz - _res.unwrap() - 1;
        str = _print_to_impl(str + _pos, fmt.reset);

        return _pos + _write_sz;
    }

    template <typename T>
    static constexpr auto to_string(T val)
    {
        const auto _res = to_pseudo_string<char>(val);
        return basic_string<char>{_res.data(), _res.size()};
    }

    static constexpr auto& to_string(basic_string<char>& val)
    {
        return val;
    }

    template <typename T>
    static constexpr auto to_wstring(T val)
    {
        const auto _res = to_pseudo_string<wchar>(val);
        return basic_string<wchar>{_res.data(), _res.size()};
    }

    static constexpr auto& to_wstring(basic_string<wchar>& val)
    {
        return val;
    }

    template <typename T>
    static constexpr auto to_u8string(T val)
    {
        const auto _res = to_pseudo_string<char8>(val);
        return basic_string<char8>{_res.data(), _res.size()};
    }

    static constexpr auto& to_u8string(basic_string<char8>& val)
    {
        return val;
    }

    template <typename T>
    static constexpr auto to_u16string(T val)
    {
        const auto _res = to_pseudo_string<char16>(val);
        return basic_string<char16>{_res.data(), _res.size()};
    }

    static constexpr auto& to_u16string(basic_string<char16>& val)
    {
        return val;
    }

    template <typename T>
    static constexpr auto to_u32string(T val)
    {
        const auto _res = to_pseudo_string<char32>(val);
        return basic_string<char32>{_res.data(), _res.size()};
    }

    static constexpr auto& to_u32string(basic_string<char32>& val)
    {
        return val;
    }

    template <typename HashType, typename CharT>
    struct hash<HashType, basic_string<CharT>>
    {
        using ResultType = HashType;

        static constexpr ResultType get_hash(const basic_string<CharT>& str) {
            return hash<HashType, const CharT*>::get_hash(str.begin(), str.end() - 1);
        }
    };

    template <typename CharT>
    struct count_sorter<basic_string<CharT>>
    {
        static constexpr void sort(
            basic_string<CharT>& arr, usize exp)
        {
            basic_string<CharT> _output = {};
            usize _index, _count[10] = {};
            constexpr CharT val = {};

            for (_index = 0; _index < arr.length(); _index++)
                _output.push_back(val);

            for (_index = 0; _index < arr.length(); _index++)
            {
                _count[(arr[_index] / exp) % 10]++;
            }

            for (_index = 1; _index < 10; _index++)
            {
                _count[_index] += _count[_index - 1];
            }

            for (_index = arr.length() - 1; _index > 0; _index--)
            {
                _output[_count[(arr[_index] / exp) % 10] - 1] = arr[_index];
                _count[(arr[_index] / exp) % 10]--;
            }

            _output[_count[(arr[0] / exp) % 10] - 1] = arr[0];
            _count[(arr[0] / exp) % 10]--;

            for (_index = 0; _index < arr.length(); _index++)
            {
                swap(arr[_index], _output[_index]);
            }
        }

        static constexpr void sort(
            basic_string<CharT>& arr, usize exp, auto&& get_digit)
        {
            basic_string<CharT> _output = {};
            usize _index, _count[10] = {};
            constexpr CharT val = {};

            for (_index = 0; _index < arr.length(); _index++)
                _output.push_back(val);

            for (_index = 0; _index < arr.length(); _index++)
            {
                _count[(get_digit(arr[_index]) / exp) % 10]++;
            }

            for (_index = 1; _index < 10; _index++)
            {
                _count[_index] += _count[_index - 1];
            }

            for (_index = arr.length() - 1; _index > 0; _index--)
            {
                _output[_count[(get_digit(arr[_index]) / exp) % 10] - 1] = get_digit(arr[_index]);
                _count[(get_digit(arr[_index]) / exp) % 10]--;
            }

            _output[_count[(get_digit(arr[0]) / exp) % 10] - 1] = get_digit(arr[0]);
            _count[(get_digit(arr[0]) / exp) % 10]--;

            for (_index = 0; _index < arr.length(); _index++)
            {
                swap(arr[_index], _output[_index]);
            }
        }
    };

    template <typename HashType, typename CharT, usize N>
    struct hash<HashType, static_basic_string<CharT, N>>
    {
        using ResultType = HashType;

        static constexpr ResultType get_hash(const static_basic_string<CharT, N>& str) {
            return hash<HashType, const CharT*>::get_hash(str.begin(), str.end() - 1);
        }
    };

    using string = basic_string<char>;
    using wstring = basic_string<wchar>;
    using u8string = basic_string<char8>;
    using u16string = basic_string<char16>;
    using u32string = basic_string<char32>;
    
    template <usize N>
    using static_string = static_basic_string<char, N>;
    
    template <usize N>
    using static_wstring = static_basic_string<wchar, N>;
    
    template <usize N>
    using static_u8string = static_basic_string<char8, N>;
    
    template <usize N>
    using static_u16string = static_basic_string<char16, N>;
    
    template <usize N>
    using static_u32string = static_basic_string<char32, N>;

    namespace string_literals
    {
        static constexpr auto operator""_s(const char* str, usize size)
        {
            return string{str, size};
        }

        static constexpr auto operator""_s(const char8* str, usize size)
        {
            return u8string{str, size};
        }

        static constexpr auto operator""_s(const char16* str, usize size)
        {
            return u16string{str, size};
        }

        static constexpr auto operator""_s(const char32* str, usize size)
        {
            return u32string{str, size};
        }

        static constexpr auto operator""_s(const wchar* str, usize size)
        {
            return wstring{str, size};
        }
    } // namespace string_literals

    namespace static_string_literals
    {
        template <static_string str>
        static constexpr auto operator""_s()
        {
            return str;
        }

        template <static_u8string str>
        static constexpr auto operator""_s()
        {
            return str;
        }

        template <static_u16string str>
        static constexpr auto operator""_s()
        {
            return str;
        }

        template <static_u32string str>
        static constexpr auto operator""_s()
        {
            return str;
        }

        template <static_wstring str>
        static constexpr auto operator""_s()
        {
            return str;
        }
    } // namespace static_string_literals
} // namespace hsd
