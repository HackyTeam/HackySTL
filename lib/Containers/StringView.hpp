#pragma once

#include "../Base/CString.hpp"
#include "../Base/Hash.hpp"
#include "../Serialize/_PrinterImpl.hpp"
#include "../Base/Types.hpp"

namespace hsd
{
    struct bad_access
    {
        const char* operator()() const
        {
            return "Tried to access an element out of bounds";
        }
    };
    
    struct out_of_range
    {
        const char* operator()() const
        {
            return "Out of range";
        }
    };

    template <typename CharT>
    class basic_string_view
    {
    public:
        using value_type = CharT;
        using const_reference = const CharT&;
        using const_pointer = const CharT*;
        using const_iterator = const_pointer;

    private:
        using cstring_utils = basic_cstring<CharT>;
        const_pointer _data = nullptr;
        usize _size = 0;

    public:
        static constexpr usize npos = ~static_cast<usize>(0u);

        constexpr basic_string_view() = default;

        constexpr basic_string_view(const_pointer cstr)
            : _data{cstr}, _size{cstring_utils::length(cstr)}
        {}

        constexpr basic_string_view(const_pointer str, usize size)
            : _data{str}, _size{size}
        {}

        template <usize N>
        constexpr basic_string_view(const CharT (&str)[N])
            : _data{str}, _size{N - 1}
        {}

        constexpr void reset()
        {
            _data = nullptr;
            _size = 0;
        }

        // No negative index supported
        constexpr const_reference operator[](usize index) const
        {
            return _data[index];
        }

        constexpr auto at(usize index) const
            -> result<reference<const CharT>, bad_access>
        {
            if(index >= _size)
                return bad_access{};

            return {_data[index]};
        }

        constexpr i32 operator<=>(const basic_string_view& rhs) const
        {
            auto _cmp_size = (rhs._size > _size) ? _size : rhs._size;
            return cstring_utils::compare(_data, rhs._data, _cmp_size);
        }

        constexpr bool operator==(const basic_string_view& rhs) const
        {
            return _size == rhs._size && operator<=>(rhs) == 0;
        }

        constexpr usize find_first_of(const basic_string_view& other) const
        {
            for (const auto* _str = _data; _str != end(); _str++)
            {
                for (const auto* _other_str = other._data; _other_str != other.end(); ++_other_str)
                {
                    if (*_str == *_other_str)
                    {
                        return static_cast<usize>(_str - _data);
                    }
                }
            }

            return npos;
        }

        constexpr usize find_first_of(const CharT* other) const
        {
            for (const auto* _str = _data; _str != end(); _str++)
            {
                for (const auto* _other_str = other; *_other_str != '\0'; ++_other_str)
                {
                    if (*_str == *_other_str)
                    {
                        return static_cast<usize>(_str - _data);
                    }
                }
            }

            return npos;
        }
        
        constexpr usize find(char letter) const
        {
            const auto* _str = _data;

            for (; _str != _data + _size; _str++)
            {
                if (*_str == letter)
                {
                    break;
                }
            }

            if (_str == _data + _size)
            {
                return npos;
            }
            else
            {
                return static_cast<usize>(_str - _data);
            }
        }

        constexpr usize find(char letter, usize pos) const
        {
            if (pos >= size())
            {
                return npos;
            }

            const auto* _str = _data + pos;

            for (; _str != _data + _size; _str++)
            {
                if (*_str == letter)
                {
                    break;
                }
            }

            if (_str == _data + _size)
            {
                return npos;
            }
            else
            {
                return static_cast<usize>(_str - _data);
            }
        }

        constexpr usize find(const basic_string_view& other) const
        {
            const auto* _str = _data;

            for (; _str != _data + _size; _str++)
            {
                if (
                    (*_str == *other.data()) && 
                    cstring_utils::compare(_str, other.data(), other.size()) == 0
                )
                {
                    break;
                }
            }

            if (_str == _data + _size)
            {
                return npos;
            }
            else
            {
                return static_cast<usize>(_str - _data);
            }
        }

        constexpr usize find(const basic_string_view& other, usize pos) const
        {
            if (pos >= size())
            {
                return npos;
            }

            const auto* _str = _data + pos;

            for (; _str != _data + _size; _str++)
            {
                if (
                    (*_str == *other.data()) && 
                    cstring_utils::compare(_str, other.data(), other.size()) == 0
                )
                {
                    break;
                }
            }

            if (_str == _data + _size)
            {
                return npos;
            }
            else
            {
                return static_cast<usize>(_str - _data);
            }
        }

        constexpr usize rfind(char letter) const
        {
            const auto* _str = _data + _size - 1;

            for (; _str != _data - 1; _str--)
            {
                if (*_str == letter)
                {
                    break;
                }
            }

            if (_str == _data - 1)
            {
                return npos;
            }
            else
            {
                return static_cast<usize>(_str - _data);
            }
        }

        constexpr usize rfind(char letter, usize pos) const
        {
            if (pos >= size())
            {
                return npos;
            }

            const auto* _str = _data + _size - 1;

            for (; _str != _data - pos - 1; _str--)
            {
                if (*_str == letter)
                {
                    break;
                }
            }
                

            if (_str == _data - pos - 1)
            {
                return npos;
            }
            else
            {
                return static_cast<usize>(_str - _data);
            }
        }

        constexpr usize rfind(const basic_string_view& other) const
        {
            const auto* _str = _data + _size - 1;

            for (; _str != _data - 1; _str--)
            {
                if (
                    (*_str == *other.data()) && 
                    cstring_utils::compare(_str, other.data(), other.size()) == 0
                )
                {
                    break;
                }
            }

            if (_str == _data - 1)
            {
                return npos;
            }
            else
            {
                return static_cast<usize>(_str - _data);
            }
        }

        constexpr usize rfind(const basic_string_view& other, usize pos) const
        {
            if (pos >= size())
            {
                return npos;
            }

            const auto* _str = _data + _size - 1;

            for (; _str != _data - pos - 1; _str--)
            {
                if (
                    (*_str == *other.data()) && 
                    cstring_utils::compare(_str, other.data(), other.size()) == 0
                )
                {
                    break;
                }
            }

            if (_str == _data - pos - 1)
            {
                return npos;
            }
            else
            {
                return static_cast<usize>(_str - _data);
            }
        }

        constexpr bool starts_with(CharT letter) const
        {
            if (_data != nullptr)
                return _data[0] == letter;

            return false;
        }

        constexpr bool starts_with(const CharT* str) const
        {
            if (_data != nullptr)
                return find(str) == 0;

            return false;
        }

        constexpr bool starts_with(const basic_string_view& str) const
        {
            if (_data != nullptr)
                return find(str) == 0;

            return false;
        }

        constexpr bool contains(CharT letter) const
        {
            if (_data != nullptr)
                return find(letter) != npos;

            return false;
        }

        constexpr bool contains(const CharT* str) const
        {
            if (_data != nullptr)
                return find(str) != npos;

            return false;
        }

        constexpr bool contains(const basic_string_view& str) const
        {
            if (_data != nullptr)
                return find(str) != npos;

            return false;
        }

        constexpr bool ends_with(CharT letter) const
        {
            if (_data != nullptr)
                return _data[size() - 1] == letter;

            return false;
        }

        constexpr bool ends_with(const CharT* str) const
        {
            usize _len = cstring::length(str);

            if (_data != nullptr)
                return rfind(str) == size() - _len;

            return false;
        }

        constexpr bool ends_with(const basic_string_view& str) const
        {
            if (_data != nullptr)
                return rfind(str) == size() - str.size();

            return false;
        }

        constexpr usize size() const
        {
            return _size;
        }

        constexpr usize capacity() const
        {
            return _size;
        }

        constexpr const_pointer data() const
        {
            return _data;
        }

        constexpr const_iterator begin() const
        {
            return _data;
        }

        constexpr const_iterator end() const
        {
            return _data + _size;
        }

        constexpr const_iterator rbegin() const
        {
            return _data + _size - 1;
        }

        constexpr const_iterator rend() const
        {
            return _data - 1;
        }

        constexpr auto sub_string(usize start, usize len) const
            -> result<basic_string_view, out_of_range>
        {
            if (start >= _size)
                return out_of_range{};

            const usize max_length = _size - start;
            return basic_string_view(_data + start, len < max_length ? len : max_length);
        }
    };

    template <format_output fmt, typename CharT, CharacterType T>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, const basic_string_view<T>& val)
    {
        using FmtType = decltype(fmt);

        if constexpr (fmt.tag != FmtType::none)
        {
            return runtime_error{"Unknown format tag for character type"};
        }

        usize _write_sz = (fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

        if (_write_sz > sz)
        {
            return runtime_error{"Buffer too small for printing"};
        }

        str = _print_to_impl(str, fmt.format);
        str = _print_to_impl(str, fmt.style);

        auto _res = unicode::convert_to<CharT, T>(str, sz, val.data(), val.size() - 1);

        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }

        usize _pos = sz - _res.unwrap() - 1;
        str = _print_to_impl(str + _pos, fmt.reset);

        return _pos + _write_sz;
    }

    namespace string_view_literals
    {
        constexpr auto operator""_sv(const char* str, usize size)
        {
            return basic_string_view<char>{str, size};
        }

        constexpr auto operator""_sv(const char8* str, usize size)
        {
            return basic_string_view<char8>{str, size};
        }

        constexpr auto operator""_sv(const char16* str, usize size)
        {
            return basic_string_view<char16>{str, size};
        }

        constexpr auto operator""_sv(const char32* str, usize size)
        {
            return basic_string_view<char32>{str, size};
        }

        constexpr auto operator""_sv(const wchar* str, usize size)
        {
            return basic_string_view<wchar>{str, size};
        }
    } // namespace string_view_literals

    template <typename HashType, typename CharT>
    struct hash<HashType, basic_string_view<CharT>>
    {
        using ResultType = HashType;

        static constexpr ResultType get_hash(basic_string_view<CharT> view) {
            return hash<HashType, const CharT*>::get_hash(view.begin(), view.end());
        }
    };

    using string_view = basic_string_view<char>;
    using wstring_view = basic_string_view<wchar>;
    using u8string_view = basic_string_view<char8>;
    using u16string_view = basic_string_view<char16>;
    using u32string_view = basic_string_view<char32>;
} // namespace hsd
