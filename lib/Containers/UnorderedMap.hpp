#pragma once

#include "../Wrappers/Pair.hpp"
#include "Vector.hpp"
#include "../Base/Hash.hpp"

namespace hsd
{
    namespace umap_detail
    {
        template < typename Allocator >
        concept DefaultAlloc = DefaultConstructible<Allocator>;
        template < typename Allocator >
        concept CopyAlloc = CopyConstructible<Allocator>;

        struct bad_key
        {
            constexpr const char* pretty_error() const
            {
                return "Tried to use an invalid key";
            }
        };

        struct bad_access
        {
            constexpr const char* pretty_error() const
            {
                return "Tried to access an element out of bounds";
            }
        };
    } // namespace umap_detail

    template< typename Key, typename T, 
        typename Hasher = hash<usize, Key>,
        typename Allocator = allocator >
    class unordered_map
    {
    private:
        using ref_value = pair< typename Hasher::ResultType, usize >;
        using ref_vector = vector< ref_value, Allocator >;
        using bucket_iter = typename ref_vector::iterator;

        vector< ref_vector, Allocator > _buckets;
        vector< pair<Key, T>, Allocator > _data;

        constexpr void _replace(usize new_size)
        {
            for (auto& _bucket : _buckets)
            {
                _bucket.clear();
            }
            
            _buckets.reserve(new_size);

            for (usize _index = new_size; _index < _buckets.size(); _index++)
            {
                _buckets.pop_back();
            }

            for (usize _index = _buckets.size(); _index < new_size; _index++)
            {
                _buckets.emplace_back(_buckets.get_allocator());
            }

            for (usize _index = 0; _index < _data.size(); _index++)
            {
                auto _hash_rez = Hasher::get_hash(_data[_index].first);
                usize _bucket_index = _hash_rez % new_size;
                _buckets[_bucket_index].emplace_back(_hash_rez, _index);
            }
        }

        constexpr pair<usize, usize> _get(const Key& key) const
        {
            auto _key_hash = Hasher::get_hash(key);
            usize _index = _key_hash % _buckets.size();

            for (auto& _val : _buckets[_index])
            {
                if (_key_hash == _val.first && _data[_val.second].first == key)
                    return {_val.second, _index};
            }

            return {static_cast<usize>(-1), _index};
        }

        constexpr auto _get_iter(const Key& key)
            -> result< pair<bucket_iter, usize>, umap_detail::bad_access >
        {
            auto _key_hash = Hasher::get_hash(key);
            usize _index = _key_hash % _buckets.size();

            for (auto _val = _buckets[_index].begin(); 
                _val != _buckets[_index].end(); _val++)
            {
                if (_key_hash == _val->first && _data[_val->second].first == key)
                    return pair{_val, _index};
            }

            return umap_detail::bad_access{};
        }

    public:
        using reference_type = T&;
        using iterator = typename vector<pair<Key, T>>::iterator;
        using const_iterator = typename vector<pair<Key, T>>::const_iterator;

        constexpr ~unordered_map() {}

        constexpr unordered_map()
        requires (umap_detail::DefaultAlloc<Allocator>)
            : _buckets(10)
        {}

        constexpr unordered_map(const Allocator& alloc)
            : _buckets(alloc), _data(alloc)
        {
            _buckets.reserve(10);

            for (hsd::usize _idx = 0; _idx <= 10; ++_idx)
            {
                _buckets.emplace_back(alloc);
            }
        }

        constexpr unordered_map(const unordered_map& other)
            : _buckets{other._buckets}, _data{other._data}
        {}

        constexpr unordered_map(unordered_map&& other)
            : _buckets{move(other._buckets)}, _data{move(other._data)}
        {}

        template <usize N>
        constexpr unordered_map(const pair<Key, T> (&other)[N])
        requires (umap_detail::DefaultAlloc<Allocator>)
            : _buckets(10)
        {
            for (usize _index = 0; _index < N; _index++)
            {
                emplace(
                    other[_index].first, 
                    other[_index].second
                );
            }
        }

        template <usize N>
        constexpr unordered_map(pair<Key, T> (&&other)[N])
        requires (umap_detail::DefaultAlloc<Allocator>)
            : _buckets(10)
        {
            for (usize _index = 0; _index < N; _index++)
            {
                emplace(
                    move(other[_index].first), 
                    move(other[_index].second)
                );
            }
        }

        constexpr unordered_map& operator=(unordered_map&& rhs)
        {
            _buckets = move(rhs._buckets);
            _data = move(rhs._data);
            return *this;
        }

        constexpr unordered_map& operator=(const unordered_map& rhs)
        {
            clear();

            for (pair<Key, T>& val : rhs._data)
                emplace(val.first, val.second);

            return *this;
        }

        template <usize N>
        constexpr unordered_map& operator=(const pair<Key, T>(&rhs)[N])
        {
            clear();

            for (usize _index = 0; _index < N; _index++)
                emplace(rhs[_index].first, rhs[_index].second);

            return *this;
        }

        template <usize N>
        constexpr unordered_map& operator=(pair<Key, T>(&&rhs)[N])
        {
            clear();

            for (usize _index = 0; _index < N; _index++)
            {
                emplace(
                    move(rhs[_index].first), 
                    move(rhs[_index].second)
                );
            }

            return *this;
        }

        constexpr auto& operator[](const Key& key)
        {
            return emplace(key).first->second;
        }

        constexpr auto& operator[](Key&& key)
        {
            return emplace(forward<Key>(key)).first->second;
        }

        constexpr const auto& operator[](const Key& key) const
        {
            return at(key).unwrap().get();
        }

        constexpr auto at(const Key& key)
            -> result<reference<T>, umap_detail::bad_key>
        {
            usize _data_index = _get(key).first;

            if (_data_index == static_cast<usize>(-1))
                return umap_detail::bad_key{};

            return {_data[_data_index].second};
        }

        constexpr auto at(const Key& key) const
            -> result<reference<const T>, umap_detail::bad_key>
        {
            usize _data_index = _get(key).first;

            if (_data_index == static_cast<usize>(-1))
                return umap_detail::bad_key{};

            return {_data[_data_index].second};
        }

        template< typename NewKey, typename... Args >
        constexpr pair<iterator, bool> emplace(const NewKey& key, const Args&... args)
        {
            auto [_data_index, _bucket_index] = _get(key);

            if (_data_index != static_cast<usize>(-1))
            {
                return {_data.begin() + _data_index, false};
            }
            else
            {
                _data.emplace_back(key, T{forward<Args>(args)...});

                if (_data.size() > (_buckets.size() / 4 * 3))
                {
                    _replace(_buckets.size() + _buckets.size() / 2);
                }
                else
                {
                    _buckets[_bucket_index].emplace_back(
                        Hasher::get_hash(_data.back().first), _data.size() - 1
                    );
                }

                return {_data.end() - 1, true};
            }
        }

        template< typename NewKey, typename... Args >
        constexpr pair<iterator, bool> emplace(NewKey&& key, Args&&... args)
        {
            auto [_data_index, _bucket_index] = _get(key);

            if (_data_index != static_cast<usize>(-1))
            {
                return {_data.begin() + _data_index, false};
            }
            else
            {
                _data.emplace_back(move(key), move(T{forward<Args>(args)...}));

                if (_data.size() > (_buckets.size() / 4 * 3))
                {
                    _replace(_buckets.size() + _buckets.size() / 2);
                }
                else
                {
                    _buckets[_bucket_index].emplace_back(
                        Hasher::get_hash(_data.back().first), _data.size() - 1
                    );
                }

                return {_data.end() - 1, true};
            }
        }

        constexpr auto erase(const_iterator pos)
            -> result<iterator, umap_detail::bad_access>
        {
            auto _new_iter = _data
                .erase(pos)
                .unwrap();
            
            _replace(_buckets.size());
            return _new_iter;
        }

        constexpr void clear()
        {
            _data.clear();

            for (auto& _bucket : _buckets)
            {
                _bucket.clear();
            }
        }

        constexpr usize size() const
        {
            return _data.size();
        }

        constexpr iterator begin()
        {
            return _data.begin();
        }

        constexpr const_iterator begin() const
        {
            return cbegin();
        }

        constexpr iterator end()
        {
            return _data.end();
        }

        constexpr const_iterator end() const
        {
            return cend();
        }

        constexpr const_iterator cbegin() const
        {
            return _data.cbegin();
        }

        constexpr const_iterator cend() const
        {
            return _data.cend();
        }

        constexpr iterator rbegin()
        {
            return _data.rbegin();
        }

        constexpr const_iterator rbegin() const
        {
            return crbegin();
        }

        constexpr iterator rend()
        {
            return _data.rend();
        }

        constexpr const_iterator rend() const
        {
            return crend();
        }

        constexpr const_iterator crbegin() const
        {
            return _data.crbegin();
        }

        constexpr const_iterator crend() const
        {
            return _data.crend();
        }
    };

    template < typename Key, typename T, usize N >
    unordered_map(pair<Key, T> (&&other)[N]) 
        -> unordered_map<Key, T, hash<usize, Key>>;

    template < typename Key, typename T, usize N >
    unordered_map(const pair<Key, T> (&other)[N]) 
        -> unordered_map<Key, T, hash<usize, Key>>;

    template < typename Key, typename T >
    using buffered_umap = unordered_map<
        Key, T, hash<usize, Key>, buffered_allocator
    >;
} // namespace hsd