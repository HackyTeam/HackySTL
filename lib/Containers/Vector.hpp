#pragma once

#include "../Base/Allocator.hpp"
#include "Span.hpp"
#include "StackArray.hpp"
#include "../Wrappers/Reference.hpp"
#include "../Base/IntegerSequence.hpp"

namespace hsd
{
    template < typename T, typename Allocator = allocator >
    class vector
    {
    private:
        using alloc_type = Allocator;
        alloc_type _alloc;
        T* _data = nullptr;
        usize _capacity = 0;

    protected:
        usize _size = 0;

        struct bad_access
        {
            constexpr const char* pretty_error() const
            {
                return "Tried to access an element out of bounds";
            }
        };

    public:
        using value_type = T;
        using iterator = T*;
        using const_iterator = const T*;

        constexpr ~vector()
        {
            for (usize _index = _size; _index > 0; --_index)
                at_unchecked(_index - 1).~T();
                
            _alloc.deallocate(_data, _capacity).unwrap();
        }

        constexpr vector() 
        requires (DefaultConstructible<alloc_type>) = default;

        constexpr vector(usize size)
        requires (DefaultConstructible<alloc_type>)
            : _alloc{}
        {
            resize(size);
        }

        constexpr vector(const alloc_type& alloc)
            : _alloc{alloc}
        {}

        constexpr vector(usize size, const alloc_type& alloc)
            : _alloc{alloc}
        {
            resize(size);
        }

        constexpr vector(const vector& other)
            : _alloc{other._alloc}, 
            _capacity{other._capacity}, 
            _size{other._size}
        {
            _data = _alloc
                .template allocate<T>(other._capacity)
                .unwrap();

            for (usize _index = 0; _index < _size; ++_index)
                construct_at(&_data[_index], other[_index]);
        }

        constexpr vector(vector&& other)
            : _alloc{move(other._alloc)},
            _data{exchange(other._data, nullptr)},
            _capacity{exchange(other._capacity, 0u)},
            _size{exchange(other._size, 0u)}
        {}

        template <usize N>
        constexpr vector(const T (&arr)[N])
        requires (DefaultConstructible<alloc_type>)
            : _capacity{N}, _size{N}
        {
            _data = _alloc
                .template allocate<T>(N)
                .unwrap();

            for (usize _index = 0; _index < _size; ++_index)
                _alloc.construct_at(&_data[_index], arr[_index]);
        }

        template <usize N>
        constexpr vector(T (&&arr)[N])
        requires (DefaultConstructible<alloc_type>)
            : _capacity{N}, _size{N}
        {
            _data = _alloc
                .template allocate<T>(N)
                .unwrap();

            for (usize _index = 0; _index < _size; ++_index)
                construct_at(&_data[_index], move(arr[_index]));
        }

        template <usize N>
        constexpr vector& operator=(const T (&arr)[N])
        requires (DefaultConstructible<alloc_type>)
        {
            if (_capacity < N)
            {
                clear();
                reserve(N);
                
                for (usize _index = 0; _index < N; ++_index)
                {
                    construct_at(&_data[_index], arr[_index]);
                }
                
                _size = N;
            }
            else
            {
                usize _index;
                usize min_size = _size > N ? _size : N;
                
                for (_index = 0; _index < min_size; ++_index)
                {
                    _data[_index] = arr[_index];
                }
                if (_size > N)
                {
                    for (_index = _size; _index > N; --_index)
                        at_unchecked(_index - 1).~T();
                }
                else if (N > _size)
                {
                    for (; _index < N; ++_index)
                    {
                        construct_at(&_data[_index], arr[_index]);
                    }
                }
                
                _size = N;
            }

            return *this;
        }

        template <usize N>
        constexpr vector& operator=(T (&&arr)[N])
        requires (DefaultConstructible<alloc_type>)
        {
            if (_capacity < N)
            {
                clear();
                reserve(N);
                
                for (usize _index = 0; _index < N; ++_index)
                {
                    construct_at(&_data[_index], move(arr[_index]));
                }
                
                _size = N;
            }
            else
            {
                usize _index;
                usize min_size = _size > N ? _size : N;
                
                for (_index = 0; _index < min_size; ++_index)
                {
                    _data[_index] = move(arr[_index]);
                }
                if (_size > N)
                {
                    for (_index = _size; _index > N; --_index)
                        at_unchecked(_index - 1).~T();
                }
                else if (N > _size)
                {
                    for (; _index < N; ++_index)
                    {
                        construct_at(&_data[_index], move(arr[_index]));
                    }
                }

                _size = N;
            }

            return *this;
        }

        constexpr vector& operator=(const vector& rhs)
        {
            clear();

            _alloc = rhs._alloc;
            reserve(rhs._size);
                
            for (usize _index = 0; _index < rhs._size; ++_index)
            {
                emplace_back(rhs[_index]);
            }

            return *this;
        }

        constexpr vector& operator=(vector&& rhs)
        {
            _alloc = exchange(rhs._alloc, _alloc);
            _data = exchange(rhs._data, _data);            
            _size = exchange(rhs._size, _size);
            _capacity = exchange(rhs._capacity, _capacity);
    
            return *this;
        }

        constexpr Allocator get_allocator() const
        {
            return _alloc;
        }

        constexpr auto& operator[](usize index)
        {
            return at_unchecked(index);
        }

        constexpr auto& operator[](usize index) const
        {
            return at_unchecked(index);
        }

        constexpr auto& front()
        {
            return *begin();
        }

        constexpr auto& front() const
        {
            return *begin();
        }

        constexpr auto& back()
        {
            return *(begin() + size() - 1);
        }

        constexpr auto& back() const
        {
            return *(begin() + size() - 1);
        }

        constexpr auto erase(const_iterator pos)
            -> result<iterator, bad_access>
        {
            return erase_for(pos, pos + 1);
        }

        constexpr auto erase_for(const_iterator from, const_iterator to)
            -> result<iterator, bad_access>
        {
            if (from < begin() || from > end() || 
                to < begin() || to > end() || from > to)
                return bad_access{};

            if (to == end())
            {
                for (auto _it = from; _it != end(); _it++)
                    _it->~T();
    
                _size -= static_cast<usize>(to - from);

                return end();
            }
            else
            {
                usize _old_size = _size;
                _size -= static_cast<usize>(to - from);

                usize _current_pos = static_cast<usize>(from - begin());
                usize _last_pos = static_cast<usize>(to - begin());

                for (usize _index = 0; _index < _old_size - _last_pos; _index++)
                {
                    _data[_current_pos + _index] = 
                        move(_data[_last_pos + _index]);
                }

                return begin() + _current_pos;
            }
        }

        constexpr auto at(usize index)
            -> result<reference<T>, bad_access>
        {
            if (index >= _size) return bad_access{};

            return {_data[index]};
        }

        constexpr auto at(usize index) const
            -> result<reference<const T>, bad_access>
        {
            if (index >= _size) return bad_access{};

            return {_data[index]};
        }

        constexpr auto& at_unchecked(usize index)
        {
            return _data[index];
        }

        constexpr const auto& at_unchecked(usize index) const
        {
            return _data[index];
        }

        constexpr void clear()
        {
            for (usize _index = _size; _index > 0; --_index)
                at_unchecked(_index - 1).~T();
                
            _size = 0;
        }

        constexpr void reserve(usize new_cap)
        {
            if (new_cap > _capacity)
            {
                // To handle _capacity = 0 case
                usize _new_capacity = _capacity ? _capacity : 1;

                while (_new_capacity < new_cap)
                    _new_capacity += (_new_capacity + 1) / 2;

                T* _new_buf = _alloc
                    .template allocate<T>(_new_capacity)
                    .unwrap();

                for (usize _index = 0; _index < _size; ++_index)
                {
                    auto& _value = at_unchecked(_index);
                    construct_at(&_new_buf[_index], move(_value));
                    _value.~T();
                }

                _alloc
                .deallocate(_data, _capacity)
                .unwrap();
                
                _data = _new_buf;
                
                _capacity = _new_capacity;
            }
        }

        constexpr void shrink_to_fit()
        {
            if (_size == 0)
            {
                T* _old_buf = exchange(_data, nullptr);
                
                _alloc
                .deallocate(_old_buf, _capacity)
                .unwrap();
                
                _capacity = 0;
            }
            else if (_size < _capacity)
            {
                T* _new_buf = _alloc
                    .template allocate<T>(_size)
                    .unwrap();
                
                move<T>(_data, _data + _size, _new_buf);
                
                _alloc
                .deallocate(_data, _capacity)
                .unwrap();
                
                _capacity = _size;
                _data = _new_buf;
            }
        }

        constexpr void resize(usize new_size)
        {
            if (new_size > _capacity)
            {
                // To handle _capacity = 0 case
                usize _new_capacity = _capacity ? _capacity : 1;
                
                while (_new_capacity < new_size)
                    _new_capacity += (_new_capacity + 1) / 2;

                T* _new_buf = _alloc
                    .template allocate<T>(_new_capacity)
                    .unwrap();
                
                usize _index = 0;

                for (; _index < _size; ++_index)
                {
                    auto& _value = at_unchecked(_index);
                    construct_at(&_new_buf[_index], move(_value));
                    _value.~T();
                }
                for (; _index < new_size; ++_index)
                {
                    construct_at(&_new_buf[_index]);
                }

                _alloc.deallocate(_data, _capacity).unwrap();
                _data = _new_buf;
                _capacity = _new_capacity;
                _size = new_size;
            }
            else if (new_size > _size)
            {
                for (usize _index = _size; _index < new_size; ++_index)
                {
                    construct_at(&_data[_index]);
                }
                
                _size = new_size;
            }
            else if (new_size < _size)
            {
                for (usize _index = _size; _index > new_size; --_index)
                    at_unchecked(_index - 1).~T();
                
                _size = new_size;
            }
        }

        constexpr void push_back(const T& val)
        {
            emplace_back(val);
        }

        constexpr void push_back(T&& val)
        {
            emplace_back(move(val));
        }

        template <typename... Args>
        constexpr void emplace_back(Args&&... args)
        {
            reserve(_size + 1);
            construct_at(&_data[_size], forward<Args>(args)...);
            ++_size;
        }

        constexpr void pop_back() noexcept
        {
            if(_size > 0)
            {
                at_unchecked(_size - 1).~T();
                _size--;
            }
        }

        constexpr auto to_span()
        {
            return span<iterator>{*this};
        }

        constexpr auto to_span() const
        {
            return span<const_iterator>{*this};
        }

        constexpr usize size() const
        {
            return _size;
        }

        constexpr usize capacity() const
        {
            return _capacity;
        }

        constexpr iterator data()
        {
            return _data;
        }

        constexpr iterator begin()
        {
            return data();
        }

        constexpr iterator end()
        {
            return begin() + size();
        }

        constexpr const_iterator begin() const
        {
            return cbegin();
        }

        constexpr const_iterator end() const
        {
            return cend();
        }

        constexpr const_iterator cbegin() const
        {
            return _data;
        }

        constexpr const_iterator cend() const
        {
            return cbegin() + size();
        }

        constexpr iterator rbegin()
        {
            return end() - 1;
        }

        constexpr iterator rend()
        {
            return begin() - 1;
        }

        constexpr const_iterator rbegin() const
        {
            return crbegin();
        }

        constexpr const_iterator rend() const
        {
            return crend();
        }

        constexpr const_iterator crbegin() const
        {
            return cend() - 1;
        }

        constexpr const_iterator crend() const
        {
            return cbegin() - 1;
        }
    };

    template < typename T, usize N >
    class static_vector
    {
    private:
        stack_array<T, N> _data;
        static constexpr usize _capacity = N;

        struct bad_access
        {
            constexpr const char* pretty_error() const
            {
                return "Tried to access an element out of bounds";
            }
        };

    protected:
        usize _size = 0;

    public:
        using value_type = T;
        using iterator = T*;
        using const_iterator = const T*;

        constexpr ~static_vector()
        {
            for (usize _index = _size; _index > 0; --_index)
                at_unchecked(_index - 1).~T();
        }

        constexpr static_vector() = default;

        constexpr static_vector(usize size)
        {
            resize(size);
        }

        constexpr static_vector(const static_vector& other)
            : _size{other._size}
        {
            for (usize _index = 0; _index < _size; ++_index)
                _data[_index] = other[_index];
        }

        constexpr static_vector(static_vector&& other)
        {
            swap(_data, other._data);
            swap(_size, other._size);
        }

        constexpr static_vector& operator=(const static_vector& rhs)
        {
            usize _index;
            usize _min_size = _size < rhs._size ? _size : rhs._size;
            
            for (_index = 0; _index < _min_size; ++_index)
                _data[_index] = rhs[_index];
            
            if (_size > rhs._size)
            {
                for (_index = _size; _index > rhs._size; --_index)
                    at_unchecked(_index - 1).~T();
            }
            else if (rhs._size > _size)
            {
                for (; _index < rhs._size; ++_index)
                    _data[_index] = rhs[_index];
            }

            _size = rhs._size;

            return *this;
        }

        constexpr static_vector& operator=(static_vector&& rhs)
        {
            clear();
            swap(_data, rhs._data);
            _size = exchange(rhs._size, 0u);
    
            return *this;
        }

        constexpr auto& operator[](usize index)
        {
            return at_unchecked(index);
        }

        constexpr auto& operator[](usize index) const
        {
            return at_unchecked(index);
        }

        constexpr auto& front()
        {
            return *begin();
        }

        constexpr auto& front() const
        {
            return *begin();
        }

        constexpr auto& back() noexcept
        {
            return *(begin() + size() - 1);
        }

        constexpr auto& back() const
        {
            return *(begin() + size() - 1);
        }

        constexpr auto erase(const_iterator pos)
            -> result<iterator, bad_access>
        {
            return erase_for(pos, pos + 1);
        }

        constexpr auto erase_for(const_iterator from, const_iterator to)
            -> result<iterator, bad_access>
        {
            if (from < begin() || from > end() || to < begin() || to > end() || from > to)
                return bad_access{};

            if (to == end())
            {
                for (; from != end(); from++)
                    from->~T();
    
                return end();
            }
            else
            {
                usize _old_size = _size;
                _size -= static_cast<usize>(to - from);

                usize _current_pos = static_cast<usize>(from - begin());
                usize _last_pos = static_cast<usize>(to - begin());

                for (usize _index = 0; _index < _old_size - _last_pos; _index++)
                {
                    _data[_current_pos + _index] = 
                        move(_data[_last_pos + _index]);
                }

                return begin() + _current_pos;
            }
        }

        constexpr auto at(usize index)
            -> result<reference<T>, bad_access>
        {
            if (index >= _size)
                return bad_access{};

            return {_data[index]};
        }

        constexpr auto at(usize index) const
            -> result<reference<const T>, bad_access>
        {
            if (index >= _size)
                return bad_access{};

            return {_data[index]};
        }

        constexpr auto& at_unchecked(usize index)
        {
            return _data[index];
        }

        constexpr const auto& at_unchecked(usize index) const
        {
            return _data[index];
        }

        constexpr void clear() noexcept
        {  
            _size = 0;
        }

        constexpr void resize(usize new_size)
        {
            if (new_size > _size)
            {
                if (new_size <= _capacity)
                {
                    for (usize _index = _size; _index < new_size; ++_index)
                        _data[_index] = T{};

                    _size = new_size;
                }
                else
                {
                    panic(
                        "Error at static_vector::resize(): "
                        "new size is greater than capacity.\n"
                    );
                }
            }
            else if (new_size < _size)
            {
                for (usize _index = _size; _index > new_size; --_index)
                    at_unchecked(_index - 1).~T();
                
                _size = new_size;
            }
        }

        constexpr void push_back(const T& val)
        {
            emplace_back(val);
        }

        constexpr void push_back(T&& val)
        {
            emplace_back(move(val));
        }

        template <typename... Args>
        constexpr void emplace_back(Args&&... args)
        {
            if (_size + 1 <= _capacity)
            {
                _data[_size] = T{forward<Args>(args)...};
                ++_size;
            }
            else
            {
                panic(
                    "Error at static_vector:"
                    ":emplace_back(): Vector is full\n"
                );
            }
        }

        constexpr void pop_back() noexcept
        {
            if(_size > 0)
            {
                at_unchecked(_size - 1).~T();
                _size--;
            }
        }

        constexpr auto to_span()
        {
            return span<iterator>{*this};
        }

        constexpr auto to_span() const
        {
            return span<const_iterator>{*this};
        }

        constexpr usize size() const
        {
            return _size;
        }

        constexpr usize capacity() const
        {
            return _capacity;
        }

        constexpr iterator data()
        {
            return &_data[0];
        }

        constexpr iterator begin()
        {
            return data();
        }

        constexpr iterator end()
        {
            return begin() + size();
        }

        constexpr const_iterator begin() const
        {
            return cbegin();
        }

        constexpr const_iterator end() const
        {
            return cend();
        }

        constexpr const_iterator cbegin() const
        {
            return &_data[0];
        }

        constexpr const_iterator cend() const
        {
            return cbegin() + size();
        }

        constexpr iterator rbegin()
        {
            return end() - 1;
        }

        constexpr iterator rend()
        {
            return begin() - 1;
        }

        constexpr const_iterator rbegin() const
        {
            return crbegin();
        }

        constexpr const_iterator rend() const
        {
            return crend();
        }

        constexpr const_iterator crbegin() const
        {
            return cend() - 1;
        }

        constexpr const_iterator crend() const
        {
            return cbegin() - 1;
        }
    };

    template < typename T, usize N > vector(const T (&)[N]) -> vector<T>;
    template < typename T, usize N > vector(T (&&)[N]) -> vector<T>;
    template < typename T > using buffered_vector = vector< T, buffered_allocator >;

    template < typename L, Convertible<L>... U >
    constexpr vector<L> make_vector(L&& first, U&&... rest)
    {
        constexpr usize size = 1 + sizeof...(U);
        vector<L> vec;
        vec.reserve(size);
        
        [&vec]<usize... _index>(hsd::index_sequence<_index...>, auto&... args)
        {
            (vec.emplace_back(forward<L>(args)), ...);
        }(hsd::make_index_sequence<size>{}, first, rest...);
        
        return vec;
    }
}
