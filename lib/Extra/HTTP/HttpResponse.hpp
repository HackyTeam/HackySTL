#pragma once

#include "HttpHeader.hpp"

namespace hsd::http
{
    class response
    {
    private:
        protocol_version _ver = protocol_version::ver_1_1;
        response_status _status{};
        string_view _status_message_spec = "Unknown";
        string _status_message{};
        header _header{};

    public:
        inline auto& set_status(response_status status)
        {
            _status = status;
            return *this;
        }

        inline auto get_status() const
        {
            return _status;
        }

        inline const auto& get_header() const
        {
            return _header;
        }
        
        inline auto& set_header_field(
            const string_view& key, const string_view& value)
        {
            _header.set_field(key, value);
            return *this;
        }

        inline auto get_version() const
        {
            return _ver;
        }

        inline auto& set_version(protocol_version version)
        {
            _ver = version;
            return *this;
        }

        inline auto& set_message(const string_view& msg)
        {
            _status_message = msg;
            return *this;
        }

        inline auto get_message() const
        {
            if (_status_message.length() != 0)
            {
                return static_cast<string_view>(_status_message);
            }
            
            return _status_message_spec;
        }

        inline auto parse(const string_view& resp)
            -> result<usize, runtime_error>
        {
            using namespace string_view_literals;
            usize _pos = resp.find(' ');
            usize _begin_pos = 0;

            if (_pos == resp.npos)
            {
                return runtime_error{
                    "Method separator not found"
                };
            }
            else
            {
                string_view _protocol = {
                    resp.data(), _pos
                };

                check_protocol(_protocol, _ver);

                ++_pos;
            }

            if (resp[_pos] < '0' && resp[_pos] > '9')
            {
                return runtime_error{
                    "HTTP responses are required to have a status code"
                };
            }
            else
            {
                _status = static_cast<response_status>(
                    cstring::parse<u16>(&resp[_pos])
                );

                auto _res = set_status_message_spec(
                    _status, _status_message_spec
                );

                if (_res.is_ok() == false)
                {
                    return _res.unwrap_err();
                }

                u16 _status_num = static_cast<u16>(_status);

                if (_status_num < 10)
                {
                    ++_pos;
                }
                else if (_status_num < 100)
                {
                    _pos += 2;
                }
                else
                {
                    _pos += 3;
                }

                if (resp[_pos] == ' ') ++_pos;

                _begin_pos = _pos;
                _pos = resp.find('\n', _pos);
            }

            if (_pos == resp.npos)
            {
                return runtime_error{
                    "Missing newline terminator"
                };
            }
            else
            {
                if (resp[_pos - 1] == '\r')
                {
                    --_pos;
                }
                else
                {
                    return runtime_error{
                        "HTTP requires to end the first line with a CRLF"
                    };
                }

                _status_message = string{&resp[_begin_pos], _pos - _begin_pos};
                _pos += 2;
            }

            return _pos + _header.parse({&resp[_pos], resp.size() - _pos}).unwrap();
        }
    };
} // namespace hsd::http