#pragma once

#include "../Containers/List.hpp"
#include "../Containers/String.hpp"
#include "../Wrappers/UniquePtr.hpp"
#include "../Containers/UnorderedMap.hpp"
#include "../Wrappers/Variant.hpp"
#include "../Serialize/Io.hpp"

namespace hsd
{
    enum class JsonToken
    {
        Null, True, False, Number, String, BArray, 
        EArray, BObject, EObject, Comma, Colon,

        Eof = -1,
        Error = -2,
        Empty = -3 // For internal purposes
    };

    class JsonError : public runtime_error
    {
        hsd::string _str;

    public:
        usize position;

        constexpr JsonError(const char* msg, usize pos)
            : runtime_error{msg}, position{pos} {}

        constexpr const char* pretty_error()
        {
            if (_str.length() != 0)
                return _str.c_str();

            return (_str = _err + (" " + to_string(position))).c_str();
        }
    };

    /// @brief JSON stream parser
    class JsonStream
    {
        using vstr = basic_string_view<char>;
        using str = basic_string<char>;
        using num = variant<i64, f128>;

        list<JsonToken> _tokens;
        list<str> _qtok_string;
        list<num> _qtok_number;
        JsonToken _current_token = JsonToken::Empty;
        const char* _token_kw = nullptr;
        usize _token_position = 0;
        str _token_str;

        usize _pos = 0;

    public:
        constexpr auto& get_tokens() { return _tokens; }

        constexpr str pop_string()
        {
            str _s = move(_qtok_string.front());
            _qtok_string.pop_front();
            return _s;
        }

        constexpr num pop_number()
        {
            num _n = move(_qtok_number.front());
            _qtok_number.pop_front();
            return _n;
        }

        // If an error occurs, the rest of buffer can be passed in
        constexpr option_err<JsonError> lex(vstr frag)
        {
            constexpr const char* const s_keywords[] = {"null", "true", "false"};
            for (bool _floating = false; char _ch : frag)
            {
                ++_pos;
                
                _reparse:
                if (_current_token == JsonToken::Empty)
                {
                    if (basic_cstring<char>::iswhitespace(_ch))
                        continue;
                    
                    switch (_ch)
                    {
                        #define CASE_CH(ch, tok)    \
                        case ch:\
                        {                           \
                            _tokens.push_back(tok);  \
                            break;                  \
                        }

                        CASE_CH('[', JsonToken::BArray)
                        CASE_CH(']', JsonToken::EArray)
                        CASE_CH('{', JsonToken::BObject)
                        CASE_CH('}', JsonToken::EObject)
                        CASE_CH(',', JsonToken::Comma)
                        CASE_CH(':', JsonToken::Colon)
                        #undef CASE_CH

                        case 'n':
                        {
                            _current_token = JsonToken::Null;
                            _token_kw = s_keywords[0];
                            ++_token_position;
                            break;
                        }
                        case 't':
                        {
                            _current_token = JsonToken::True;
                            _token_kw = s_keywords[1];
                            ++_token_position;
                            break;
                        }
                        case 'f':
                        {
                            _current_token = JsonToken::False;
                            _token_kw = s_keywords[2];
                            ++_token_position;
                            break;
                        }
                        case '"':
                        {
                            _current_token = JsonToken::String;
                            break;
                        }
                        default:
                        {
                            // Only ASCII numbers supported
                            if (_ch == '-' || _ch == '+' || (_ch >= '0' && _ch <= '9'))
                            {
                                _current_token = JsonToken::Number;
                                
                                if (_ch != '+' && _ch != '-')
                                    _token_str.push_back('+'); // Explicit sign for parse<i32>
                                
                                _token_str.push_back(_ch);
                                ++_token_position;
                            }
                            else
                            {
                                _tokens.push_back(JsonToken::Error);
                                return JsonError{"Syntax error: unexpected character", _pos};
                            }
                        }
                    }
                }
                else
                {
                    if (_token_kw)
                    {
                        if (_ch != _token_kw[_token_position])
                        {
                            // Error and recover
                            _tokens.push_back(JsonToken::Error);
                            _current_token = JsonToken::Empty;
                            _token_position = 0;
                            _token_kw = nullptr;
                            return JsonError{"Syntax error: unexpected character", _pos};
                        }
                        
                        ++_token_position;
                        
                        if (_token_kw[_token_position] == 0)
                        {
                            _tokens.push_back(_current_token);
                            _current_token = JsonToken::Empty;
                            _token_position = 0;
                            _token_kw = nullptr;
                        }
                    }
                    else if (_current_token == JsonToken::String)
                    {
                        // handle escape sequences
                        if (_ch == '"')
                        {
                            _tokens.push_back(_current_token);
                            _qtok_string.push_back(move(_token_str));
                            _current_token = JsonToken::Empty;
                            _token_position = 0;
                            _token_str.clear();
                        }
                        else
                        {
                            ++_token_position;
                            _token_str.push_back(_ch);
                        }
                    }
                    else if (_current_token == JsonToken::Number)
                    {
                        if (_ch >= '0' and _ch <= '9')
                        {
                            _token_position++;
                            _token_str.push_back(_ch);
                        }
                        else if (_ch == '.')
                        {
                            if (_floating == true)
                            {
                                return JsonError{"Syntax error: unexpected character", _pos};
                            }

                            _floating = true;
                            _token_position++;
                            _token_str.push_back(_ch);
                        }
                        else if (_ch == 'e' || _ch == 'E')
                        {
                            return JsonError{"Work in progress: exponential notation", _pos};
                        }
                        else
                        {
                            _tokens.push_back(_current_token);
                            
                            if (_floating == true)
                            {
                                if (_token_str.back() == '.')
                                    _token_str.push_back('0');

                                _floating = false;
                                _qtok_number.emplace_back(
                                    cstring::template parse<f128>(_token_str.c_str())
                                );
                            }
                            else
                            {
                                _qtok_number.emplace_back(
                                    cstring::template parse<i64>(_token_str.c_str())
                                );
                            }
                            
                            _current_token = JsonToken::Empty;
                            _token_position = 0;
                            _token_str.clear();

                            goto _reparse;
                        }
                    }
                }
            }

            return {};
        }

        // If an error occurs, the rest of buffer can be passed in
        inline option_err<JsonError> lex_file(string_view filename)
        {
            //static const char* const s_keywords[] = {"null", "true", "false"};
            auto _res  = io::load_file(filename.data());
            
            if (_res.is_ok() == false)
            {
                return JsonError{"Couldn't open file", static_cast<usize>(-1)};
            }
            
            auto _stream = move(_res.unwrap());
            _stream.get_stream().reserve(1024);

            while (_stream.is_eof() == false)
            {
                string_view _chunk = _stream.read_chunk().unwrap().get().get_stream();
                
                auto _res = lex(_chunk);
                
                if (_res.is_ok() == false)
                {
                    return _res.unwrap_err();
                }
            }

            return {};
        }

        // End the stream of tokens
        constexpr option_err<JsonError> push_eot()
        {
            ++_pos;
            
            if (_current_token == JsonToken::Number)
            {
                _tokens.push_back(_current_token);
                
                if (_token_str.find('.') != str::npos)
                {
                    _qtok_number.emplace_back(
                        cstring::template parse<f128>(_token_str.c_str())
                    );
                }
                else
                {
                    _qtok_number.emplace_back(
                        cstring::template parse<i64>(_token_str.c_str())
                    );
                }
                
                _current_token = JsonToken::Empty;
                _token_position = 0;
                _token_str.clear();
            }
            if (_current_token != JsonToken::Empty)
            {
                _tokens.push_back(JsonToken::Error);
                return JsonError{"Syntax error: unexpected end of transmission", _pos};
            }
            
            _tokens.push_back(JsonToken::Eof);
            return {};
        }
    };

    enum class JsonValueType
    {
        Null, True, False, Number,
        String, Object, Array
    };

    class JsonValue
    {
    public:
        constexpr virtual ~JsonValue() = default;
        constexpr virtual JsonValueType type() const noexcept = 0;
        constexpr virtual bool is_complete() const noexcept { return true; };

        template <typename Class>
        constexpr Class& as() { return static_cast<Class&>(*this); }

        template <typename Class>
        constexpr result<reference<Class>, runtime_error> try_as(JsonValueType t)
        {
            if (type() == t)
            {
                return {static_cast<Class&>(*this)};
            }
            else
            {
                return runtime_error{"Cast to wrong type"};
            }
        }

        constexpr result<string_view, runtime_error> as_str();
        constexpr result<bool, runtime_error> as_bool();

        template <NumericType Number>
        constexpr result<Number, runtime_error> as_num();

        constexpr auto& as_object();
        constexpr auto& as_array();

        constexpr result<reference<JsonValue>, runtime_error> access(const string_view& key);
        constexpr result<reference<JsonValue>, runtime_error> access(usize index);

        constexpr JsonValue& operator[](const string_view& key);
        constexpr JsonValue& operator[](usize index);
    };

    // Streaming pending value, todo
    class JsonPendingValue : public JsonValue
    {
        constexpr bool is_complete() const noexcept override { return false; }
    };

    class JsonPrimitive : public JsonValue
    {
        JsonValueType _t; // only Null, True & False allowed

        constexpr JsonPrimitive(JsonValueType t) : _t(t) {}

    public:
        constexpr JsonValueType type() const noexcept override
        {
            return _t;
        }

        static constexpr JsonPrimitive mk_null() { return {JsonValueType::Null}; }
        static constexpr JsonPrimitive mk_true() { return {JsonValueType::True}; }
        static constexpr JsonPrimitive mk_false() { return {JsonValueType::False}; }
    };

    class JsonNumber : public JsonValue
    {
        variant<i64, f128> _value;

    public:
        constexpr explicit JsonNumber(variant<i64, f128>&& v)
            : _value{move(v)}
        {}

        constexpr JsonValueType type() const noexcept override
        {
            return JsonValueType::Number;
        }

        constexpr auto& value() const { return _value; }
    };

    class JsonString : public JsonValue
    {
        string _value;

    public:
        explicit JsonString(string&& v)
            : _value{move(v)}
        {}

        JsonValueType type() const noexcept override
        {
            return JsonValueType::String;
        }

        string_view value() const
        {
            return static_cast<string_view>(_value);
        }
    };

    class JsonArray : public JsonValue
    {
        vector<unique_ptr<JsonValue>> _values;

    public:
        explicit JsonArray(vector<unique_ptr<JsonValue>>&& v)
            : _values{move(v)}
        {}

        JsonValueType type() const noexcept override
        {
            return JsonValueType::Array;
        }

        auto& values()
        {
            return _values;
        }
    };

    class JsonObject : public JsonValue
    {
        unordered_map<string, unique_ptr<JsonValue>> _values;

    public:
        explicit JsonObject(unordered_map<string, unique_ptr<JsonValue>>&& v)
            : _values{move(v)}
        {}

        JsonValueType type() const noexcept override
        {
            return JsonValueType::Object;
        }

        auto& values()
        {
            return _values;
        }
    };

    constexpr auto JsonValue::as_str()
        -> result<string_view, runtime_error>
    {
        auto _res = try_as<JsonString>(JsonValueType::String);

        if (_res)
        {
            return _res.unwrap().get().value();
        }
        else
        {
            return _res.unwrap_err();
        }
    }

    constexpr result<bool, runtime_error> JsonValue::as_bool()
    {
        if (type() == JsonValueType::True)
        {
            return true;
        }
        else if (type() == JsonValueType::False)
        {
            return false;
        }

        // throw
        return runtime_error{"Cast to wrong type"};
    }

    template <NumericType Number>
    constexpr result<Number, runtime_error> JsonValue::as_num()
    {
        auto _res = try_as<JsonNumber>(JsonValueType::Number);

        if (_res)
        {
            if constexpr (IsIntegral<Number>)
            {
                auto _val = _res.unwrap().get().value().template get<i64>();

                if (!_val)
                {
                    return runtime_error{"Wrong numeric type selected"};
                }
                
                auto _num = _val.unwrap();

                if (_num < limits<Number>::min or _num > limits<Number>::max)
                {
                    return runtime_error{"Number out of range"};
                }

                return static_cast<Number>(_num);
            }
            else if constexpr (IsFloat<Number>)
            {
                auto _val = _res.unwrap().get().value().template get<f128>();

                if (!_val)
                {
                    return runtime_error{"Wrong numeric type selected"};
                }

                auto _num = _val.unwrap();

                if (_num < limits<Number>::min or _num > limits<Number>::max)
                {
                    return runtime_error{"Number out of range"};
                }

                return static_cast<Number>(_num);
            }
            else
            {
                return runtime_error{"Wrong numeric type selected"};
            }
        }
        else
        {
            return _res.unwrap_err();
        }
    }

    constexpr auto& JsonValue::as_array()
    {
        return try_as<JsonArray>(JsonValueType::Array).unwrap().get().values();
    }

    constexpr auto& JsonValue::as_object()
    {
        return try_as<JsonObject>(JsonValueType::Object).unwrap().get().values();
    }

    constexpr auto JsonValue::access(const string_view& key)
        -> result<reference<JsonValue>, runtime_error>
    {
        auto _res = try_as<JsonObject>(JsonValueType::Object);

        if (_res)
        {
            return {*_res.unwrap().get().values().at(key).unwrap().get()};
        }

        return _res.unwrap_err();
    }

    constexpr auto JsonValue::access(usize index)
        -> result<reference<JsonValue>, runtime_error>
    {
        auto _res = try_as<JsonArray>(JsonValueType::Array);

        if (_res)
        {
            return {*_res.unwrap().get().values().at(index).unwrap().get()};
        }

        return _res.unwrap_err();
    }

    constexpr JsonValue& JsonValue::operator[](const string_view& key)
    {
        return access(key).unwrap();
    }

    constexpr JsonValue& JsonValue::operator[](usize index)
    {
        return access(index).unwrap();
    }

    class JsonTokenIterator
    {
        JsonStream& _stream;

    public:
        constexpr JsonTokenIterator(JsonStream& stream)
            : _stream(stream) {}

        constexpr JsonToken next()
        {
            JsonToken tk = _stream.get_tokens().front();
            _stream.get_tokens().pop_front();
            return tk;
        }

        constexpr JsonToken peek()
        {
            return _stream.get_tokens().front();
        }

        constexpr void skip()
        {
            _stream.get_tokens().pop_front();
        }

        constexpr bool empty() const
        {
            return _stream.get_tokens().empty();
        }

        constexpr auto next_number()
        {
            return _stream.pop_number();
        }

        constexpr auto next_string()
        {
            return _stream.pop_string();
        }
    };

    class JsonParser
    {
        JsonTokenIterator _stream;

    public:
        JsonParser(JsonStream& s) : _stream(s) {}

        result<unique_ptr<JsonValue>, JsonError> parse_next()
        {
            if (_stream.empty())
                //return JsonPendingValue::make();
                return JsonError{"Work in progress: pending values not supported", 0};

            JsonToken _tok = _stream.next();
            switch (_tok)
            {
                case JsonToken::Eof:
                    return JsonError{"Unexpected EOF", 0};
                case JsonToken::Null:
                    return {make_unique<JsonPrimitive>(JsonPrimitive::mk_null())};
                case JsonToken::True:
                    return {make_unique<JsonPrimitive>(JsonPrimitive::mk_true())};
                case JsonToken::False:
                    return {make_unique<JsonPrimitive>(JsonPrimitive::mk_false())};
                case JsonToken::Number:
                    return {make_unique<JsonNumber>(_stream.next_number())};
                case JsonToken::String:
                    return {make_unique<JsonString>(_stream.next_string())};
                case JsonToken::BArray:
                {
                    vector<unique_ptr<JsonValue>> _array;
                    
                    if (_stream.empty())
                    {
                        _partial_arr:
                        return JsonError{"Work in progress: partial arrays", 0};
                    }

                    if (_stream.peek() != JsonToken::EArray)
                    {
                        while (true)
                        {
                            auto _res = parse_next();
                            
                            if (!_res)
                            {
                                return _res;
                            }
                            
                            _array.push_back(_res.expect());
                            
                            if (_stream.empty())
                            {
                                goto _partial_arr;
                            }
                            if (_stream.peek() == JsonToken::EArray)
                            {
                                _stream.skip();
                                break;
                            }
                            if (_stream.next() != JsonToken::Comma)
                                return JsonError{"Syntax error: expected a comma while parsing array", 0};
                        }
                    }
                    
                    return {make_unique<JsonArray>(move(_array))};
                }
                case JsonToken::BObject:
                {
                    unordered_map<string, unique_ptr<JsonValue>> _map;
                    
                    if (_stream.empty())
                    {
                        _partial_obj:
                        return JsonError{"Work in progress: partial objects", 0};
                    }
                    if (_stream.peek() != JsonToken::EObject)
                    {
                        while (true)
                        {
                            if (_stream.next() != JsonToken::String)
                            {
                                return JsonError{"Syntax error: expected string name", 0};
                            }
                           
                            auto _name = _stream.next_string();
                            
                            if (_stream.empty())
                            {
                                goto _partial_obj;
                            }
                            if (_stream.next() != JsonToken::Colon)
                            {
                                return JsonError{"Syntax error: expected a colon", 0};
                            }
                            
                            auto _res = parse_next();

                            if (!_res)
                            {
                                return _res;
                            }

                            _map.emplace(move(_name), _res.expect());
                            
                            if (_stream.peek() == JsonToken::EObject)
                            {
                                _stream.skip();
                                break;
                            }
                            if (_stream.next() != JsonToken::Comma)
                            {
                                return JsonError{"Syntax error: expected a comma while parsing object", 0};
                            }
                            if (_stream.empty())
                            {
                                goto _partial_obj;
                            }
                        }
                    }
                    return {make_unique<JsonObject>(move(_map))};
                }
                default:
                    return JsonError{"Syntax error: unexpected token", static_cast<usize>(_tok)};
            }
        }
    };
}
