#pragma once

#include "../Containers/String.hpp"

#if defined(HSD_PLATFORM_WINDOWS)
#define _WINSOCKAPI_
#include <windows.h>
#else
#include <unistd.h>
#endif

#if defined(HSD_PLATFORM_WINDOWS)
#define PATH_SEPARATOR '\\'
#define PATH_SEPARATOR_STR "\\"
#define ROOT_PATH "C:\\"
#define ROOT_PATH_LEN 3u
#define PATH_SEPARATOR_STR_LEN 1u
#else
#define PATH_SEPARATOR '/'
#define PATH_SEPARATOR_STR "/"
#define ROOT_PATH "/"
#define ROOT_PATH_LEN 1u
#define PATH_SEPARATOR_STR_LEN 1u
#endif

///
/// @brief Used in identifying the classes
/// and functions in the standard library. 
///
namespace hsd
{
    ///
    /// @brief Namespace for file system related functions.
    ///
    namespace filesystem
    {
        ///
        /// @brief Namespace for file system implementation details.
        ///
        namespace fs_detail
        {
            ///
            /// @brief Structure for holding path information.
            ///
            class location
            {
            public:
                ///
                /// @brief Holds the relative path of a file or directory.
                ///
                string _relative;

                ///
                /// @brief Holds the absolute path of a file or directory.
                ///
                string _absolute;

            private:
                inline void _resolve_location(const char* loc)
                {
                    char _buffer[2048]{};

                    #if defined(HSD_PLATFORM_WINDOWS)
                    GetFullPathNameA(loc, 2048, _buffer, nullptr);
                    #else
                    realpath(loc, _buffer);
                    #endif

                    _absolute = string{_buffer};

                    #if defined(HSD_PLATFORM_WINDOWS)
                    GetCurrentDirectory(2048, _buffer);
                    #else
                    getcwd(_buffer, 2048);
                    #endif

                    const char* _absolute_ptr = _absolute.c_str();
                    const char* _current_ptr = _buffer;

                    const auto _check_paths = [](const char* absolute_p, const char* current_p)
                    {
                        return *absolute_p == *current_p && *absolute_p != '\0' && *current_p != '\0';
                    };

                    for (; _check_paths(_absolute_ptr, _current_ptr); ++_absolute_ptr, ++_current_ptr)
                    {}

                    if (*_current_ptr == '\0')
                    {
                        _relative.emplace_back('.');
                        _relative += (*_absolute_ptr != '\0') ? _absolute_ptr : PATH_SEPARATOR_STR;
                        return;
                    }

                    _relative = "." PATH_SEPARATOR_STR;
                    
                    for (; *_current_ptr != '\0'; ++_current_ptr)
                    {
                        if (*_current_ptr == PATH_SEPARATOR && _current_ptr[1] != '\0')
                        {
                            _relative += ".." PATH_SEPARATOR_STR;
                        }
                    }

                    _relative += _absolute_ptr;
                }

            public:
                ///
                /// @brief Constructs a new empty location object
                ///
                inline location() = default;

                ///
                /// @brief Constructs a new location object.
                /// 
                /// @param loc - The path of a file or directory.
                ///
                inline location(const char* loc)
                {
                    _resolve_location(loc);
                }

                ///
                /// @brief Constructs a new location object.
                /// 
                /// @param loc - The path of a file or directory.
                ///
                inline location(const string& loc)
                {
                    _resolve_location(loc.c_str());
                }

                ///
                /// @brief Constructs by copy a new location object.
                /// 
                /// @param other - The location object to copy.
                ///
                inline location(const location& other)
                    : _relative{other._relative},
                      _absolute{other._absolute}
                {}

                ///
                /// @brief Constructs by move a new location object.
                /// 
                /// @param other - The location object to move.
                ///
                inline location(location&& other)
                    : _relative{move(other._relative)},
                      _absolute{move(other._absolute)}
                {}

                ///
                /// @brief Assigns by copy a new location object.
                /// 
                /// @param rhs - The location object to copy.
                /// @return location& - A reference to the assigned location object.
                ///
                inline location& operator=(const location& rhs)
                {
                    _relative = rhs._relative;
                    _absolute = rhs._absolute;
                    return *this;
                }

                ///
                /// @brief Assigns by move a new location object.dd
                /// 
                /// @param rhs - The location object to move.
                /// @return location& - A reference to the assigned location object.
                ///
                inline location& operator=(location&& rhs)
                {
                    _relative = move(rhs._relative);
                    _absolute = move(rhs._absolute);
                    return *this;
                }
            };
        } // namespace fs_detail
    } // namespace filesystem
} // namespace hsd