#pragma once

#include "../Containers/UnorderedMap.hpp"
#include "../Wrappers/Functional.hpp"
#include "_SStreamDetail.hpp"
#include "../Containers/StringView.hpp"

namespace hsd
{
    class parser_stream
    {
    private:
        vector<string_view> _args_buf;

    public:
        // It's a non-movable and copyable type
        inline parser_stream(const parser_stream&) = delete;
        inline parser_stream(parser_stream&&) = delete;
        inline parser_stream& operator=(const parser_stream&) = delete;
        inline parser_stream& operator=(parser_stream&&) = delete;
        inline parser_stream() = default;
        inline ~parser_stream() = default;

        template <typename... Args>
        inline option_err<runtime_error> set_data(Args&... args)
        {
            if (sizeof...(Args) > _args_buf.size())
            {
                return runtime_error{"Input too small to parse"};
            }
            else
            {
                option_err<runtime_error> _error{};
                tuple<reference<Args>...> _tup = {reference<Args>{args}...};
                
                auto _set_func = [&_error](const auto& data, auto& arg)
                {
                    using sstream_detail::_parse_from;

                    if (_error.is_ok() == true)
                    {
                        _error = _parse_from(data, arg);
                    }
                };

                [&]<usize... Ints>(index_sequence<Ints...>)
                {
                    (_set_func(_args_buf[Ints], _tup.template get<Ints>()), ...);
                }(make_index_sequence<sizeof...(Args)>{});

                if (_error.is_ok() == false)
                {
                    return _error.unwrap_err();
                }
            }

            return {};
        }

        inline void emplace(const char* arg)
        {
            _args_buf.emplace_back(arg);
        }

        inline void clear()
        {
            _args_buf.clear();
        }
    };

    class argument_parser
    {
    private:
        using function_type = function<void(parser_stream&)>;
        using action_type = pair<function_type, usize>;
        unordered_map<string_view, action_type> _actions;
        vector<string_view> _informations;

    public:
        inline argument_parser(const string_view& info)
        {
            _informations.emplace_back(info);
        }
        
        inline argument_parser(const argument_parser&) = delete;
        inline argument_parser(argument_parser&&) = delete;
        inline argument_parser& operator=(const argument_parser&) = delete;
        inline argument_parser& operator=(argument_parser&&) = delete;
        inline ~argument_parser() = default;

        inline void add(
            const string_view& argument, usize num_args,
            function_type&& func, const string_view& help)
        {
            _actions.emplace(argument, func, num_args);
            _informations.emplace_back(help);
        }

        inline void print_info()
        {
            for (const auto& _info : _informations)
            {
                printf("%s\n", _info.data());
            }
        }

        inline void parse(i32 argc, const char** argv)
        {
            if (argc == 1)
            {
                return;
            }
            else if (cstring::compare(argv[1], "-h") == 0 || 
                cstring::compare(argv[1], "--help") == 0)
            {
                print_info();
            }
            
            //       0          1      2     3    4     5      6
            // launch_program clear command arg1 arg2 arg3 command2 
            // num_args = 3
            // index = 2

            i32 _index = 1, _incrementor;
            parser_stream _buf;

            while (_index < argc)
            {
                auto _action = _actions.at(argv[_index]);
                
                if (!_action.is_ok())
                {
                    print_info();
                }

                auto& [_function, _num_args] = _action
                    .unwrap().get();
                
                _incrementor = _num_args;
                _index += 1;
                
                for (; _incrementor != 0; _incrementor--)
                {
                    _buf.emplace(argv[_index + _num_args - _incrementor]);
                }

                _function(_buf).unwrap();
                _index += _num_args;
                _buf.clear();
            }
        }
    };
} // namespace hsd
