#pragma once

#include "_FormatGeneratorDetail.hpp"

namespace hsd
{
    template <typename CharT, usize N1, usize N2, usize N3>
    struct format_output
    {
        template <usize N>
        using lit_type = basic_string_literal<CharT, N>;

        lit_type<N1> format;
        lit_type<N2> style;
        lit_type<N3> reset;

        static constexpr usize none  = 0; 
        static constexpr usize hex   = 1;  // hexadecimal
        static constexpr usize exp   = 2;  // exponent
        static constexpr usize prec  = 16; // precision

        usize tag;
        usize prec_val;
    };

    template <format_literal lit, typename T>
    requires (!(lit.base.tag & lit.base.ovr))
    static constexpr auto format()
    {
        using char_type = typename decltype(lit)::char_type;
        using cstring_type = basic_cstring<char_type>;

        constexpr auto _fmt_style_attr = get_format_style_attr<char_type, lit.base.style_val>();
        constexpr auto _fmt_reset_attr = get_reset_attr<char_type, lit.base.style_val>();

        static_assert(_fmt_style_attr.size() - 1 == cstring_type::length(_fmt_style_attr.data));
        static_assert(_fmt_reset_attr.size() - 1 == cstring_type::length(_fmt_reset_attr.data));

        return format_output{
            .format = lit.format,
            .style = _fmt_style_attr,
            .reset = _fmt_reset_attr,
            .tag = lit.base.tag & ~lit.base.style,
            .prec_val = lit.base.prec_val
        };
    }

    template <format_literal lit>
    requires (!(lit.base.tag & lit.base.ovr))
    static constexpr auto format()
    {
        using char_type = typename decltype(lit)::char_type;
        using cstring_type = basic_cstring<char_type>;

        constexpr auto _fmt_style_attr = get_format_style_attr<char_type, lit.base.style_val>();
        constexpr auto _fmt_reset_attr = get_reset_attr<char_type, lit.base.style_val>();

        static_assert(_fmt_style_attr.size() - 1 == cstring_type::length(_fmt_style_attr.data));
        static_assert(_fmt_reset_attr.size() - 1 == cstring_type::length(_fmt_reset_attr.data));

        return format_output{
            .format = lit.format,
            .style = _fmt_style_attr,
            .reset = _fmt_reset_attr,
            .tag = lit.base.tag & ~lit.base.style,
            .prec_val = lit.base.prec_val
        };
    }
} // namespace hsd
