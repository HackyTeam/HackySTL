#pragma once

#include "SStream.hpp"
#include "../Containers/String.hpp"

#if defined(HSD_PLATFORM_WINDOWS)
#define _WINSOCKAPI_
#include <windows.h>
#else
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#endif

namespace hsd
{
    #if defined(HSD_PLATFORM_WINDOWS)
    enum class io_options : DWORD
    {
        none = 0,
        read = GENERIC_READ,
        write = GENERIC_WRITE,
        read_write = GENERIC_READ | GENERIC_WRITE,
        append = FILE_APPEND_DATA,
        rw_append = GENERIC_READ | FILE_APPEND_DATA,
        rw_create = GENERIC_READ | GENERIC_WRITE | CREATE_ALWAYS
    };

    namespace io_detail
    {
        static inline auto file_error_code()
        {
            return ::GetLastError();
        }

        static inline const char* file_error_msg()
        {
            static char _msg_buf[256]{};
            
            FormatMessageA(
                FORMAT_MESSAGE_FROM_SYSTEM | 
                FORMAT_MESSAGE_IGNORE_INSERTS,
                nullptr, file_error_code(), 
                MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), 
                _msg_buf, sizeof(_msg_buf), nullptr
            );

            return _msg_buf;
        }

        class file_handler
        {
        private:
            HANDLE _handle = nullptr;
            OVERLAPPED _overlapped = {};
            bool _is_overlapped = false;
            bool _is_external = false;
            io_options _mode = io_options::none;

        public:
            inline file_handler() = default;

            inline file_handler(const file_handler&) = delete;
            inline file_handler& operator=(const file_handler&) = delete;

            inline file_handler(file_handler&& other)
            {
                swap(other._handle, _handle);
                swap(other._overlapped, _overlapped);
                swap(other._is_overlapped, _is_overlapped);
                swap(other._is_external, _is_external);
                swap(other._mode, _mode);
            }

            inline file_handler& operator=(file_handler&& other)
            {
                swap(other._handle, _handle);
                swap(other._overlapped, _overlapped);
                swap(other._is_overlapped, _is_overlapped);
                swap(other._is_external, _is_external);
                swap(other._mode, _mode);

                return *this;
            }

            inline ~file_handler()
            {
                close();
            }

            inline void close()
            {
                if (_handle != nullptr)
                {
                    if (_is_overlapped == false)
                    {
                        if (CloseHandle(_handle) == 0)
                        {
                            panic(file_error_msg());
                        }
                    }
                    else if (_is_external == false)
                    {
                        if (CancelIoEx(_handle, &_overlapped) == 0)
                        {
                            panic(file_error_msg());
                        }
                    }

                    _handle = nullptr;
                }
            }

            inline bool is_open() const
            {
                return _handle != nullptr;
            }

            inline bool only_read() const
            {
                return _mode == io_options::read;
            }

            inline bool only_write() const
            {
                return _mode == io_options::write;
            }

            inline auto write(const void* data, u64 size)
            {
                DWORD _written = 0;

                if (WriteFile(_handle, data, size, &_written, &_overlapped) == 0)
                {
                    if (file_error_code() != ERROR_IO_PENDING)
                    {
                        return static_cast<DWORD>(-1);
                    }

                    if (GetOverlappedResult(_handle, &_overlapped, &_written, true) == 0)
                    {
                        return static_cast<DWORD>(-1);
                    }
                }

                return _written;
            }

            inline auto read(void* data, u64 size)
            {
                DWORD _read = 0;

                if (ReadFile(_handle, data, size, &_read, &_overlapped) == 0)
                {
                    if (file_error_code() != ERROR_IO_PENDING)
                    {
                        return static_cast<DWORD>(-1);
                    }

                    if (GetOverlappedResult(_handle, &_overlapped, &_read, true) == 0)
                    {
                        return static_cast<DWORD>(-1);
                    }
                }

                return _read;
            }

            
            inline auto read_line(char* data, usize size, const char* sep = "")
            {
                DWORD _sz = read(data, size);

                if (_sz == 0) return static_cast<DWORD>(0);

                const char* _ptr = nullptr;
                
                while (*sep != '\0' && _ptr == nullptr)
                {
                    _ptr = cstring::find_rev(data, *sep++, _sz - 1);
                }

                if (_ptr != nullptr)
                {
                    DWORD _off_sz = _ptr - (data + _sz) + 1;
                    ::SetFilePointer(_handle, _off_sz, nullptr, FILE_CURRENT);
                    return _sz + _off_sz;
                }
                
                return static_cast<decltype(_sz)>(-1);
            }

            static inline file_handler open(
                const char* path, io_options mode)
            {
                file_handler _file;

                if (mode == io_options::rw_create)
                {
                    _file._handle = CreateFileA(
                        path,
                        static_cast<DWORD>(mode),
                        FILE_SHARE_READ | FILE_SHARE_WRITE,
                        nullptr, CREATE_ALWAYS,
                        FILE_FLAG_OVERLAPPED, nullptr
                    );
                }
                else
                {
                    _file._handle = CreateFileA(
                        path, static_cast<DWORD>(mode), 
                        FILE_SHARE_READ | FILE_SHARE_WRITE, 
                        nullptr, OPEN_EXISTING, 
                        FILE_FLAG_OVERLAPPED, nullptr
                    );

                    if (file_error_code() == ERROR_FILE_NOT_FOUND)
                    {
                        _file._handle = CreateFileA(
                            path, static_cast<DWORD>(mode), 
                            FILE_SHARE_READ | FILE_SHARE_WRITE, 
                            nullptr, CREATE_ALWAYS, 
                            FILE_FLAG_OVERLAPPED, nullptr
                        );
                    }
                }

                _file._mode = mode;
                _file._is_overlapped = true;
                _file._is_external = true;

                return _file;
            }

            static inline file_handler get_stdin()
            {
                file_handler _file;

                _file._handle = GetStdHandle(STD_INPUT_HANDLE);
                _file._mode = io_options::read;
                _file._is_overlapped = true;
                _file._is_external = true;

                return _file;
            }

            static inline file_handler get_stdout()
            {
                file_handler _file;

                _file._handle = GetStdHandle(STD_OUTPUT_HANDLE);
                _file._mode = io_options::write;
                _file._is_overlapped = true;
                _file._is_external = true;

                return _file;
            }

            static inline file_handler get_stderr()
            {
                file_handler _file;

                _file._handle = GetStdHandle(STD_ERROR_HANDLE);
                _file._mode = io_options::write;
                _file._is_overlapped = true;
                _file._is_external = true;

                return _file;
            }
        };        
    } // namespace io_detail
    #else
    enum class io_options : i32
    {
        none = 0,
        read       = O_RDONLY,
        write      = O_WRONLY | O_CREAT | O_TRUNC,
        append     = O_WRONLY | O_CREAT | O_APPEND,
        read_write = O_RDWR,
        rw_create  = O_RDWR | O_CREAT | O_TRUNC,
        rw_append  = O_RDWR | O_CREAT | O_APPEND
    };

    namespace io_detail
    {
        static inline auto file_error_code()
        {
            return errno;
        }

        static inline const char* file_error_msg()
        {
            return strerror(file_error_code());
        }

        class file_handler
        {
        private:
            i32 _fd = -1;
            io_options _mode = io_options::none;
            bool _is_external = false;

        public:
            inline file_handler() = default;

            inline file_handler(const file_handler&) = delete;
            inline file_handler& operator=(const file_handler&) = delete;

            inline file_handler(file_handler&& other)
                : _fd{other._fd}, _mode{other._mode}, _is_external{other._is_external}
            {
                other._fd = -1;
            }

            inline file_handler& operator=(file_handler&& rhs)
            {
                _fd = rhs._fd;
                _mode = rhs._mode;
                _is_external = rhs._is_external;
                rhs._fd = -1;
                
                return *this;
            }

            inline ~file_handler()
            {
                close();
            }

            inline void close()
            {
                if (_fd != -1 && _is_external == false)
                {
                    ::close(_fd);
                    _fd = -1;
                }
            }

            inline bool is_open() const
            {
                return _fd != -1;
            }

            inline bool only_read() const
            {
                return _mode == io_options::read;
            }

            inline bool only_write() const
            {
                return _mode == io_options::write;
            }

            inline isize write(const char* data, usize size)
            {
                return ::write(_fd, data, size);
            }

            inline isize read(char* data, usize size)
            {
                return ::read(_fd, data, size);
            }

            inline isize read_line(char* data, usize size, const char* sep = "")
            {
                isize _sz = read(data, size);

                if (_sz == 0) return 0;

                const char* _ptr = nullptr;
                
                while (*sep != '\0' && _ptr == nullptr)
                {
                    _ptr = cstring::find_rev(data, *sep++, _sz - 1);
                }

                if (_ptr != nullptr)
                {
                    isize _off_sz = _ptr - (data + _sz) + 1;
                    ::lseek64(_fd, _off_sz, SEEK_CUR);
                    return _sz + _off_sz;
                }
                
                return -1;
            }

            static inline file_handler open(
                const char* path, io_options mode)
            {
                file_handler fd;
                fd._fd = ::open(path, static_cast<i32>(mode));
                fd._mode = mode;
                fd._is_external = true;

                return fd;
            }

            static inline file_handler get_stdin()
            {
                file_handler fd;
                fd._fd = 0;
                fd._mode = io_options::read;
                fd._is_external = false;

                return fd;
            }

            static inline file_handler get_stdout()
            {
                file_handler fd;
                fd._fd = 1;
                fd._mode = io_options::write;
                fd._is_external = false;

                return fd;
            }

            static inline file_handler get_stderr()
            {
                file_handler fd;
                fd._fd = 2;
                fd._mode = io_options::write;
                fd._is_external = false;

                return fd;
            }
        };  
    } // namespace io_detail
    #endif

    class io : private sstream
    {
    private:
        io_detail::file_handler _file;
        bool _is_eof = false;

        inline io(io_detail::file_handler&& file)
            : _file{move(file)}
        {}

    public:

        inline io(const io&) = delete;
        inline io& operator=(const io&) = delete;

        inline io(io&& other)
            : sstream{move(other.get_stream())}, 
            _file{move(other._file)}, _is_eof{other._is_eof} 
        {}

        inline io& operator=(io&& rhs)
        {
            swap(get_stream(), rhs.get_stream());
            swap(_file, rhs._file);
            swap(_is_eof, rhs._is_eof);

            return *this;
        }

        inline ~io()
        {
            close();
        }

        inline sstream& get_stream()
        {
            return *this;
        }

        inline bool is_open() const
        {
            return _file.is_open();
        }

        inline bool is_eof() const
        {
            return _is_eof;
        }

        inline void close()
        {
            _file.close();
        }

        inline result<reference<io>, runtime_error> flush()
        {
            if (_file.only_read() == true)
            {
                return runtime_error {
                    "Cannot write file. It is in read mode"
                };
            }
            else if (_file.is_open() == false)
            {
                return runtime_error {
                    "Cannot write file. It is not open"
                };
            }

            auto _sz = _file.write(get_stream().data(), get_stream().size());
            get_stream().clear();
            
            if (_sz == static_cast<decltype(_sz)>(-1))
            {
                return runtime_error {io_detail::file_error_msg()};
            }
            
            return {*this};
        }

        inline result<reference<io>, runtime_error> read_chunk()
        {
            if (_file.only_write())
            {
                return runtime_error {
                    "Cannot read file. It is in write mode"
                };
            }
            else if (_file.is_open() == false)
            {
                return runtime_error {
                    "Cannot read file. It is not open"
                };
            }

            auto _sz = _file.read(
                this->get_current_ptr(), 
                this->get_remaining_capacity() - 1
            );

            if (_sz == static_cast<decltype(_sz)>(-1))
            {
                return runtime_error {io_detail::file_error_msg()};
            }

            this->advance_size(static_cast<usize>(_sz));
            *get_stream().end() = '\0';
            _is_eof = static_cast<usize>(_sz) < (get_stream().capacity() - 1);
            
            bool _finished_reading = (
                cstring::find(
                    get_stream().get_separators(), 
                    get_stream().back()
                ) != nullptr || _is_eof == true
            );

            this->set_finished(_finished_reading);
            return {*this};
        }

        inline result<reference<io>, runtime_error> read_line_chunk()
        {
            if (_file.only_write())
            {
                return runtime_error {
                    "Cannot read file. It is in write mode"
                };
            }
            else if (_file.is_open() == false)
            {
                return runtime_error {
                    "Cannot read file. It is not open"
                };
            }

            auto _sz = _file.read_line(
                this->get_current_ptr(), 
                this->get_remaining_capacity() - 1,
                get_stream().get_separators()
            );

            if (_sz == static_cast<decltype(_sz)>(-1))
            {
                return runtime_error {io_detail::file_error_msg()};
            }

            this->advance_size(static_cast<usize>(_sz));
            *get_stream().end() = '\0';
            _is_eof = (_sz == 0);
            
            this->set_finished(true);
            return {*this};
        }

        template <DefaultConstructible T>
        inline result<T, runtime_error> read_value()
        {
            using ret_type = result<T, runtime_error>;

            if (get_stream().size() != 0)
            {
                T _value{};
                option_err<runtime_error> _res = 
                    get_stream().set_data(_value);
                
                return _res.is_ok() ? ret_type{move(_value)} 
                    : ret_type{_res.unwrap_err()};
            }
            else if (_is_eof == false)
            {
                T _value{};
                
                auto _read_res = read_line_chunk();
                
                if (_read_res.is_ok() == false)
                {
                    return {_read_res.unwrap_err()};
                }

                auto _set_res = 
                    _read_res
                    .unwrap()
                    .get()
                    .get_stream()
                    .set_data(_value);

                return _set_res.is_ok() ? ret_type{move(_value)} 
                    : ret_type{_set_res.unwrap_err()};
            }

            return runtime_error{
                "Reached end of file"
            };
        }

        inline result<string, runtime_error> read_line()
        {
            using ret_type = result<string, runtime_error>;
            get_stream().set_separators("\n\t\r");

            if (get_stream().size() != 0)
            {
                string _value{};
                option_err<runtime_error> _res = 
                    get_stream().read_line(_value);
                
                get_stream().set_default_separators();

                return _res.is_ok() ? ret_type{move(_value)} 
                    : ret_type{_res.unwrap_err()};
            }
            else if (_is_eof == false)
            {
                string _value{};
                
                auto _read_res = read_line_chunk();
                
                if (_read_res.is_ok() == false)
                {
                    return {_read_res.unwrap_err()};
                }

                auto _set_res = 
                    _read_res
                    .unwrap()
                    .get()
                    .get_stream()
                    .read_line(_value);

                get_stream().set_default_separators();
                return _set_res.is_ok() ? ret_type{move(_value)} 
                    : ret_type{_set_res.unwrap_err()};
            }

            get_stream().set_default_separators();
            
            return runtime_error{
                "Reached end of file"
            };
        }

        template <typename T, typename... Args>
        inline io& print(T&&, Args&&... args)
        {   
            get_stream().append_data(T{}, forward<Args>(args)...);
            return *this;
        }

        static inline auto& cout()
        {
            static io _cout{io_detail::file_handler::get_stdout()};
            return _cout;
        }

        static inline auto& cerr()
        {
            static io _cerr{io_detail::file_handler::get_stderr()};
            return _cerr;
        }

        static inline auto& cin()
        {
            static io _cin{io_detail::file_handler::get_stdin()};
            return _cin;
        }

        static inline auto load_file(
            const char* file_path, io_options mode = io_options::read)
            -> result<io, runtime_error>
        {
            auto _file = io_detail::file_handler::open(file_path, mode);

            if (_file.is_open() == false)
            {
                return runtime_error {
                    "Cannot open file. File not found"
                };
            }

            return io{move(_file)};
        }
    };

    template <typename T, typename... Args>
    static void print(T&&, Args&&... args)
    {
        if (hsd::io::cout().get_stream().capacity() == 0)
        {
            hsd::io::cout().get_stream().reserve(1024);
        }

        hsd::io::cout()
        .print(T{}, hsd::forward<Args>(args)...)
        .flush()
        .unwrap();
    }

    template <typename T, typename... Args>
    static void println(T&&, Args&&... args)
    {
        if (hsd::io::cout().get_stream().capacity() == 0)
        {
            hsd::io::cout().get_stream().reserve(1024);
        }

        static constexpr T _func{};
        static constexpr auto _fmt = hsd::format_literals::operator""_fmt<_func() + "\n">();

        hsd::io::cout()
        .print(_fmt, hsd::forward<Args>(args)...)
        .flush()
        .unwrap();
    }

    template <typename T, typename... Args>
    static void print_err(T&&, Args&&... args)
    {
        if (hsd::io::cerr().get_stream().capacity() == 0)
        {
            hsd::io::cerr().get_stream().reserve(1024);
        }

        hsd::io::cerr()
        .print(T{}, hsd::forward<Args>(args)...)
        .flush()
        .unwrap();
    }

    template <typename T, typename... Args>
    static void println_err(T&&, Args&&... args)
    {
        if (hsd::io::cerr().get_stream().capacity() == 0)
        {
            hsd::io::cerr().get_stream().reserve(1024);
        }

        static constexpr T _func{};
        static constexpr auto _fmt = hsd::format_literals::operator""_fmt<_func() + "\n">();

        hsd::io::cerr()
        .print(_fmt, hsd::forward<Args>(args)...)
        .flush()
        .unwrap();
    }
} // namespace hsd
