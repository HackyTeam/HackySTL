#pragma once

#include "../Threading/Time.hpp"
#include "Io.hpp"
#include "../Containers/Range.hpp"

#include "../Base/SourceLocation.hpp"

namespace hsd
{
    namespace logger_detail
    {
        class profiler_value
        {
        private:
            source_location _loc;
            precise_clock _clk{};

        public:
            inline profiler_value(source_location&& loc)
                : _loc{move(loc)}
            {}

            inline const char* file_name() const
            {
                return _loc.file_name();
            }

            inline const char* function_name() const
            {
                return _loc.function_name();
            }

            inline u32 line() const
            {
                return _loc.line();
            }

            inline u32 column() const
            {
                return _loc.column();
            }

            inline precise_clock elapsed_time() const
            {
                return _clk.elapsed_time();
            }
        };        
    } // namespace logger_detail
    
    class stack_trace
    {
    private:
        using stack_type = hsd::vector<source_location>;
        using stack_iterator = typename stack_type::iterator;

        static inline stack_type _stack;

    public:

        inline stack_trace() = default;
        inline stack_trace(const stack_trace&) = default;

        inline ~stack_trace()
        {
            _stack.pop_back();
        }

        static inline stack_trace add(
            source_location loc = source_location::current())
        {
            _stack.emplace_back(move(loc));
            return {};
        }

        static inline void print_stack()
        {
            using namespace format_literals;

            for (const auto& _val : _stack | views::reverse)
            {
                println_err(
                    "Info: {}:{}:{}\n\tFunction: {}"_fmt,
                    _val.file_name(), _val.line(), 
                    _val.column(), _val.function_name()
                );
            }
        }
        
        inline const auto& get() const
        {
            return _stack.back();
        }
    };

    class profiler
    {
    private:
        using profiler_type = hsd::vector<logger_detail::profiler_value>;
        using profiler_iterator = typename profiler_type::iterator;

        static inline profiler_type _stack;

    public:

        inline profiler() = default;
        inline profiler(const profiler&) =default;

        inline ~profiler()
        {
            if(_stack.size() > 0)
            {
                using namespace format_literals;

                println(
                    "Info: {}:{}:{}\n\tFunction: {}, time taken: {}us"_fmt, 
                    get().file_name(), get().line(), get().column(), 
                    get().function_name(), get().elapsed_time().to_microseconds()
                );
            }

            _stack.pop_back();
        }

        static inline profiler add(
            source_location loc = source_location::current())
        {
            _stack.emplace_back(move(loc));
            return {};
        }
        
        static inline const logger_detail::profiler_value& get()
        {
            return _stack.back();
        }
    };

    struct stack_trace_error
    {
        inline const char* pretty_error() const
        {   
            stack_trace::print_stack();
            return "\nThe stack was unwind up there";
        }
    };
} // namespace hsd