#pragma once

#include "_SStreamDetail.hpp"
#include "FormatGenerator.hpp"
#include "_PrinterImpl.hpp"
#include "../Wrappers/Tuple.hpp"

namespace hsd
{
    template <typename Base>
    class sstream_wrapper : protected Base
    {
    private:
        using base_type = Base;
        using CharT = typename Base::value_type;

        static constexpr CharT _default_seps[] = {' ', '\t', '\n', '\r', '\0'};
        const CharT* _separators = _default_seps;
        bool _finished_reading = false;

    protected:
        using base_type::_size;

        template <typename T> 
        requires (!IsSame<T, sstream_wrapper>)
        constexpr sstream_wrapper(const T& other)
            : base_type{other}
        {}

        constexpr void set_finished(bool state)
        {
            _finished_reading = state;
        }

        constexpr CharT* get_current_ptr()
        {
            return data() + size();
        }

        constexpr usize get_remaining_capacity() const
        {
            return capacity() - size();
        }

        constexpr void advance_size(usize amount)
        {
            this->_size += amount;
        }

    public:
        using iterator = CharT*;
        using const_iterator = const CharT*;
        
        constexpr sstream_wrapper() = default;

        constexpr sstream_wrapper(const sstream_wrapper&) = delete;
        constexpr sstream_wrapper& operator=(const sstream_wrapper&) = delete;

        constexpr sstream_wrapper(sstream_wrapper&& other)
            : base_type{move(other)}, _separators{other._separators}
        {}

        constexpr sstream_wrapper& operator=(sstream_wrapper&& rhs)
        {
            base_type::operator=(move(rhs));
            _separators = rhs._separators;
            return *this;
        }

        template <typename... Args>
        constexpr option_err<runtime_error> set_data(Args&... args)
        {
            auto _data_set = sstream_detail::
                split_data<sizeof...(Args)>(data(), _separators);

            if (sizeof...(Args) > _data_set.size())
            {
                return runtime_error{"Input too small to parse"};
            }
            else if (sizeof...(Args) == _data_set.size() && _finished_reading == false)
            {
                return runtime_error{"Attempted to parse incomplete data"};
            }
            else
            {                
                option_err<runtime_error> _error{};
                tuple<reference<Args>...> _tup = {reference<Args>{args}...};
                
                auto _set_func = [&_error](const auto& data, auto& arg)
                {
                    using sstream_detail::_parse_from;

                    if (_error.is_ok() == true)
                    {
                        _error = _parse_from(data, arg);
                    }
                };

                [&]<usize... Ints>(index_sequence<Ints...>)
                {
                    (_set_func(_data_set[Ints], _tup.template get<Ints>()), ...);
                }(make_index_sequence<sizeof...(Args)>{});

                if (_error.is_ok() == false)
                {
                    return _error.unwrap_err();
                }

                usize _sz = 0;

                for (const auto* _ptr = _data_set.back().data() + _data_set.back().size(); *_ptr != '\0'; ++_ptr, ++_sz)
                {
                    data()[_sz] = *_ptr;
                }

                this->_size = _sz;

                [[unlikely]] if (size() < capacity())
                {
                    data()[size()] = '\0';
                }
            }

            return {};
        }

        constexpr option_err<runtime_error> read_line(auto& arg)
        {
            constexpr const CharT _new_sep[] = {'\r', '\n', '\0'};
            const CharT* _curr_sep = get_separators();
            
            set_separators(_new_sep);
            
            auto _res = set_data(arg);
            
            set_separators(_curr_sep);

            return _res;
        }

        template <typename T, typename... Args>
        constexpr void write_data(T&&, Args&&... args)
        {
            this->_size = 0;
            append_data<T, Args...>({}, forward<Args>(args)...);
        }

        template <typename T, typename... Args>
        constexpr void append_data(T&&, Args&&... args)
        {
            static constexpr T _func{};
            static constexpr auto fmt = _func();
            using char_type = typename decltype(fmt)::char_type;
            using tup_type = type_tuple<Args...>;

            static_assert(
                is_same<char_type, CharT>::value, 
                "Unsupported character type"
            );

            static constexpr auto _fmt_buf =
                parse_literal<fmt, sizeof...(Args) + 1>().unwrap();

            static constexpr auto _last = _fmt_buf[tup_type::size];
            static constexpr auto _last_fmt = 
                format_literal<char_type, _last.length + 1>
                {
                    .format = {_last.format, _last.length},
                    .base = _last.base
                };


            static_assert(
                _fmt_buf.size() == sizeof...(Args) + 1, 
                "The number of arguments doesn't match"
            );

            if constexpr (sizeof...(Args) != 0)
            {
                tuple _tup = {forward<Args>(args)...};

                [this, &_tup]<usize... Ints>(index_sequence<Ints...>)
                {
                    const auto _print_func = [this]<auto print_fmt>(auto&& val)
                    {
                        using val_type = decay_t<decltype(val)>;

                        if constexpr (print_fmt.base.tag & print_fmt.base.ovr)
                        {
                            static_assert(
                                !(print_fmt.base.tag & print_fmt.base.style),
                                "Format literal cannot have both"
                                " ovr and style tags"
                            );

                            static_assert(
                                requires (val_type _t) {{_t.pretty_args()} -> IsTuple;},
                                "Object must have a pretty_args() method which returns a tuple."    
                            );

                            static constexpr decltype(print_fmt) _curr_fmt = 
                                {
                                    .format = print_fmt.format,
                                    .base = {
                                        .tag = print_fmt.base.tag & ~print_fmt.base.ovr,
                                        .prec_val = print_fmt.base.prec_val,
                                        .style_val = print_fmt.base.style_val
                                    }
                                };

                            constexpr auto _tup_size = decltype(val.pretty_args())::size();

                            [this, _tup_args = val.pretty_args()]<
                                auto pretty_fmt = val_type::template pretty_fmt<CharT>(), usize... Ints2
                            >(index_sequence<Ints2...>)
                            {
                                using namespace format_literals;

                                auto _res = print_to<format<_curr_fmt>(), CharT>(
                                    this->data() + this->size(), this->capacity() - this->size()
                                ).unwrap();

                                advance_size(_res);

                                append_data(operator""_fmt<pretty_fmt>(), _tup_args.template get<Ints2>()...);
                            }(make_index_sequence<_tup_size>{});
                        }
                        else
                        {
                            auto _res = print_to<format<print_fmt>()>(
                                this->data() + this->size(), this->capacity() - this->size(), forward<val_type>(val)
                            ).unwrap();

                            advance_size(_res);
                        }
                    };

                    ((_print_func.template operator()<
                        format_literal<char_type, _fmt_buf[Ints].length + 1>
                        {
                            .format = {_fmt_buf[Ints].format, _fmt_buf[Ints].length},
                            .base = _fmt_buf[Ints].base
                        }
                    >(_tup.template get<Ints>())), ...);
                }(make_index_sequence<sizeof...(Args)>{});

                auto _res = print_to<format<_last_fmt>()>(
                    data() + size(), capacity() - size()
                ).unwrap();

                advance_size(_res);
            }
            else
            {
                auto _res = print_to<format<_last_fmt>()>(
                    data() + size(), capacity() - size()
                ).unwrap();

                advance_size(_res);
            }
        }

        constexpr void pop_back()
        {
            base_type::pop_back();
            this->back() = '\0';
        }

        constexpr void push_back(CharT c)
        {
            if (this->size() != 0)
            {
                this->back() = c;
                this->emplace_back('\0');
            }
            else
            {
                this->emplace_back(c);
                this->emplace_back('\0');
            }
        }

        constexpr void set_separators(const CharT* separators)
        {
            _separators = separators;
        }

        constexpr const CharT* get_separators() const
        {
            return _separators;
        }

        constexpr void set_default_separators()
        {
            _separators = _default_seps;
        }

        constexpr void clear()
        {
            base_type::clear();
            this->emplace_back('\0');
        }

        using base_type::resize;
        using base_type::capacity;
        using base_type::size;
        using base_type::data;
        using base_type::begin;
        using base_type::end;
        using base_type::cbegin;
        using base_type::cend;
        using base_type::back;

        constexpr const_iterator c_str() const
        {
            return this->begin();
        }

        constexpr operator basic_string_view<CharT>() const
        {
            return basic_string_view<CharT>{c_str(), this->size()};
        }
    };

    template <typename CharT, typename Allocator = allocator>
    class basic_sstream : public sstream_wrapper<vector<CharT, Allocator>>
    {
    private:
        using subbase_type = vector<CharT, Allocator>;
        using base_type = sstream_wrapper<subbase_type>;
        using alloc_type = Allocator;
    
    public:
        constexpr basic_sstream()
        requires (DefaultConstructible<alloc_type>) = default;

        constexpr basic_sstream(const alloc_type& alloc)
            : base_type{alloc}
        {}

        constexpr basic_sstream(const basic_sstream&) = delete;
        constexpr basic_sstream& operator=(const basic_sstream&) = delete;

        constexpr basic_sstream(basic_sstream&& other)
            : base_type{move(other)}
        {}

        constexpr basic_sstream& operator=(basic_sstream&& rhs)
        {
            base_type::operator=(move(rhs));
            return *this;
        }

        using subbase_type::reserve;
        using subbase_type::get_allocator;
    };

    template <typename CharT, usize N>
    class static_basic_sstream : public sstream_wrapper<static_vector<CharT, N>>
    {
    private:
        using subbase_type = static_vector<CharT, N>;
        using base_type = sstream_wrapper<subbase_type>;
    
    public:
        constexpr static_basic_sstream() = default;

        constexpr static_basic_sstream(const static_basic_sstream&) = delete;
        constexpr static_basic_sstream& operator=(const static_basic_sstream&) = delete;

        constexpr static_basic_sstream(static_basic_sstream&& other)
            : base_type{move(other)}
        {}

        constexpr static_basic_sstream& operator=(static_basic_sstream&& rhs)
        {
            base_type::operator=(move(rhs));
            return *this;
        }
    };
    
    using sstream = basic_sstream<char>;
    using wsstream = basic_sstream<wchar>;
    using u8sstream = basic_sstream<char8>;
    using u16sstream = basic_sstream<char16>;
    using u32sstream = basic_sstream<char32>;

    using buffered_sstream = basic_sstream<char, hsd::buffered_allocator>;
    using buffered_wsstream = basic_sstream<wchar, hsd::buffered_allocator>;
    using buffered_u8sstream = basic_sstream<char8, hsd::buffered_allocator>;
    using buffered_u16sstream = basic_sstream<char16, hsd::buffered_allocator>;
    using buffered_u32sstream = basic_sstream<char32, hsd::buffered_allocator>;

    template <usize N>
    using static_sstream = static_basic_sstream<char, N>;

    template <usize N>
    using static_wsstream = static_basic_sstream<wchar, N>;

    template <usize N>
    using static_u8sstream = static_basic_sstream<char8, N>;

    template <usize N>
    using static_u16sstream = static_basic_sstream<char16, N>;

    template <usize N>
    using static_u32sstream = static_basic_sstream<char32, N>;
} // namespace hsd
