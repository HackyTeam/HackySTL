#pragma once

#include "_ToStringBase.hpp"
#include "_ToStringFloating.hpp"

namespace hsd
{
    namespace to_string_detail
    {
        template <typename CharT = char>
        static constexpr void to_pseudo_string_int_impl(auto& buf, IsIntegral auto val)
        {
            const bool _negative = val < 0;

            if (val == 0)
            {
               buf.push('0'); 
            }

            for (; val != 0; val /= 10)
            {
                const auto _res = (_negative) ? -(val % 10) : (val % 10);
                buf.push('0' + _res);
            }

            if (_negative) buf.push('-');
        }

        template <typename CharT = char>
        static constexpr void to_pseudo_string_hex_int_impl(auto& buf, IsIntegral auto val)
        {
            add_unsigned_t<decltype(val)> _copy = val;
            constexpr const CharT _hex_table[16] = {
                '0', '1', '2', '3', '4', '5', '6', '7', 
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
            };

            if (_copy == 0)
            {
               buf.push('0'); 
            }

            for (; _copy != 0; _copy >>= 4)
            {
                buf.push(_hex_table[_copy & 15]);
            }
        }
    } // namespace to_string_detail

    template <typename CharT = char>
    static constexpr auto to_pseudo_string(IsIntegral auto val)
    {
        to_string_detail::to_string_value_rev<CharT, 41> _string = {};
        to_string_detail::to_pseudo_string_int_impl(_string, val);

        return _string;
    }

    template <typename CharT = char>
    static constexpr auto to_pseudo_string_hex(IsIntegral auto val)
    {
        to_string_detail::to_string_value_rev<CharT, 35> _string = {};
        to_string_detail::to_pseudo_string_hex_int_impl(_string, val);

        _string.push('x');
        _string.push('0');

        return _string;
    }

    template <typename CharT = char, usize DecPrecision = 6>
    static constexpr auto to_pseudo_string(IsFloat auto val)
    {
        using FpType = decltype(val);
        const bool _neg = val < 0.;
        val = _neg ? -val : val;

        using max_digits = to_string_detail::fp_max_digits<FpType>;
        to_string_detail::to_string_value_for<CharT, max_digits::val + DecPrecision + 2> _str{};

        if (_neg == true)
        {
            _str.push(static_cast<CharT>('-'));
        }

        if (math::is_nan(val) == true)
        {
            _str.push('n');
            _str.push('a');
            _str.push('n');

            return _str;
        }
        else if (val == limits<FpType>::infinity)
        {
            _str.push('i');
            _str.push('n');
            _str.push('f');

            return _str;
        }

        const auto _floor_val = math::trunc(val);        
    
        to_string_detail::round_to_string(_str, _floor_val);
        
        _str.push(static_cast<CharT>('.'));
        
        to_string_detail::point_to_string(_str, DecPrecision, val - _floor_val);

        return _str;
    }

    template <typename CharT = char>
    static constexpr auto to_pseudo_string_hex(IsFloat auto val)
    {
        to_string_detail::to_string_value_rev<CharT, 27> _str = {};
        
        using FpType = decltype(val);
        const bool _neg = val < 0.;
        val = _neg ? -val : val;

        if (math::is_nan(val) == true)
        {
            _str.push('n');
            _str.push('a');
            _str.push('n');

            return _str;
        }
        else if (val == limits<FpType>::infinity)
        {
            _str.push('f');
            _str.push('n');
            _str.push('i');

            if (_neg == true)
            {
                _str.push('-');
            }

            return _str;
        }

        to_string_detail::fp_rep _rep = {};
        
        if (is_constant_evaluated() == true)
        {
            _rep = to_string_detail::get_rep(val);
        }
        else
        {
            const auto _conv = to_string_detail::get_rep_experimental(val);
            _rep.is_normal = _conv.exponent != 0;
            _rep.exponent = -limits<FpType>::max_exponent + 1;
            _rep.exponent += _conv.exponent;
            _rep.mantissa = _conv.mantissa;
            
            if constexpr (limits<FpType>::mantissa == 64)
            {
                _rep.bits = 63;
            }
            else
            {
                _rep.bits = limits<FpType>::mantissa;
            }
        }

        to_string_detail::to_pseudo_string_int_impl(_str, _rep.exponent);
        _str.push('p');
        
        if (_rep.mantissa != 0)
        {
            _rep.mantissa <<= 4 - (_rep.bits & 3);
            _rep.bits += 4 - (_rep.bits & 3);

            for (; (_rep.mantissa & 15) == 0; _rep.mantissa >>= 4, _rep.bits -= 4) {}

            const auto _before_size = _str.size();
            to_string_detail::to_pseudo_string_hex_int_impl(_str, _rep.mantissa);

            for (auto _diff = _str.size() - _before_size; _diff < _rep.bits / 4; ++_diff)
            {
                _str.push('0');
            }

            _str.push('.');
        }

        _str.push('0' + _rep.is_normal);
        _str.push('x');
        _str.push('0');

        if (_neg == true)
        {
            _str.push('-');
        }

        return _str;
    }

    template <typename CharT = char, usize DecPrecision = 6>
    static constexpr auto to_pseudo_string_exp(IsFloat auto val)
    {
        const bool _neg_num = val < 0.;
        val = _neg_num ? -val : val;

        using FpType = decltype(val);
        to_string_detail::to_string_value_for<CharT, DecPrecision + 9> _str{};

        if (math::is_nan(val) == true)
        {
            _str.push('n');
            _str.push('a');
            _str.push('n');

            return _str;
        }
        else if (val == limits<FpType>::infinity)
        {
            _str.push('i');
            _str.push('n');
            _str.push('f');

            return _str;
        }

        constexpr FpType _ten = 10;
        constexpr FpType _log_2_10 = 1 / math::log2(_ten);
        constexpr FpType _eps = 1e-12;
        constexpr FpType _trunc_eps = 2e-6;
        
        i32 _exp = 0;
        
        if (is_constant_evaluated() == true)
        {
            FpType _log_val = (val != 0) ? to_string_detail::get_base_2_exp(val).first * _log_2_10 + _eps : 0;
            _exp = _log_val;
            _exp -= (val < 1 && val != 0 && hsd::math::abs(_log_val - _exp) > _trunc_eps);
        }
        else
        {
            FpType _log_val = (val != 0) ? hsd::math::log2(val) * _log_2_10 + _eps : 0;
            _exp = _log_val;
            _exp -= (val < 1 && val != 0 && hsd::math::abs(_log_val - _exp) > _trunc_eps);
        }
        
        const bool _neg_exp = _exp < 0;

        if (_neg_num == true)
        {
            _str.push(static_cast<CharT>('-'));
        }

        if (_neg_exp == true)
        {
            to_string_detail::point_to_string(
                _str, DecPrecision - _exp, val, -_exp - 1, true
            );
        }
        else
        {
            const auto _floor_val = math::trunc(val);
            to_string_detail::round_to_string(_str, _floor_val, DecPrecision + 1);

            if (val == 0)
            {
                _str.push('.');
            }
            
            const auto _after_length = _str.size() - 2 - ((_neg_num == true) ? 1 : 0);
            to_string_detail::point_to_string(
                _str, DecPrecision, val - _floor_val, _after_length
            );
        }

        _str.push('e');

        if (_neg_exp == false)
        {
            _str.push('+');
        }
        else
        {
            _str.push('-');
            _exp = -_exp;
        }

        if (_exp >= 0 && _exp < 10)
        {
            _str.push('0');
            _str.push('0' + static_cast<CharT>(_exp % 10));

            return _str;
        }

        to_string_detail::to_string_value_rev<CharT, 4> _exp_str{};
        to_string_detail::to_pseudo_string_int_impl(_exp_str, _exp);

        for (const auto _chr : _exp_str) _str.push(_chr);

        return _str;
    }
}