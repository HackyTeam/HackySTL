#pragma once

#include "FormatGenerator.hpp"
#include "ToString.hpp"
#include "../Base/Unicode.hpp"

namespace hsd
{
    template <typename FmtType>
    using fmt_char_type = typename FmtType::template lit_type<1>::char_type;

    template <typename T>
    concept FormatIntegral = IsIntegral<T> && !IsChar<T>;

    template <typename CharT>
    static constexpr CharT* _print_to_impl(CharT* dest, const auto& str)
    {
        for (const CharT _chr : str)
        {
            *dest = _chr;
            ++dest;
        }

        return dest - 1;
    }

    template <format_output fmt, typename CharT>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz)
    {
        using FmtType = decltype(fmt);

        if constexpr (fmt.tag != FmtType::none)
        {
            return runtime_error{"Unknown format tag for character type"};
        }

        usize _write_sz = (fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

        if (_write_sz > sz)
        {
            return runtime_error{"Buffer too small for printing"};
        }

        str = _print_to_impl(str, fmt.format);
        str = _print_to_impl(str, fmt.style);
        str = _print_to_impl(str, fmt.reset);

        return _write_sz;
    }

    template <format_output fmt, typename CharT, FormatIntegral T>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, T&& val)
    {
        using FmtType = decltype(fmt);

        auto _arr = [&val]
        {
            if constexpr (fmt.tag == FmtType::none)
            {
                return to_pseudo_string<CharT>(val);
            }
            else if constexpr ((fmt.tag & FmtType::prec) == FmtType::prec)
            {
                return option_err<runtime_error>{"Precision not allowed for integral type"};
            }
            else if constexpr ((fmt.tag & FmtType::hex) == FmtType::hex)
            {
                return to_pseudo_string_hex<CharT>(val);
            }
            else
            {
                return option_err<runtime_error>{"Unknown format tag for integral type"};
            }
        }();

        if constexpr (IsSame<decltype(_arr), option_err<runtime_error>>)
        {
            return _arr.unwrap_err();
        }
        else
        {
            usize _write_sz = (_arr.size() + fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

            if (_write_sz > sz)
            {
                return runtime_error{"Buffer too small for printing"};
            }

            str = _print_to_impl(str, fmt.format);
            str = _print_to_impl(str, fmt.style);
            str = _print_to_impl(str, _arr) + 1;
            str = _print_to_impl(str, fmt.reset);

            return _write_sz;
        }
    }

    template <format_output fmt, typename CharT, FloatingPointType T>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, T&& val)
    {
        using FmtType = decltype(fmt);

        auto _arr = [&val]
        {
            constexpr usize _prec_val = ((fmt.tag & FmtType::prec) == FmtType::prec) ? fmt.prec_val : 6;
            constexpr auto _tag = fmt.tag & ~fmt.prec;

            if constexpr (_tag == FmtType::none)
            {
                return to_pseudo_string<CharT, _prec_val>(val);
            }
            else if constexpr ((_tag & FmtType::hex) == FmtType::hex)
            {
                return to_pseudo_string_hex<CharT>(val);
            }
            else if constexpr ((_tag & FmtType::exp) == FmtType::exp)
            {
                return to_pseudo_string_exp<CharT, _prec_val>(val);
            }
            else
            {
                return option_err<runtime_error>{"Unknown format tag for floating-point type"};
            }
        }();

        if constexpr (IsSame<decltype(_arr), option_err<runtime_error>>)
        {
            return _arr.unwrap_err();
        }
        else
        {
            usize _write_sz = (_arr.size() + fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

            if (_write_sz > sz)
            {
                return runtime_error{"Buffer too small for printing"};
            }

            str = _print_to_impl(str, fmt.format);
            str = _print_to_impl(str, fmt.style);
            str = _print_to_impl(str, _arr) + 1;
            str = _print_to_impl(str, fmt.reset);

            return _write_sz;
        }
    }

    template <format_output fmt, typename CharT, CharacterType T>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, const T* val)
    {
        using FmtType = decltype(fmt);

        if constexpr (fmt.tag != FmtType::none)
        {
            return runtime_error{"Unknown format tag for character type"};
        }

        usize _write_sz = (fmt.format.size() + fmt.style.size() + fmt.reset.size() - 3);

        if (_write_sz > sz)
        {
            return runtime_error{"Buffer too small for printing"};
        }

        str = _print_to_impl(str, fmt.format);
        str = _print_to_impl(str, fmt.style);

        auto _res = unicode::convert_to<CharT, T>(str, sz, val);

        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }

        usize _pos = sz - _res.unwrap() - 1;
        str = _print_to_impl(str + _pos, fmt.reset);

        return _pos + _write_sz;
    }

    template <format_output fmt, typename CharT, CharacterType T>
    requires (IsSame<fmt_char_type<decltype(fmt)>, CharT>)
    static constexpr result<usize, runtime_error> print_to(CharT* str, usize sz, T&& val)
    {
        const T _arr[] = {val, '\0'}; 
        return print_to<fmt, CharT>(str, sz, _arr);
    }
}