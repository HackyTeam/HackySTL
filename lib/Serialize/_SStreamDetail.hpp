#pragma once

#include "../Containers/Vector.hpp"
#include "../Containers/StringView.hpp"
#include "../Base/CString.hpp"

namespace hsd::sstream_detail
{
    template <typename CharT>
    const CharT* _consume_sep(const CharT* str, const CharT* sep)
    {
        while (*str != '\0' && *basic_cstring<CharT>::find_or_end(sep, *str) == *str)
        {
            str++;
        }

        return str;
    }

    template <typename CharT>
    const CharT* _find_sep(const CharT* str, const CharT* sep)
    {
        while (*str != '\0' && *basic_cstring<CharT>::find_or_end(sep, *str) != *str)
        {
            str++;
        }

        return str;
    }

    template <usize N, typename CharT>
    static inline auto split_data(const CharT* str, const CharT* sep)
    {
        static_vector<basic_string_view<CharT>, N> _buf;
        const CharT* _first_iter = _consume_sep(str, sep);
        const CharT* _second_iter = _find_sep(_first_iter, sep);

        while (*_second_iter != '\0' && _buf.size() < N - 1)
        {
            _buf.emplace_back(_first_iter, static_cast<usize>(_second_iter - _first_iter));
            _first_iter = _consume_sep(_second_iter, sep);
            _second_iter = _find_sep(_first_iter, sep);
        }

        _buf.emplace_back(_first_iter, static_cast<usize>(_second_iter - _first_iter));

        return _buf;
    }

    template <typename CharT, typename T>
    requires (IsIntegral<T> && !IsChar<T>)
    static constexpr option_err<runtime_error> _parse_from(const basic_string_view<CharT>& from, T& to)
    {
        if (from.size() == 0)
        {
            return runtime_error{"Empty input"};
        }

        if (from.size() > 2 && from[0] == '0' && (from[1] == 'x' || from[1] == 'x'))
        {
            to = 0;

            for (usize _idx = 2; _idx < from.size(); ++_idx)
            {
                to <<= 4;
                const CharT _chr = from[_idx];

                if (_chr >= '0' && _chr <= '9')
                {
                    to |= _chr - '0';
                }
                else if (_chr >= 'a' && _chr <= 'f')
                {
                    to |= 10 + (_chr - 'a');
                }
                else if (_chr >= 'A' && _chr <= 'F')
                {
                    to |= 10 + (_chr - 'A');
                }
                else
                {
                    return runtime_error{"Invalid hexadecimal input"};
                }
            }
        }
        else
        {
            remove_unsigned_t<T> _val = 0;

            bool _is_neg = from[0] == '-';
            auto _copy = from;

            if (from[0] == '-' || from[0] == '+')
            {
                _copy = {from.data() + 1, from.size() - 1};
            }

            for (const CharT _chr : _copy)
            {
                _val *= 10;

                if (_chr >= '0' && _chr <= '9')
                {
                    _val += _chr - '0';
                }
                else
                {
                    return runtime_error{"Invalid decimal input"};
                }
            }

            if (_is_neg == true)
            {
                _val = -_val;
            }

            to = _val;
        }

        return {};
    }

    template <typename CharT, FloatingPointType T>
    static constexpr option_err<runtime_error> _parse_from_fixed(const basic_string_view<CharT>& from, T& to)
    {
        T _point = 0;
        T _round = 0;
        T _mul = 1;

        if (from.size() == 0)
        {
            return runtime_error{"Empty input"};
        }

        constexpr T _ten = 10;
        bool _has_point = false;
        const bool _is_negative = from[0] == '-';
        const bool _is_positive = from[0] == '+';

        for (auto _it = from.rbegin(); _it != from.rend() + (_is_positive || _is_negative); --_it)
        {
            const CharT _chr = *_it;

            if (_chr >= '0' && _chr <= '9')
            {
                if (_has_point == false)
                {
                    _point /= _ten;
                    _point += (_chr - '0') / _ten;
                }

                _round += (_chr - '0') * _mul;
                _mul *= _ten;
            }
            else if (_chr == '.' && _has_point == false)
            {
                _round = 0; _mul = 1;
                _has_point = true;
            }
            else
            {
                return runtime_error{"Invalid character"};
            }
        }

        to = (_has_point == true) ? _round + _point : _round;
        to = (_is_negative == true) ? -to : to;

        return {};
    }

    template <typename CharT, FloatingPointType T>
    static constexpr option_err<runtime_error> _parse_from_exp(const basic_string_view<CharT>& from, usize exp_pos, T& to)
    {
        isize _exp = 0;
        constexpr T _ten = 10;
        
        auto _res = _parse_from<CharT>({from.data() + exp_pos + 1, from.size() - exp_pos - 1}, _exp);

        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }

        _res = _parse_from_fixed<CharT>({from.data(), exp_pos}, to);
        
        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        } 
        
        to *= math::pow(_ten, static_cast<T>(_exp));
        return {};
    }

    template <typename CharT, FloatingPointType T>
    static constexpr option_err<runtime_error> _parse_from_hex(const basic_string_view<CharT>& from, T& to)
    {
        bool _has_point = false;
        usize _exp_pos = -1;
        isize _exp = 0;

        T _div = 0.0625;
        T _two = 2;
        
        to = 0;

        for (usize _idx = 2; _idx < from.size(); ++_idx)
        {
            const CharT _chr = from[_idx];

            if (_chr != '.' && _chr != 'p' && _chr != 'P')
            {
                to *= (_has_point == false) ? 16 : 1;
                _div *= (_has_point == true) ? 0.0625 : 1;
            }

            if (_chr >= '0' && _chr <= '9')
            {
                to += (_has_point == false) ? (_chr - '0') : (_chr - '0') * _div;
            }
            else if (_chr >= 'a' && _chr <= 'f')
            {
                to += (_has_point == false) ? (10 + _chr - 'a') : (10 + _chr - 'a') * _div;
            }
            else if (_chr >= 'A' && _chr <= 'F')
            {
                to += (_has_point == false) ? (10 + _chr - 'A') : (10 + _chr - 'F') * _div;
            }
            else if (_chr == '.')
            {
                _has_point = true;
            }
            else if (_chr == 'p' || _chr == 'P')
            {
                _exp_pos = _idx;
                break;
            }
            else
            {
                return runtime_error{"Invalid hexadecimal input"};
            }
        }

        if (_exp_pos == from.npos)
        {
            return runtime_error{"Hexadecimal float literal requires exponent"};
        }

        auto _res = _parse_from<CharT>({from.data() + _exp_pos + 1, from.size() - _exp_pos - 1}, _exp);

        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }

        to *= math::pow(_two, static_cast<T>(_exp));
        return {};
    }

    template <typename CharT, FloatingPointType T>
    static constexpr option_err<runtime_error> _parse_from(const basic_string_view<CharT>& from, T& to)
    {
        constexpr const CharT _arr[] = {'e', 'E', '\0'};

        if (from.size() > 2 && from[0] == '0' && (from[1] == 'x' || from[1] == 'x'))
        {
            return _parse_from_hex<CharT>(from, to);
        }
        else 
        {
            usize _exp_pos = from.find_first_of(_arr);

            if (_exp_pos != from.npos)
            {
                return _parse_from_exp(from, _exp_pos, to);
            }
            else
            {
                return _parse_from_fixed(from, to);
            }
        }
    }

    template <typename CharT, CharacterType T>
    static constexpr option_err<runtime_error> _parse_from(const basic_string_view<CharT>& from, T& to)
    {
        if (from.size() == 0)
        {
            return runtime_error{"Empty input"};
        }

        T _arr[2] = {};

        auto _res = unicode::convert_to(_arr, 2, from.data(), from.size());
        to = _arr[0];

        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }

        return {};
    }

    template <typename CharT, CharacterType T, usize N>
    static constexpr option_err<runtime_error> _parse_from(const basic_string_view<CharT>& from, T (&to)[N])
    {
        if (from.size() == 0)
        {
            return runtime_error{"Empty input"};
        }

        auto _res = unicode::convert_to(to, N, from.data(), from.size());

        if (_res.is_ok() == false)
        {
            return _res.unwrap_err();
        }

        return {};
    }
} // namespace hsd::sstream_detail
