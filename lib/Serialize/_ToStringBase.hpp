#pragma once

#include "../Base/Types.hpp"

namespace hsd::to_string_detail
{
    template <typename CharT, usize N>
    class to_string_value_rev
    {
    private:
        usize _begin_idx = N;
        CharT _arr[N]{};

    public:
        constexpr void push(CharT val)
        {
            _arr[--_begin_idx] = val;
        }

        constexpr const auto* data() const
        {
            return begin();
        }

        constexpr usize size() const
        {
            return N - _begin_idx;
        }

        constexpr const auto* begin() const
        {
            return _arr + _begin_idx;
        }

        constexpr const auto* end() const
        {
            return _arr + N;
        }
    };

    template <typename CharT, usize N>
    class to_string_value_for
    {
    private:
        usize _end_idx = 0;
        CharT _arr[N]{};
    
    public:
        constexpr void push(CharT val)
        {
            _arr[_end_idx++] = val;
        }
    
        constexpr const auto* data() const
        {
            return begin();
        }
    
        constexpr usize size() const
        {
            return _end_idx;
        }
    
        constexpr const auto* begin() const
        {
            return _arr;
        }
    
        constexpr const auto* end() const
        {
            return _arr + _end_idx;
        }
    };
} // namespace hsd:: to_string_detail