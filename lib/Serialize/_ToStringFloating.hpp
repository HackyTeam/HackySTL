#pragma once

#include "../Base/Math.hpp"
#include "../Wrappers/Pair.hpp"
#include "../Containers/StackArray.hpp"

namespace hsd::to_string_detail
{
    struct fp_rep
    {
        bool is_normal;
        u16 bits;
        i32 exponent;
        u64 mantissa;
    };

    template <typename>
    struct fp_max_digits;

    template <>
    struct fp_max_digits<f128>
    {
        static constexpr usize val = 4933;
    };

    template <>
    struct fp_max_digits<f64>
    {
        static constexpr usize val = 309;
    };

    template <>
    struct fp_max_digits<f32>
    {
        static constexpr usize val = 39;
    };

    template <FloatingPointType T>
    static consteval auto get_log_table()
    {
        constexpr usize _sz = 
            limits<T>::max_exponent - 
            limits<T>::min_exponent_denorm;

        stack_array<T, _sz> _arr{};
        auto _val = limits<T>::denorm_min;

        for (auto _it = _arr.begin(); _it != _arr.end() - 1; ++_it)
        {
            *_it = _val;
            _val *= 2;
        }

        _arr[_sz - 1] = _val;
        return _arr;
    }

    template <bool Floating = true>
    static constexpr auto get_base_2_exp(auto val)
    {
        using FpType = decltype(val);
        
        constexpr auto _min_exp = limits<FpType>::min_exponent_denorm;
        constexpr auto _max_exp = limits<FpType>::max_exponent;
        constexpr auto _array = get_log_table<FpType>();
        
        i32 _left = 0;
        i32 _right = _max_exp - _min_exp - 1;
        
        while (_left <= _right)
        {
            const auto _middle = (_left + _right) / 2;

            if (_array[_middle] < val)
            {
                _left = _middle + 1;
            }
            else if (_array[_middle] > val)
            {
                _right = _middle - 1;
            }
            else
            {
                return pair{_middle + _min_exp, _array[_middle]};
            }
        }

        if constexpr (Floating == true)
        {    
            // small enough number to not be bothered
            auto _div_val = math::log2(val / _array[_right]);

            return pair{static_cast<i32>(_right + _min_exp + _div_val), _array[_right]};
        }
        else
        {
            return pair{_right + _min_exp, _array[_right]};
        }
    }

    template <FloatingPointType T>
    static constexpr auto get_subnormal_exp()
    {
        i32 _exp = 0;
        constexpr T _until = limits<T>::min / 2;

        for (T _val = 1; _val != _until; _val /= 2)
        {
            ++_exp;
        }

        return _exp;
    }

    template <FloatingPointType T>
    static constexpr auto generate_mantissa(T val)
    {
        u64 _mantissa = 0;
        u16 _bits = 0;
        T _mantissa_mask = 1.5;

        for (; _mantissa_mask != 1; _mantissa_mask *= .5, _mantissa_mask += 1)
        {
            _mantissa <<= 1;
            ++_bits;

            if (val >= _mantissa_mask)
            {
                _mantissa |= 1;
                val -= 1 + (_mantissa_mask - 1);
                val += 1;
            }

            _mantissa_mask -= 1;
        }

        return pair{_mantissa, _bits};
    }

    template <FloatingPointType T>
    static constexpr auto generate_subnorm_rep(T val)
    {
        constexpr auto _exp = -get_subnormal_exp<T>();
        constexpr auto _subnorm_max = limits<T>::min / 2;
        u64 _mant = 0;
        u16 _bits = 0;

        for (T _mantissa_mask = _subnorm_max; _mantissa_mask != 0; _mantissa_mask *= .5)
        {
            ++_bits;
            _mant <<= 1;

            if (val >= _mantissa_mask)
            {
                _mant |= 1;
                val -= _mantissa_mask;
            }
        }

        return fp_rep {
            .is_normal = false, 
            .bits = _bits, 
            .exponent = _exp, 
            .mantissa = _mant
        };
    }

    template <FloatingPointType T>
    static constexpr auto get_rep(T val)
    {
        if (math::abs(val) == 0)
        {
            return fp_rep{};
        }
        if (val < limits<T>::min)
        {
            return generate_subnorm_rep(val);
        }

        auto [_exp, _pow] = get_base_2_exp<false>(val);
        auto [_mant, _bits] = generate_mantissa(val / _pow);

        return fp_rep {
            .is_normal = true,
            .bits = _bits, 
            .exponent = _exp, 
            .mantissa = _mant
        };
    }
        
    static constexpr auto get_rep_experimental(FloatingPointType auto val)
    {
        using FpType = decltype(val);
        using IntType = conditional_t<
            sizeof(FpType) == 16, u128, conditional_t<
                sizeof(FpType) == 8, u64, u32
            >
        >;

        struct rep
        {
            IntType mantissa : limits<FpType>::mantissa;
            IntType exponent : limits<FpType>::exponent;
            IntType sign : 1;
        };

        if constexpr (sizeof(FpType) == 16 && IsSame<f128, FpType>)
        {
            // x87 moronic math
            auto _res = bit_cast<rep>(val);
            _res.mantissa <<= 1; 
            _res.mantissa >>= 1;

            return _res;
        }
        else
        {
            return bit_cast<rep>(val);
        }
    }

    static constexpr void round_to_string(
        auto& buf, FloatingPointType auto val, usize limit = -1)
    {
        if (val == 0)
        {
            buf.push('0');
            return;
        }
        
        namespace ctmath_impl = math::constexpr_math::ctmath_detail;
        
        using FpType = decltype(val);
        constexpr FpType _ten = 10;
        constexpr FpType _point_one = 0.1;
        constexpr FpType _ten_neg_pow_17 = 1e-17;
        constexpr FpType _ten_pow_10 = 1e10;
        
        constexpr FpType _conv_add = ctmath_impl::pow_type_check(_ten, -static_cast<i32>(limits<FpType>::digits10) + 1);
        constexpr FpType _correct_criteria = 1 - _conv_add;
        
        usize _exp = 0, _idx_p = 1;
        bool _is_exp = limit != static_cast<usize>(-1);
        
        for (auto _copy = val; _copy != 0; _copy = math::trunc(_copy * _point_one))
        {
            ++_exp;
        }
        
        auto _exp_val = ctmath_impl::pow_type_check(_ten, _exp - 1);
        
        for (_exp = _exp - 1; _exp != 0 && _idx_p < limit; --_exp, ++_idx_p)
        {
            const auto _correction = _exp_val + _exp_val * _ten_neg_pow_17;
            const auto _conv = val / _exp_val;
            auto _conv_correct = val / _correction;
            _conv_correct += (_conv_correct > _correct_criteria && _conv_correct < 1) ? _conv_add : 0;
            
            const auto _normalized_val = (_conv - _conv_correct) * _ten_pow_10;
            const char _digit = static_cast<char>(math::trunc(_conv_correct));
            
            buf.push('0' + _digit);
            
            if (_is_exp == true)
            {
                buf.push('.');
                _is_exp = false;
            }
            
            val -= math::trunc(_conv_correct) * _exp_val;
            val += _normalized_val; 
            _exp_val *= _point_one;
        }

        const auto _conv = math::trunc(val / _exp_val);
        const char _digit = static_cast<char>(_conv);

        buf.push('0' + _digit);

        if (_is_exp == true)
        {
            buf.push('.');
            _is_exp = false;
        }
    }

    static constexpr void point_to_string(
        auto& buf, usize precision, FloatingPointType auto val, 
        usize current_p = 0, bool is_exp = false)
    {
        using FpType = decltype(val);

        if (current_p >= precision)
        {
            return;
        }

        if (val == static_cast<FpType>(0))
        {
            for (usize _idx = current_p; _idx != precision; ++_idx)
            { 
                if (is_exp == true)
                {
                    buf.push('.');
                    is_exp = false;
                }

                buf.push('0');
            }

            return;
        }

        constexpr FpType _ten = 10;

        constexpr auto _correct_threshold = 
            limits<FpType>::max_digits10 * 2 + 
            limits<FpType>::max_digits10 / 2;

        auto _correct_digits = [&val, _ten]()
        {
            namespace ctmath_impl = math::constexpr_math::ctmath_detail;

            constexpr FpType _corr_sub = ctmath_impl::pow_type_check(_ten, -static_cast<i32>(limits<FpType>::digits10));
            constexpr FpType _corr_add = ctmath_impl::pow_type_check(_ten, -static_cast<i32>(limits<FpType>::digits10) - 1);
            constexpr FpType _conv_add = ctmath_impl::pow_type_check(_ten, -static_cast<i32>(limits<FpType>::digits10) + 1);
            
            constexpr FpType _correct_mul = 1 - _corr_sub + _corr_add;
            constexpr FpType _correct_criteria = 1 - _conv_add;

            constexpr auto _div_exp = (limits<FpType>::max_digits10 / 10);
            constexpr auto _max_div_exp = (_div_exp != -0) ? _div_exp * 2 : 1;

            constexpr FpType _div_factor = ctmath_impl::pow_type_check(_ten, _max_div_exp);

            const auto _conv = val * _ten;
            auto _conv_correct = _conv * _correct_mul;
            _conv_correct += (_conv_correct > _correct_criteria && _conv_correct < 1) ? _conv_add : 0;

            const auto _normalized_val = (_conv - _conv_correct) / _div_factor;
            const auto _trunc_val = math::trunc(_conv_correct);
            val = _conv - _trunc_val + math::abs(_normalized_val);

            return _trunc_val;
        };

        auto _continue_normal = [&val, _ten]()
        {
            namespace ctmath_impl = math::constexpr_math::ctmath_detail;

            constexpr FpType _conv_add = ctmath_impl::pow_type_check(_ten, -static_cast<i32>(limits<FpType>::digits10) + 1);
            constexpr FpType _correct_criteria = 1 - _conv_add;

            auto _conv = val * _ten;
            _conv += (_conv > _correct_criteria && _conv < 1) ? _conv_add : 0;
            
            const auto _trunc_val = math::trunc(_conv);
            val = _conv - _trunc_val;

            return _trunc_val;
        };

        const auto _normal_loop_limit = hsd::math::min(precision, _correct_threshold);
        auto _ignore_loop_limit = hsd::math::min(precision, current_p);
        
        usize _idx;

        for (_idx = 0; _idx < _normal_loop_limit; ++_idx)
        {
            const auto _trunc_val = _continue_normal();
            
            if (_idx >= current_p)
            {
                if (_trunc_val > 0 && is_exp == true)
                {
                    buf.push(static_cast<char>(_trunc_val) + '0');
                    buf.push('.');
                    is_exp = false;
                }
                else if (is_exp == false) [[likely]]
                {
                    buf.push(static_cast<char>(_trunc_val) + '0');
                }
            }
        }

        const bool _can_decrement = (
            _ignore_loop_limit != 0 && is_exp == true && _idx != _ignore_loop_limit
        ); 
        
        _ignore_loop_limit -= _can_decrement;

        for (; _idx < _ignore_loop_limit; ++_idx)
        {
            _correct_digits();
        }

        // Exponent correction needed
        if ( _idx < precision && is_exp == true)
        {
            const auto _trunc_val = _correct_digits();

            if (_trunc_val != 0)
            {
                buf.push(static_cast<char>(_trunc_val) + '0');
                buf.push('.');
                is_exp = false;
                
                precision -= _can_decrement;
                _idx += 1;
            }
        }

        // Handle 0 edge case as still being the first digit somehow
        if ( _idx < precision)
        {
            const auto _trunc_val = _correct_digits();
            buf.push(static_cast<char>(
                (_trunc_val == 0 && is_exp == true) ? 1 : _trunc_val) + '0'
            );

            if (is_exp == true)
            {
                precision -= _can_decrement;
                buf.push('.');
                is_exp = false;
            }
        }

        for (_idx += 1; _idx < precision; ++_idx)
        {
            const auto _trunc_val = _correct_digits();
            buf.push(static_cast<char>(_trunc_val) + '0');
        }
    }
}  // namespace hsd::to_string_detail