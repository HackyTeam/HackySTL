#pragma once

#include "../Base/Utility.hpp"

namespace hsd
{
    template <typename T, typename U>
    concept IsComparablePair = (
        requires(T lhs){ lhs == lhs -> template IsSame<bool>; } &&
        requires(U rhs){ rhs == rhs -> template IsSame<bool>; }
    );

    template <typename T1, typename T2>
    struct pair
    {
        T1 first;
        T2 second;

        constexpr ~pair() = default;
        constexpr pair() = default;

        template <typename _T1, typename _T2>
        constexpr pair(_T1&& first_val, _T2&& second_val)
            : first{forward<_T1>(first_val)}, second{forward<_T2>(second_val)}
        {}

        constexpr pair(const pair& other)
            : first{other.first}, second{other.second}
        {}
        
        constexpr pair(pair&& other)
            : first{move(other.first)}, second{move(other.second)}
        {}

        constexpr pair& operator=(const pair& rhs)
        {
            first = rhs.first;
            second = rhs.second;
            return *this;
        }
        
        constexpr pair& operator=(pair&& rhs)
        {
            swap(first, rhs.first);
            swap(second, rhs.second);
            return *this;
        }

        constexpr bool operator==(const pair& rhs) const
        requires (IsComparablePair<T1, T2>)
        {
            return first == rhs.first && second == rhs.second;
        }

        constexpr bool operator!=(const pair& rhs) const
        requires (IsComparablePair<T1, T2>)
        {
            return !(*this == rhs);
        }
    };

    template <typename T1, typename T2> pair(const T1&, const T2&) 
        -> pair<decay_t<T1>, decay_t<T2>>;
    template <typename T1, typename T2> requires (!is_const<T1>::value)
        pair(T1&&, const T2&) -> pair<decay_t<T1>, decay_t<T2>>;
    template <typename T1, typename T2> requires (is_const<T1>::value)
        pair(T1&&, const T2&) -> pair<const decay_t<T1>, decay_t<T2>>;
    template <typename T1, typename T2> requires (!is_const<T2>::value)
        pair(const T1&, T2&&) -> pair<decay_t<T1>, decay_t<T2>>;
    template <typename T1, typename T2> requires (is_const<T2>::value)
        pair(const T1&, T2&&) -> pair<decay_t<T1>, const decay_t<T2>>;

    template <typename T1, typename T2> 
    requires (!is_const<T1>::value && !is_const<T2>::value)
        pair(T1&&, T2&&) -> pair<decay_t<T1>, decay_t<T2>>;

    template <typename T1, typename T2> 
    requires (is_const<T1>::value && !is_const<T2>::value)
        pair(T1&&, T2&&) -> pair<const decay_t<T1>, decay_t<T2>>;

    template <typename T1, typename T2> 
    requires (!is_const<T1>::value && is_const<T2>::value)
        pair(T1&&, T2&&)-> pair<decay_t<T1>, const decay_t<T2>>;

    template <typename T1, typename T2>
    requires (is_const<T1>::value && is_const<T2>::value)
        pair(T1&&, T2&&) -> pair<const decay_t<T1>, const decay_t<T2>>;

    template <typename T1, typename T2>
    static constexpr auto make_pair(T1&& first, T2&& second)
    {
        return pair{forward<T1>(first), forward<T2>(second)};
    }
} // namespace hsd