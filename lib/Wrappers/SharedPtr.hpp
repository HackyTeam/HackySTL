#pragma once

#include "../Base/Allocator.hpp"
#include "../Threading/Atomic.hpp"
#include "../Base/Utility.hpp"

namespace hsd
{
    namespace non_atomic_types
    {
        namespace shared_detail
        {
            template <typename Base, typename Derived>
            concept ConvertibleDerived = (
                Convertible<Derived, Base> || 
                std::is_base_of_v<Base, Derived>
            );

            template < typename T, typename Allocator >
            class storage
            {
            public:
                using alloc_type = Allocator;
                using pointer_type = remove_array_t<T>*;
                using value_type = remove_array_t<T>;

            private:
                alloc_type _alloc;
                pointer_type _data = nullptr;
                usize _size = 0;

                template <typename U, typename Alloc>
                friend class storage;

            public:
                constexpr storage()
                requires (DefaultConstructible<alloc_type>) = default;

                constexpr storage(pointer_type ptr, usize size)
                requires (DefaultConstructible<alloc_type>)
                    : _data{ptr}, _size{size}
                {}

                constexpr storage(const alloc_type& alloc, usize size)
                requires (CopyConstructible<value_type>)
                    : _alloc{alloc}, _size{size}
                {
                    _data = _alloc
                        .template allocate<value_type>(size)
                        .unwrap();
                    
                    construct_at(_data);

                    for (usize _index = 0; _index < size; _index++)
                        construct_at(&_data[_index]);
                }

                constexpr storage(pointer_type ptr, const alloc_type& alloc, usize size)
                requires (CopyConstructible<value_type>)
                    : _alloc{alloc}, _data{ptr}, _size{size}
                {}

                constexpr storage(const storage& other)
                    : _alloc{other._alloc}, _data{other._data}, _size{other._size}
                {}

                constexpr storage(storage&& other)
                    : _alloc{move(other._alloc)}
                {
                    _data = exchange(other._data, nullptr);
                    swap(_size, other._size);
                }

                template <typename U = T>
                constexpr storage(const storage<U, Allocator>& other)
                    : _alloc{other._alloc}, _data{other._data}, _size{other._size}
                {}

                template <typename U = T>
                constexpr storage(storage<U, Allocator>&& other)
                    : _alloc{move(other._alloc)}
                {
                    _data = exchange(other._data, nullptr);
                    swap(_size, other._size);
                }

                constexpr storage& operator=(const storage& rhs)
                {
                    _alloc = rhs._alloc;
                    _data = rhs._data;
                    _size = rhs._size;
                    return *this;
                }

                constexpr storage& operator=(storage&& rhs)
                {
                    swap(_alloc, rhs._alloc);
                    _data = exchange(rhs._data, nullptr);                    
                    swap(_size, rhs._size);
                    return *this;
                }

                template <typename U = T>
                constexpr storage& operator=(const storage<U, Allocator>& rhs)
                {
                    _alloc = rhs._alloc;
                    _data = rhs._data;
                    _size = rhs._size;
                    return *this;
                }

                template <typename U = T>
                constexpr storage& operator=(storage<U, Allocator>&& rhs)
                {
                    swap(_alloc, rhs._alloc); 
                    _data = exchange(rhs._data, nullptr);               
                    swap(_size, rhs._size);
                    return *this;
                }

                constexpr auto deallocate()
                {
                    return _alloc.deallocate(_data, _size);
                }

                constexpr usize get_size() const
                {
                    return _size;
                }

                constexpr auto* get_pointer() const
                {
                    return _data;
                }

                constexpr void set_pointer(pointer_type ptr)
                {
                    this->_data = ptr;
                }

                constexpr void set_size(usize size)
                {
                    _size = size;
                }
            };

            template <typename Allocator>
            class counter
            {
            public:
                using alloc_type = Allocator;
                using pointer_type = usize*;
                using value_type = usize;

            private:
                alloc_type _alloc;
                pointer_type _data = nullptr;              

            public:
                constexpr counter()
                requires (DefaultConstructible<alloc_type>)
                {
                    _data = _alloc
                        .template allocate<value_type>(1)
                        .unwrap();

                    *_data = 1;
                }

                constexpr counter(const alloc_type& alloc)
                    : _alloc{alloc}
                {
                    _data = _alloc
                        .template allocate<value_type>(1)
                        .unwrap();
                    
                    *_data = 1;
                }

                constexpr counter(pointer_type ptr)
                requires (DefaultConstructible<alloc_type>)
                    : _data{ptr}
                {
                    (*_data)++;
                }

                constexpr counter(const alloc_type& alloc, usize* ptr)
                    : _alloc{alloc}, _data{ptr}
                {
                    (*_data)++;
                }

                constexpr counter(const counter& other)
                    : _alloc{other._alloc}, _data{other._data}
                {
                    (*_data)++;
                }

                constexpr counter& operator=(const counter& rhs)
                {
                    _alloc = rhs._alloc;
                    _data = rhs._data;
                    (*_data)++;
                    return *this;
                }

                constexpr counter& operator=(counter&& rhs)
                {
                    swap(_alloc, rhs._alloc);
                    swap(_data, rhs._data);
                    return *this;
                }

                constexpr usize& operator*()
                {
                    return *_data;
                }

                constexpr auto deallocate()
                {
                    return _alloc.deallocate(_data, 1);
                }

                constexpr usize get_size() const
                {
                    return 1u;
                }

                constexpr auto* get_pointer() const
                {
                    return _data;
                }

                constexpr void set_pointer(pointer_type ptr)
                {
                    _data = ptr;
                }
            };
            
        } // namespace shared_detail

        template < typename T, typename Allocator = allocator >
        class shared_ptr
        {
        private:
            using storage_type = shared_detail::storage<T, Allocator>;
            using counter_type = shared_detail::counter<Allocator>;

        public:
            using alloc_type = Allocator;
            using pointer_type = typename storage_type::pointer_type;
            using value_type = typename storage_type::value_type;
            using reference_type = typename storage_type::value_type&;

        private:
            storage_type _value;
            counter_type _count{};

            template <typename U, typename Alloc>
            friend class shared_ptr;

            constexpr void _delete()
            {
                if (_count.get_pointer() != nullptr)
                {
                    (*_count)--;

                    if (*_count == 0)
                    {
                        if (get() != nullptr) 
                        {
                            if constexpr (is_array<T>::value)
                            {
                                for (usize i = 0, size = _value.get_size(); i < size; ++i)
                                    get()[size-i].~value_type();
                            }
                            else
                            {
                                get()->~value_type();
                            }
                        }
                        _value.deallocate().unwrap();
                        _count.deallocate().unwrap();
                    }

                    _value.set_pointer(nullptr);
                    _count.set_pointer(nullptr);
                }
            }

        public:
            constexpr shared_ptr() = default;
            constexpr shared_ptr(null_type) {}

            constexpr shared_ptr(pointer_type ptr) 
            requires (DefaultConstructible<alloc_type>)
                : _value{ptr, 1u}
            {}

            constexpr shared_ptr(pointer_type ptr, usize size) 
            requires (DefaultConstructible<alloc_type>)
                : _value{ptr, size}
            {}

            constexpr shared_ptr(const alloc_type& alloc) 
                : _value{alloc, 1u}, _count{alloc}
            {}

            constexpr shared_ptr(const alloc_type& alloc, usize size) 
                : _value{alloc, size}, _count{alloc}
            {}

            constexpr shared_ptr(pointer_type ptr, 
                const alloc_type& alloc, usize size) 
                : _value{ptr, alloc, size}, _count{alloc}
            {}

            template <typename U = T> 
            constexpr shared_ptr(const shared_ptr<U, Allocator>& other)
            requires(shared_detail::ConvertibleDerived<T, U>)
                : _value{other._value}, _count{other._count}
            {}

            template <typename U = T> 
            constexpr shared_ptr(shared_ptr<U, Allocator>&& other)
            requires(shared_detail::ConvertibleDerived<T, U>)
                : _value{move(other._value)}, _count{move(other._count)}
            {}

            constexpr ~shared_ptr()
            {
                _delete();
            }

            constexpr shared_ptr& operator=(null_type)
            {
                _delete();
                return *this;
            }

            template <typename U = T> 
            requires(shared_detail::ConvertibleDerived<T, U>)
            constexpr shared_ptr& operator=(shared_ptr<U, Allocator>& rhs)
            {
                _delete();
                _value = rhs._value;
                _count = rhs._count;
                return *this;
            }

            template <typename U = T> 
            requires(shared_detail::ConvertibleDerived<T, U>)
            constexpr shared_ptr& operator=(shared_ptr<U, Allocator>&& rhs)
            {
                _delete();
                _value = move(rhs._value);
                _count = move(rhs._count);
                return *this;
            }

            constexpr bool operator==(const shared_ptr& rhs) const
            {
                return get() == rhs.get();
            }

            constexpr bool operator!=(const shared_ptr& rhs) const
            {
                return get() == rhs.get();
            }

            constexpr bool operator==(null_type) const
            {
                return get() == nullptr;
            }

            constexpr bool operator!=(null_type) const
            {
                return get() == nullptr;
            }

            constexpr auto* get()
            {
                return _value.get_pointer();
            }

            constexpr auto* get() const
            {
                return _value.get_pointer();
            }

            constexpr auto* operator->()
            {
                return get();
            }

            constexpr auto* operator->() const
            {
                return get();
            }

            constexpr auto& operator*()
            {
                return *get();
            }

            constexpr auto& operator*() const
            {
                return *get();
            }

            constexpr usize get_count()
            {
                return *_count;
            }

            constexpr bool is_unique()
            {
                return get_count() == 1;
            }
        };

        template < typename T, typename Allocator >
        struct MakeShr
        {
            using single_object = shared_ptr<T, Allocator>;
        };

        template < typename T, typename Allocator >
        struct MakeShr<T[], Allocator>
        {
            using array = shared_ptr<T[], Allocator>;
        };

        template < typename T, usize N, typename Allocator >
        struct MakeShr<T[N], Allocator>
        {
            struct invalid_type {};  
        };

        template < typename T, typename Allocator = allocator, typename... Args >
        requires (DefaultConstructible<Allocator> && IsSame<remove_array_t<T>, T>)
        static constexpr typename MakeShr<T, Allocator>::single_object 
        make_shared(Args&&... args)
        {
            Allocator _alloc;

            auto* _ptr = _alloc
                .template allocate<remove_array_t<T>>(1)
                .unwrap();
            
            construct_at(_ptr, forward<Args>(args)...);
            
            return _ptr;
        }

        template < typename T, typename Allocator = allocator, typename U, typename... Args >
        requires (IsSame<remove_array_t<T>, T> && IsSame<Allocator, U>)
        static constexpr typename MakeShr<T, Allocator>::single_object 
        make_shared(U& alloc, Args&&... args)
        {
            auto* _ptr = alloc
                .template allocate<remove_array_t<T>>(1)
                .unwrap();

            construct_at(_ptr, forward<Args>(args)...);
            return {_ptr, alloc, 1};
        }

        template < typename T, typename Allocator = allocator >
        requires (DefaultConstructible<Allocator> && !IsSame<remove_array_t<T>, T>)
        static constexpr typename MakeShr<T, Allocator>::array
        make_shared(usize size)
        {
            Allocator _alloc;
            
            auto* _ptr = _alloc
                .template allocate<remove_array_t<T>>(size)
                .unwrap();
            
            return {_ptr, size};
        }

        template < typename T, typename Allocator = allocator, typename U >
        requires (!IsSame<remove_array_t<T>, T> && IsSame<Allocator, U>)
        static constexpr typename MakeShr<T, Allocator>::array
        make_shared(U& alloc, usize size)
        {
            return {alloc, size};
        }

        template < typename T, typename Allocator = allocator, typename... Args >
        static constexpr typename MakeShr<T, Allocator>::invalid_type 
        make_shared(Args&&...) = delete;
    }

    namespace atomic_types
    {
        namespace shared_detail
        {
            template <typename Base, typename Derived>
            concept ConvertibleDerived = (
                Convertible<Derived, Base> || 
                std::is_base_of_v<Base, Derived>
            );

            template < typename T, typename Allocator >
            class storage
            {
            public:
                using alloc_type = Allocator;
                using pointer_type = remove_array_t<T>*;
                using value_type = remove_array_t<T>;

            private:
                alloc_type _alloc;
                pointer_type _data = nullptr;
                usize _size = 0;

                template <typename U, typename Alloc>
                friend class storage;

            public:
                constexpr storage()
                requires (DefaultConstructible<alloc_type>) = default;

                constexpr storage(pointer_type ptr, usize size)
                requires (DefaultConstructible<alloc_type>)
                    : _data{ptr}, _size{size}
                {}

                constexpr storage(const alloc_type& alloc, usize size)
                requires (CopyConstructible<value_type>)
                    : _alloc{alloc}, _size{size}
                {
                    _data = _alloc
                        .template allocate<value_type>(size)
                        .unwrap();
                    
                    construct_at(_data);

                    for (usize _index = 0; _index < size; _index++)
                        construct_at(&_data[_index]);
                }

                constexpr storage(pointer_type ptr, const alloc_type& alloc, usize size)
                requires (CopyConstructible<value_type>)
                    : _alloc{alloc}, _data{ptr}, _size{size}
                {}

                constexpr storage(const storage& other)
                    : _alloc{other._alloc}, _data{other._data}, _size{other._size}
                {}

                constexpr storage(storage&& other)
                    : _alloc{move(other._alloc)}
                {
                    _data = exchange(other._data, nullptr);
                    swap(_size, other._size);
                }

                template <typename U = T>
                constexpr storage(const storage<U, Allocator>& other)
                    : _alloc{other._alloc}, _data{other._data}, _size{other._size}
                {}

                template <typename U = T>
                constexpr storage(storage<U, Allocator>&& other)
                    : _alloc{move(other._alloc)}
                {
                    _data = exchange(other._data, nullptr);
                    swap(_size, other._size);
                }

                constexpr storage& operator=(const storage& rhs)
                {
                    _alloc = rhs._alloc;
                    _data = rhs._data;
                    _size = rhs._size;
                    return *this;
                }

                constexpr storage& operator=(storage&& rhs)
                {
                    swap(_alloc, rhs._alloc);
                    swap(_data, rhs._data);                    
                    swap(_size, rhs._size);
                    return *this;
                }

                template <typename U = T>
                constexpr storage& operator=(const storage<U, Allocator>& rhs)
                {
                    _alloc = rhs._alloc;
                    _data = rhs._data;
                    _size = rhs._size;
                    return *this;
                }

                template <typename U = T>
                constexpr storage& operator=(storage<U, Allocator>&& rhs)
                {
                    swap(_alloc, rhs._alloc);
                    _data = exchange(rhs._data, nullptr);                    
                    swap(_size, rhs._size);
                    return *this;
                }

                constexpr auto deallocate()
                {
                    return _alloc.deallocate(_data, _size);
                }

                constexpr usize get_size() const
                {
                    return _size;
                }

                constexpr auto* get_pointer() const
                {
                    return _data;
                }

                constexpr void set_pointer(pointer_type ptr)
                {
                    this->_data = ptr;
                }

                constexpr void set_size(usize size)
                {
                    _size = size;
                }
            };

            template < typename Allocator >
            class counter
            {
            public:
                using alloc_type = Allocator;
                using pointer_type = atomic_usize*;
                using value_type = atomic_usize;

            private:
                alloc_type _alloc;
                atomic_usize* _data = nullptr;              

            public:
                constexpr counter()
                requires (DefaultConstructible<alloc_type>)
                {
                    _data = _alloc
                        .template allocate<value_type>(1)
                        .unwrap();
                    
                    *_data = 1;
                }

                constexpr counter(const alloc_type& alloc)
                    : _alloc{alloc}
                {
                    _data = _alloc
                        .template allocate<value_type>(1)
                        .unwrap();
                    
                    *_data = 1;
                }

                constexpr counter(atomic_usize* ptr)
                requires (DefaultConstructible<alloc_type>)
                    : _data{ptr}
                {
                    (*_data)++;
                }

                constexpr counter(const alloc_type& alloc, atomic_usize* ptr)
                    : _alloc{alloc}, _data{ptr}
                {
                    (*_data)++;
                }

                constexpr counter(const counter& other)
                    : _alloc{other._alloc}, _data{other._data}
                {
                    (*_data)++;
                }

                constexpr counter& operator=(const counter& rhs)
                {
                    _alloc = rhs._alloc;
                    _data = rhs._data;
                    (*_data)++;
                    return *this;
                }

                constexpr counter& operator=(counter&& rhs)
                {
                    swap(_alloc, rhs._alloc);
                    swap(_data, rhs._data);
                    return *this;
                }

                constexpr atomic_usize& operator*()
                {
                    return *_data;
                }

                constexpr auto deallocate()
                {
                    return _alloc.deallocate(_data, 1);
                }

                constexpr usize get_size() const
                {
                    return 1u;
                }

                constexpr auto* get_pointer() const
                {
                    return _data;
                }

                constexpr void set_pointer(pointer_type ptr)
                {
                    _data = ptr;
                }
            };
            
        } // namespace shared_detail

        template < typename T, typename Allocator = allocator >
        class shared_ptr
        {
        private:
            using storage_type = shared_detail::storage<T, Allocator>;
            using counter_type = shared_detail::counter<Allocator>;

        public:
            using alloc_type = Allocator;
            using pointer_type = typename storage_type::pointer_type;
            using value_type = typename storage_type::value_type;
            using reference_type = typename storage_type::value_type&;

        private:
            storage_type _value;
            counter_type _count{};

            template < typename U, typename Alloc >
            friend class shared_ptr;

            constexpr void _delete()
            {
                if (_count.get_pointer() != nullptr)
                {
                    (*_count)--;

                    if (*_count == 0)
                    {
                        if (get() != nullptr) 
                        {
                            if constexpr (is_array<T>::value)
                            {
                                for (usize i = 0, size = _value.get_size(); i < size; ++i)
                                    get()[size-i].~value_type();
                            }
                            else
                            {
                                get()->~value_type();
                            }
                        }
                        _value.deallocate().unwrap();
                        _count.deallocate().unwrap();
                    }

                    _value.set_pointer(nullptr);
                    _count.set_pointer(nullptr);
                }
            }

        public:
            constexpr shared_ptr() = default;
            constexpr shared_ptr(null_type) {}

            constexpr shared_ptr(pointer_type ptr) 
            requires (DefaultConstructible<alloc_type>)
                : _value{ptr, 1u}
            {}

            constexpr shared_ptr(pointer_type ptr, usize size) 
            requires (DefaultConstructible<alloc_type>)
                : _value{ptr, size}
            {}

            constexpr shared_ptr(const alloc_type& alloc) 
                : _value{alloc, 1u}, _count{alloc}
            {}

            constexpr shared_ptr(const alloc_type& alloc, usize size) 
                : _value{alloc, size}, _count{alloc}
            {}

            constexpr shared_ptr(pointer_type ptr, 
                const alloc_type& alloc, usize size) 
                : _value{ptr, alloc, size}, _count{alloc}
            {}

            template <typename U = T> 
            constexpr shared_ptr(const shared_ptr<U, Allocator>& other)
            requires(shared_detail::ConvertibleDerived<T, U>)
                : _value{other._value}, _count{other._count}
            {}

            template <typename U = T> 
            constexpr shared_ptr(shared_ptr<U, Allocator>&& other)
            requires(shared_detail::ConvertibleDerived<T, U>)
                : _value{move(other._value)}, _count{move(other._count)}
            {}

            constexpr ~shared_ptr()
            {
                _delete();
            }

            constexpr shared_ptr& operator=(null_type)
            {
                _delete();
                return *this;
            }

            template <typename U = T> 
            requires(shared_detail::ConvertibleDerived<T, U>)
            constexpr shared_ptr& operator=(shared_ptr<U, Allocator>& rhs)
            {
                _delete();
                _value = rhs._value;
                _count = rhs._count;
                return *this;
            }

            template <typename U = T> 
            requires(shared_detail::ConvertibleDerived<T, U>)
            constexpr shared_ptr& operator=(shared_ptr<U, Allocator>&& rhs)
            {
                _delete();
                _value = move(rhs._value);
                _count = move(rhs._count);
                return *this;
            }

            constexpr bool operator==(const shared_ptr& rhs) const
            {
                return get() == rhs.get();
            }

            constexpr bool operator!=(const shared_ptr& rhs) const
            {
                return get() == rhs.get();
            }

            constexpr bool operator==(null_type) const
            {
                return get() == nullptr;
            }

            constexpr bool operator!=(null_type) const
            {
                return get() == nullptr;
            }

            constexpr auto* get()
            {
                return _value.get_pointer();
            }

            constexpr auto* get() const
            {
                return _value.get_pointer();
            }

            constexpr auto* operator->()
            {
                return get();
            }

            constexpr auto* operator->() const
            {
                return get();
            }

            constexpr auto& operator*()
            {
                return *get();
            }

            constexpr auto& operator*() const
            {
                return *get();
            }

            constexpr usize get_count()
            {
                return *_count;
            }

            constexpr bool is_unique()
            {
                return get_count() == 1;
            }
        };

        template < typename T, typename Allocator >
        struct MakeShr
        {
            using single_object = shared_ptr<T, Allocator>;
        };

        template < typename T, typename Allocator >
        struct MakeShr<T[], Allocator>
        {
            using array = shared_ptr<T[], Allocator>;
        };

        template < typename T, usize N, typename Allocator >
        struct MakeShr<T[N], Allocator>
        {
            struct invalid_type {};  
        };

        template < typename T, typename Allocator = allocator, typename... Args >
        requires (DefaultConstructible<Allocator> && IsSame<remove_array_t<T>, T>)
        static constexpr typename MakeShr<T, Allocator>::single_object 
        make_shared(Args&&... args)
        {
            Allocator _alloc;

            auto* _ptr = _alloc
                .template allocate<remove_array_t<T>>(1)
                .unwrap();
            
            construct_at(_ptr, forward<Args>(args)...);
            
            return _ptr;
        }

        template < typename T, typename Allocator = allocator, typename U, typename... Args >
        requires (IsSame<remove_array_t<T>, T> && IsSame<Allocator, U>)
        static constexpr typename MakeShr<T, Allocator>::single_object 
        make_shared(U& alloc, Args&&... args)
        {
            auto* _ptr = alloc
                .template allocate<remove_array_t<T>>(1)
                .unwrap();

            construct_at(_ptr, forward<Args>(args)...);
            return {_ptr, alloc, 1};
        }

        template < typename T, typename Allocator = allocator >
        requires (DefaultConstructible<Allocator> && !IsSame<remove_array_t<T>, T>)
        static constexpr typename MakeShr<T, Allocator>::array
        make_shared(usize size)
        {
            Allocator _alloc;
            
            auto* _ptr = _alloc
                .template allocate<remove_array_t<T>>(size)
                .unwrap();
            
            return {_ptr, size};
        }

        template < typename T, typename Allocator = allocator, typename U >
        requires (!IsSame<remove_array_t<T>, T> && IsSame<Allocator, U>)
        static constexpr typename MakeShr<T, Allocator>::array
        make_shared(U& alloc, usize size)
        {
            return {alloc, size};
        }

        template < typename T, typename Allocator = allocator, typename... Args >
        static constexpr typename MakeShr<T, Allocator>::invalid_type 
        make_shared(Args&&...) = delete;
    }

    template < typename T, typename Allocator = allocator >
    using unsafe_shared_ptr = non_atomic_types::shared_ptr<T, Allocator>;
    template < typename T, typename Allocator = allocator >
    using safe_shared_ptr = atomic_types::shared_ptr<T, Allocator>;

    template < typename T, typename Allocator = allocator, typename... Args >
    static constexpr auto make_unsafe_shared(Args&&... args)
    {
        return non_atomic_types::make_shared<T, Allocator, Args...>(
            forward<Args>(args)...
        );
    }

    template < typename T, typename Allocator = allocator, typename... Args >
    static constexpr auto make_safe_shared(Args&&... args)
    {
        return atomic_types::make_shared<T, Allocator, Args...>(
            forward<Args>(args)...
        );
    }

    template < typename T, typename Allocator = allocator, typename U, typename... Args >
    static constexpr auto make_unsafe_shared(U& alloc, Args&&... args)
    {
        return non_atomic_types::make_shared<T, Allocator, U, Args...>(
            alloc, forward<Args>(args)...
        );
    }

    template < typename T, typename Allocator = allocator, typename U, typename... Args >
    static constexpr auto make_safe_shared(U& alloc, Args&&... args)
    {
        return atomic_types::make_shared<T, Allocator, U, Args...>(
            alloc, forward<Args>(args)...
        );
    }

    template < typename T, typename Allocator = allocator >
    static constexpr auto make_unsafe_shared(usize size)
    {
        return non_atomic_types::make_shared<T, Allocator>(size);
    }

    template < typename T, typename Allocator = allocator >
    static constexpr auto make_safe_shared(usize size)
    {
        return atomic_types::make_shared<T, Allocator>(size);
    }

    template < typename T, typename Allocator = allocator, typename U >
    static constexpr auto make_unsafe_shared(U& alloc, usize size)
    {
        return non_atomic_types::make_shared<T, Allocator, U>(alloc, size);
    }

    template < typename T, typename Allocator = allocator, typename U >
    static constexpr auto make_safe_shared(U& alloc, usize size)
    {
        return atomic_types::make_shared<T, Allocator, U>(alloc, size);
    }
}