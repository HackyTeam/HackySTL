#pragma once

#include "../Base/Allocator.hpp"
#include "../Base/Concepts.hpp"

namespace hsd
{
    namespace unique_detail
    {
        template <typename T, typename U>
        concept ConvertibleDerived = Convertible<U, T> || std::is_base_of_v<T, U>;

        template < typename T, typename Allocator >
        class storage
        {
        public:
            using alloc_type = Allocator;
            using pointer_type = remove_array_t<T>*;
            using value_type = remove_array_t<T>;

        private:
            alloc_type _alloc;
            pointer_type _data = nullptr;
            usize _size = 0;
            
            template <typename U, typename Alloc>
            friend class storage;

        public:
            constexpr storage()
            requires (DefaultConstructible<alloc_type>) = default;

            constexpr storage(pointer_type ptr, usize size)
            requires (DefaultConstructible<alloc_type>)
                : _data{ptr}, _size{size}
            {}

            constexpr storage(const alloc_type& alloc, usize size)
            requires (DefaultConstructible<value_type>)
                : _alloc{alloc}, _size{size}
            {
                _data = _alloc
                    .template allocate<value_type>(size)
                    .unwrap();
                
                construct_at(_data);

                for(usize _index = 0; _index < size; _index++)
                {
                    construct_at(&_data[_index]);
                }
            }

            constexpr storage(pointer_type ptr, const alloc_type& alloc, usize size)
                : _alloc{alloc}, _data{ptr}, _size{size}
            {}

            constexpr storage(const storage&) = delete;

            template <typename U = T>
            constexpr storage(storage<U, Allocator>&& other)
                : _alloc{move(other._alloc)}, 
                _data{exchange(other._data, nullptr)},
                _size{exchange(other._size, 0)}
            {}

            template <typename U = T>
            constexpr storage& operator=(storage<U, Allocator>&& rhs)
            {
                deallocate().unwrap();
                swap(rhs._alloc, _alloc);
                _data = exchange(rhs._data, nullptr);
                swap(_size, rhs._size);
                return *this;
            }

            constexpr auto deallocate()
            {
                return _alloc.deallocate(_data, _size);
            }

            constexpr usize get_size() const
            {
                return _size;
            }

            constexpr auto* get_pointer() const
            {
                return _data;
            }

            constexpr void set_pointer(pointer_type ptr)
            {
                _data = ptr;
            }

            constexpr void set_size(usize size)
            {
                _size = size;
            }
        };
    } // namespace unique_detail

    template < typename T, typename Allocator = allocator >
    class unique_ptr
    {
    private:
        using storage_type = unique_detail::storage<T, Allocator>;
        storage_type _value;

        template <typename U, typename Alloc>
        friend class unique_ptr;

    public:
        using alloc_type = Allocator;
        using pointer_type = typename storage_type::pointer_type;
        using value_type = typename storage_type::value_type;
        using reference_type = typename storage_type::value_type&;

    private:
        constexpr void _delete()
        {
            if (get() != nullptr)
            {
                if constexpr (is_array<T>::value)
                {
                    for (usize i = 0, size = _value.get_size(); i < size; ++i)
                        get()[size - i].~value_type();
                }
                else
                {
                    get()->~value_type();
                }
            }
            
            _value.deallocate().unwrap();
            _value.set_pointer(nullptr);
        }

    public:     
        constexpr unique_ptr() = default;
        constexpr unique_ptr(null_type) {}
        unique_ptr(unique_ptr&) = delete;
        unique_ptr(const unique_ptr&) = delete;

        constexpr unique_ptr(pointer_type ptr) 
            : _value{ptr, 1u}
        {}

        constexpr unique_ptr(pointer_type ptr, usize size) 
            : _value{ptr, size}
        {}

        constexpr unique_ptr(const alloc_type& alloc) 
            : _value{alloc, 1u}
        {}

        constexpr unique_ptr(const alloc_type& alloc, usize size) 
            : _value{alloc, size}
        {}

        constexpr unique_ptr(pointer_type ptr, 
            const alloc_type& alloc, usize size) 
            : _value{ptr, alloc, size}
        {}

        template <typename U = T> 
        requires(unique_detail::ConvertibleDerived<T, U>)
        constexpr unique_ptr(unique_ptr<U, Allocator>&& other)
            : _value{move(other._value)}
        {}

        constexpr ~unique_ptr()
        {
            _delete();
        }

        constexpr unique_ptr& operator=(null_type)
        {
            _delete();
            return *this;
        }

        template <typename U = T> 
        requires(unique_detail::ConvertibleDerived<T, U>)
        constexpr unique_ptr& operator=(unique_ptr<U, Allocator>&& rhs)
        {
            _delete();
            _value = move(rhs._value);
            return *this;
        }

        constexpr auto* get()
        {
            return _value.get_pointer();
        }

        constexpr auto* get() const
        {
            return _value.get_pointer();
        }

        constexpr bool operator!=(null_type) const
        {
            return _value.get_pointer() != nullptr;
        }

        constexpr bool operator==(null_type) const
        {
            return _value.get_pointer() == nullptr;
        }

        constexpr auto* operator->()
        {
            return get();
        }

        constexpr auto* operator->() const
        {
            return get();
        }

        constexpr auto& operator*()
        {
            return *get();
        }

        constexpr auto& operator*() const
        {
            return *get();
        }
    };

    template < typename T, typename Allocator >
    struct MakeUniq
    {
        using single_object = unique_ptr<T, Allocator>;
    };

    template < typename T, typename Allocator >
    struct MakeUniq<T[], Allocator>
    {
        using array = unique_ptr<T[], Allocator>;
    };
    
    template < typename T, usize N, typename Allocator >
    struct MakeUniq<T[N], Allocator>
    {
        struct invalid_type {};  
    };
    
    template < typename T, typename Allocator = allocator, typename... Args >
    requires (DefaultConstructible<Allocator> && IsSame<remove_array_t<T>, T>)
    static constexpr typename MakeUniq<T, Allocator>::single_object 
    make_unique(Args&&... args)
    {
        Allocator _alloc;
        remove_array_t<T>* _ptr = _alloc
            .template allocate<remove_array_t<T>>(1)
            .unwrap();
        
        construct_at(_ptr, forward<Args>(args)...);
        return _ptr;
    }

    template < typename T, typename Allocator = allocator, typename U, typename... Args >
    requires (IsSame<remove_array_t<T>, T> && IsSame<Allocator, U>)
    static constexpr typename MakeUniq<T, Allocator>::single_object 
    make_unique(U& alloc, Args&&... args)
    {
        remove_array_t<T>* _ptr = alloc
            .template allocate<remove_array_t<T>>(1)
            .unwrap();
        
        construct_at(_ptr, forward<Args>(args)...);
        return unique_ptr<T, Allocator>{_ptr, alloc, 1};
    }

    template < typename T, typename Allocator = allocator >
    requires (DefaultConstructible<Allocator> && !IsSame<remove_array_t<T>, T>)
    static constexpr typename MakeUniq<T, Allocator>::array 
    make_unique(usize size)
    {
        Allocator _alloc;
        
        remove_array_t<T>* _ptr = _alloc
            .template allocate<remove_array_t<T>>(size)
            .unwrap();
        
        return {_ptr, size};
    }

    template < typename T, typename Allocator = allocator, typename U >
    requires (!IsSame<remove_array_t<T>, T> && IsSame<Allocator, U>)
    static constexpr typename MakeUniq<T, Allocator>::array 
    make_unique(U& alloc, usize size)
    {
        return {alloc, size};
    }

    template <typename T, typename Allocator = allocator, typename... Args>
    static constexpr typename MakeUniq<T, Allocator>::invalid_type 
    make_unique(const Allocator&, Args&&...) = delete;
} // namespace hsd
